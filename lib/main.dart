import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/router.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/localization/localizations_delegate.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/screen_util.dart';
import 'package:provider/provider.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';
import 'app_config.dart';
import 'locator.dart';
import 'utils/provider_setup.dart';

enum UniLinksType { string, uri }

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: brandColor, // Color for Android
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness:
          Brightness.light // Dark == white status bar -- for IOS.
      ));
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await setupSharedPreferences();
  await initPlatformState();
  await Firebase.initializeApp();
  setupLocator();
  var configuredApp = AppConfig(
    appName: 'STGPocketPills',
    flavorName: 'staging',
    apiBaseUrl: 'https://c9.api.pocketpills.com',
    webviewBaseUrl: 'https://stgapp.pocketpills.com',
    mixPanelToken: "6a4886648afb69fca0a05c2649c6149d",
    oneSignalToken: 'cbb37f0e-8394-4c2c-aa36-041a471abade',
    child: MyApp(),
  );

  setupAppConfig(configuredApp);
  runApp(configuredApp);
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale newLocale) {
    var state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(newLocale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _latestLink = 'Unknown';
  Uri _latestUri;
  static final DataStoreService dataStore = locator<DataStoreService>();

  StreamSubscription _sub;

  UniLinksType _type = UniLinksType.string;

  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  initState() {
    initPlatformState();
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
    if (_sub != null) {
      _sub.cancel();
    }
  }

  initPlatformState() async {
    if (_type == UniLinksType.string) {
      await initPlatformStateForStringUniLinks();
    }
  }

  initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
    _sub = getLinksStream().listen((String link) {
      if (!mounted) return;
      setState(() {
        _latestLink = link ?? 'Unknown';
        _latestUri = null;
        try {
          if (link != null) {
            print('AMANUNILINK:$link');
            _latestLink = link ?? 'Unknown';
            _latestUri = Uri.parse(link);
            extractArguments(link);
            print('AMANUNILINKURI:$_latestUri');
          }
        } on FormatException {}
      });
    }, onError: (err) {
      if (!mounted) return;
      setState(() {
        _latestLink = 'Failed to get latest link: $err.';
        _latestUri = null;
      });
    });

    // Get the latest link
    String initialLink;
    Uri initialUri;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      initialLink = await getInitialLink();
      print('initial link: $initialLink');

      if (initialLink != null) {
        initialUri = Uri.parse(initialLink);
        extractArguments(initialLink);
      }
    } on PlatformException {
      initialLink = 'Failed to get initial link.';
      initialUri = null;
    } on FormatException {
      initialLink = 'Failed to parse the initial link as Uri.';
      initialUri = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _latestLink = initialLink;
      _latestUri = initialUri;
    });
  }

  static void extractArguments(String value) async {
    print('AMANEXTRACTARGCALLED');
    List<String> args = value.split('&');
    List<String> params = args[0].split('?');
    List<String> token = args[1].split('=');
    List<String> group = params[1].split('=');

    await dataStore.writeString(DataStoreService.GROUP, group[1]);
    await dataStore.writeString(DataStoreService.TOKEN, token[1]);

    print('groups:${group[1]} + token:${token[1]}');
    await HttpApiUtils().chambersActivate(group[1], token[1], null);
  }

  @override
  void didChangeDependencies() async {
    getLocale().then((locale) {
      setState(() {
        _locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    AppConfig.of(context);
    print('AMANMAINBUILDCALLED');
    return MultiProvider(
        providers: providers,
        child: MaterialApp(
          locale: _locale,
          builder: (context, child) {
            ScreenUtil.init(context);
            return MediaQuery(
              child: child,
              data: MediaQuery.of(context).copyWith(
                textScaleFactor: ScreenUtil().getTextScale(),
              ),
            );
          },
          supportedLocales: [
            const Locale('en', ''),
            const Locale('fr', ''),
          ],
          localizationsDelegates: [
            const AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          localeResolutionCallback: (locale, supportedLocales) {
            for (var supportedLocale in supportedLocales) {
              if (supportedLocale?.languageCode == locale?.languageCode &&
                  supportedLocale?.countryCode == locale?.countryCode) {
                return supportedLocale;
              }
            }
            return supportedLocales?.first;
          },
          title: 'PocketPills',
          theme: ThemeData(
              primaryColor: brandColor,
              accentColor: brandColor,
              backgroundColor: Colors.white,
              scaffoldBackgroundColor: Colors.white,
              buttonTheme: ButtonThemeData(height: 48)),
          navigatorKey: locator<NavigationService>().navigatorKey,
          initialRoute: SplashView.routeName,
          onGenerateRoute: PPRouter.generateRoute,
          debugShowCheckedModeBanner: false,
        ));
  }
}
