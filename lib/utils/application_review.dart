import 'package:intl/intl.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';

class ApplicationReview {
  static final DataStoreService dataStore = locator<DataStoreService>();

  static saveOnClickAppviewCardTimeStamp() async {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd/MM/yyyy');
    String formattedDate = formatter.format(now);
    await dataStore.writeBoolean(DataStoreService.IS_APP_REVIEW,true);
    await dataStore.saveReviewClickDate(formattedDate);
  }

  static bool showAppReviewCard() {
    var clickDate = dataStore.getReviewClickDateToken();
    String day, month, year;
    if (clickDate != null) {
      List<String> list = clickDate.split("/");
      day = list[0];
      month = list[1];
      year = list[2];
    } else if (dataStore.readBoolean(DataStoreService.IS_APP_REVIEW) == null || dataStore.readBoolean(DataStoreService.IS_APP_REVIEW) == false) {
      return true;
    } else {
      return false;
    }
    var temp = DateTime.now();
    var currentDate = DateTime.utc(temp.year, temp.month, temp.day);
    var previoudDate = DateTime.utc(int.parse(year), int.parse(month), int.parse(day));

    int diffDays = currentDate.difference(previoudDate).inDays;

    if (diffDays >= 30) {
      dataStore.writeBoolean(DataStoreService.IS_APP_REVIEW,false);
      return true;
    } else {
      return false;
    }
  }
}
