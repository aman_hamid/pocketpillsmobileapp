import 'package:flutter/material.dart';
import 'package:pocketpills/utils/localization/app_translations.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<AppTranslations> {
  final Locale newLocale;
  const AppLocalizationsDelegate({this.newLocale});

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<AppTranslations> load(Locale locale) {
    return AppTranslations.load(newLocale ?? locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppTranslations> old) => false;
}
