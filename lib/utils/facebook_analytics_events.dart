import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics_event.dart';
import 'package:pocketpills/utils/facebook_analytics.dart';

class FacebookAnalyticsEvents extends AnalyticsEvents {
  FacebookAnalytics _facebookAnalytics;
  static final dataStore = locator<DataStoreService>();

  FacebookAnalyticsEvents() {
    _facebookAnalytics = FacebookAnalytics();
    initilize();
  }

  Future<void> initilize() async {
    await _facebookAnalytics.initialize();
  }

  @override
  Future<void> sendAnalyticsEvent(String eventName, [Map<String, dynamic> bundle]) {
    _facebookAnalytics.logEvent(eventName, bundle);
  }

  @override
  Future<void> sendInitiatedEvent(int prescriptionId) {
    Map<String, String> map = new Map(); //TODO Change parameter
    map["contentData"] = "prescription";
    map["contentId"] = prescriptionId != null ? prescriptionId.toString() : -1;
    map["contentType"] = "Initiate Checkout";

    _facebookAnalytics.logInitiatedEvent(map);
  }

  Future<void> logCompleteRegistrationEvent() async {
    await _facebookAnalytics.logCompleteRegistrationEvent();
  }

  @override
  Future<void> setAnalyticsEventIdentify() {
    Map<String, dynamic> map = new Map(); //TODO Change parameter
    map["email"] = dataStore.readString(DataStoreService.EMAIL) != null ? dataStore.readString(DataStoreService.EMAIL) : " ";
    map["firstName"] =
        dataStore.readString(DataStoreService.FIRSTNAME) != null ? dataStore.readString(DataStoreService.FIRSTNAME) : " ";
    map["lastName"] = dataStore.readString(DataStoreService.LASTNAME) != null ? dataStore.readString(DataStoreService.LASTNAME) : " ";
    map["phone"] = dataStore.readString(DataStoreService.PHONE) != null ? dataStore.readString(DataStoreService.PHONE) : " ";
    map["dateOfBirth"] = dataStore.readString(DataStoreService.DATE_OF_BIRTH) != null ? dataStore.readString(DataStoreService.DATE_OF_BIRTH) : " ";
    map["gender"] = dataStore.readString(DataStoreService.GENDER) != null ? dataStore.readString(DataStoreService.GENDER) : " ";
    map["city"] = "CANADA";
    map["state"] = "CANADA";
    map["zip"] = dataStore.readString(DataStoreService.ZIP) != null ? dataStore.readString(DataStoreService.ZIP) : " ";
    map["country"] = "CANADA";
   //_facebookAnalytics.setUserData(map);

    Map<String, dynamic> userIdentifier = new Map();
    map["userid"] = dataStore.getUserId() != null ? dataStore.getUserId().toString() : "";
    //_facebookAnalytics.setUserIdentifier(userIdentifier);
  }
}
