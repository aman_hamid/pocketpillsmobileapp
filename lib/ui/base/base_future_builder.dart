import 'package:flutter/widgets.dart';

class BaseFutureBuilder<T> extends StatefulWidget {
  final Future<T> future;

  final bool rememberFutureResult;

  final Widget whenActive;

  final Widget whenWaiting;

  final Widget whenNone;

  final Widget whenNotDone;

  final Widget Function(T snapshotData) whenDone;

  final Widget Function(Object error) whenError;

  /// See [FutureBuilder] for more info
  final T initialData;

  const BaseFutureBuilder(
      {Key key,
      @required this.future,
      @required this.rememberFutureResult,
      @required this.whenDone,
      @required this.whenNotDone,
      this.whenError,
      this.whenActive,
      this.whenNone,
      this.whenWaiting,
      this.initialData})
      : assert(future != null),
        assert(rememberFutureResult != null),
        assert(whenDone != null),
        assert(whenNotDone != null),
        super(key: key);

  @override
  _BaseFutureBuilderState createState() => _BaseFutureBuilderState<T>();
}

class _BaseFutureBuilderState<T> extends State<BaseFutureBuilder<T>> {
  Future<T> _cachedFuture;

  @override
  void initState() {
    super.initState();
    _cachedFuture = this.widget.future;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<T>(
      future: this.widget.rememberFutureResult ? _cachedFuture : this.widget.future,
      initialData: this.widget.initialData,
      builder: (context, snapshot) {
        if (this.widget.whenActive != null && snapshot.connectionState == ConnectionState.active) {
          return this.widget.whenActive;
        }

        if (this.widget.whenNone != null && snapshot.connectionState == ConnectionState.none) {
          return this.widget.whenNone;
        }

        if (this.widget.whenWaiting != null && snapshot.connectionState == ConnectionState.waiting) {
          return this.widget.whenWaiting;
        }

        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            if (this.widget.whenError != null) {
              return this.widget.whenError(snapshot.error);
            } else {
              return this.widget.whenNotDone;
            }
          }
          return this.widget.whenDone(snapshot.data);
        }

        return this.widget.whenNotDone;
      },
    );
  }
}
