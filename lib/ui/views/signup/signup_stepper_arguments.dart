import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class SignupStepperArguments {
  final int position;
  final String from;
  final BaseStepperSource source;
  final bool chambersFlow;

  SignupStepperArguments(
      {this.position, this.source, this.from, this.chambersFlow});
}
