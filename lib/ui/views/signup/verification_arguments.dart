import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class VerificationArguments {
  final int phoneNo;
  final BaseStepperSource source;

  VerificationArguments({this.phoneNo, this.source});
}
