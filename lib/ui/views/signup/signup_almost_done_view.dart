import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_user_contact_view.dart';
import 'package:pocketpills/ui/views/signup/success_view.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class SignUpAlmostDoneWidget extends StatefulWidget {
  static const routeName = 'signUpAlmostDoneWidget';
  final Function onSuccess;
  final Function(String) onShowSnackBar;
  final Function onBack;
  final BaseStepperSource source;
  final String from;
  bool chambersFlow;

  SignUpAlmostDoneWidget(
      {Key key,
      this.onBack,
      this.onSuccess,
      this.onShowSnackBar,
      this.source,
      this.from,
      this.chambersFlow})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpAlmostDoneState();
  }
}

class SignUpAlmostDoneState extends BaseState<SignUpAlmostDoneWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  PPRadioGroup<String> selfMedicationRadioGroup;
  PPRadioGroup<String> manageMedicationRadioGroup;
  PPRadioGroup<String> genderRadioGroup;

  final _aboutUsformKey = GlobalKey<FormState>();
  String dropDownValue = null;
  String dropDownError;
  String prevMobileNumber = "";
  String phState = '';
  bool manitobaCheck = true;
  bool showReferral = false;
  bool autovalidate = false;
  bool phoneAlreadyRegister = false;

  bool completeContactDetails = false;
  bool verified = false;
  bool completeHealthInfo = false;

  static bool gender = false, email = false, referralCode = false;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _invitationCodeController =
      TextEditingController();

  final TextEditingController _phoneNumberController = TextEditingController();

  final FocusNode _emailFnode = FocusNode();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  static bool selfMedication = false,
      manageMedication = false,
      provincial = false;
  SignUpAboutYouModel model;
  bool initialize = false;

  @override
  void initState() {
    super.initState();
    model = SignUpAboutYouModel();

    verified = dataStore.readBoolean(DataStoreService.VERIFIED);
    _emailController.addListener(emailListener);
    _invitationCodeController.addListener(referralListener);
    _phoneNumberController.addListener(phoneNumberListener);

    _emailController.text = dataStore.readString(DataStoreService.EMAIL);
    String referralCode = dataStore.readString(DataStoreService.REFERRAL_CODE);
    String invitationCode =
        dataStore.readString(DataStoreService.INVITATION_CODE);
    (referralCode != null || invitationCode != null)
        ? showReferral = true
        : showReferral = false;
    _invitationCodeController.text = referralCode != null
        ? referralCode
        : invitationCode != null
            ? invitationCode
            : "";
    dataStore.readString(DataStoreService.PROVINCE) != null
        ? dropDownValue =
            dataStore.readString(DataStoreService.PROVINCE).toLowerCase()
        : " ";
  }

  @override
  void dispose() {
    _emailController.removeListener(emailListener);
    _invitationCodeController.removeListener(referralListener);
    super.dispose();
  }

  bool isFormValid() {
    bool ge = genderRadioGroup.validate();
    return ge;
  }

  emailListener() {
    if (email == false) {
      email = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_email_entered);
    }
  }

  referralListener() {
    if (referralCode == false) {
      referralCode = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_referralcode_entered);
    }
  }

  phoneNumberListener() async {
    if (_phoneNumberController.text.length == 10 &&
        prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification checkPhone =
          await Provider.of<SignUpModel>(context).verifyPhone(prevMobileNumber);
      if (checkPhone.redirect == PhoneNumberStateConstant.LOGIN ||
          checkPhone.redirect == PhoneNumberStateConstant.FORGOT) {
        setState(() {
          phoneAlreadyRegister = true;
          phState = checkPhone.redirect;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phoneAlreadyRegister = false;
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;

    if (_phoneNumberController.text.length ==
        SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
    }
  }

  Widget getGenderUI() {
    return genderRadioGroup;
  }

  Widget getTakeMedicationUI() {
    return selfMedicationRadioGroup;
  }

  Widget getManageMedicationUI() {
    return manageMedicationRadioGroup;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model, context),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return WillPopScope(
                child: getProvider(),
                onWillPop: widget.source == BaseStepperSource.NEW_USER
                    ? () async {
                        Navigator.canPop(context)
                            ? Navigator.pop(context)
                            : _onBackPressed(context);
                        return true;
                      }
                    : () => _onBackPressed(context));
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(
      SignUpAboutYouModel model, BuildContext context) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization([
      "signup",
      "forgot",
      "common",
      "modal",
      "modals",
      "copay"
    ]); // will take 3 secs
    return await Future.wait([future1]);
  }

  Widget getProvider() {
    if (!initialize) {
      initializeContent();
      initialize = true;
    }

    genderRadioGroup.initialValue =
        dataStore.readString(DataStoreService.GENDER);
    return ChangeNotifierProvider(
      create: (_) => SignUpAboutYouModel(),
      child: Consumer<SignUpAboutYouModel>(builder:
          (BuildContext context, SignUpAboutYouModel model, Widget child) {
        return getMainView(model);
      }),
    );
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  Widget getMainView(SignUpAboutYouModel model) {
    return BaseScaffold(
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? AppBar(
              backgroundColor: Colors.white,
              title: Image.asset('graphics/logo-horizontal-dark.png',
                  width: MediaQuery.of(context).size.width * 0.45),
              centerTitle: true,
            )
          : null,
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                children: <Widget>[
                  Expanded(
                      child: SingleChildScrollView(
                          child: getStartedView(context, model))),
                  Builder(
                      builder: (BuildContext context) =>
                          PPBottomBars.getButtonedBottomBar(
                            child: model.state == ViewState.Busy
                                ? ViewConstants.progressIndicator
                                : PrimaryButton(
                                    text:
                                        LocalizationUtils.getSingleValueString(
                                            "signup", "signup.almostdone.next"),
                                    onPressed: () {
                                      onNextClick(context, model);
                                    },
                                    fullWidth: true),
                          ))
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getStartedView(BuildContext context, SignUpAboutYouModel model) {
    return getBaseDefaultView(context, model);
  }

  void onClickCellPhoneAlreadyRegister(SignUpAboutYouModel model) async {
    if (phState == PhoneNumberStateConstant.LOGIN) {
      Navigator.pushNamedAndRemoveUntil(
          context, LoginWidget.routeName, (Route<dynamic> route) => false,
          arguments: LoginArguments(
              phone: _phoneNumberController.text != null
                  ? _phoneNumberController.text
                  : "",
              source: BaseStepperSource.ABOUT_YOU_SCREEN));
    } else if (phState == PhoneNumberStateConstant.FORGOT) {
      var success = await model.getOtp(_phoneNumberController.text);
      if (success == true) {
        Navigator.pushNamedAndRemoveUntil(context,
            LoginVerificationWidget.routeName, (Route<dynamic> route) => false,
            arguments: VerificationArguments(
              phoneNo: int.parse(_phoneNumberController.text),
            ));
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }

  Widget getBaseDefaultView(BuildContext context, SignUpAboutYouModel model) {
    return Form(
      key: _aboutUsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                  child: Column(
                    children: <Widget>[
                      widget.source == BaseStepperSource.NEW_USER
                          ? pharmacistFlow()
                          : normalFlow(),
                      provinceCard()
                    ],
                  )),
              getManitobaCheck(),
              Padding(
                padding: const EdgeInsets.only(
                    left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX),
                child: getGenderUI(),
              ),
              Padding(
                  padding: EdgeInsets.only(
                    left: MEDIUM_XXX,
                    right: MEDIUM_XXX,
                  ),
                  child: Column(children: <Widget>[
                    SizedBox(height: MEDIUM_XX),
                    PPFormFields.getTextField(
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.emailAddress,
                        focusNode: _emailFnode,
                        controller: _emailController,
                        textInputAction: TextInputAction.done,
                        onErrorStr: LocalizationUtils.getSingleValueString(
                                "common", "common.label.required") +
                            "*",
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString(
                                "signup", "signup.fields.email-label"),
                            hintText: LocalizationUtils.getSingleValueString(
                                "signup", "signup.fields.enter-email")),
                        onFieldSubmitted: (value) {
                          onNextClick(context, model);
                        }),
                    Padding(
                        padding: EdgeInsets.only(top: SMALL),
                        child: SizedBox(height: MEDIUM_XX)),
                    PPTexts.getFormLabel(
                        LocalizationUtils.getSingleValueString(
                            "signup", "signup.fields.email-hint"),
                        isBold: false),
                    SizedBox(height: SMALL),
                    dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) ==
                            false
                        ? Container(
                            decoration: BoxDecoration(
                                color: Color(0xfff7f7f7),
                                borderRadius: BorderRadius.circular(4.0)),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(12.0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            showReferral == false
                                                ? showReferral = true
                                                : showReferral = false;
                                          });
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              LocalizationUtils
                                                  .getSingleValueString(
                                                      "signup",
                                                      "signup.fields.have-referral-code"),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            new Expanded(
                                                child: Align(
                                              alignment: Alignment.topRight,
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    showReferral == false
                                                        ? showReferral = true
                                                        : showReferral = false;
                                                  });
                                                },
                                                child: Image.asset(
                                                    showReferral == false
                                                        ? 'graphics/icons/ic_round_arrow_down.png'
                                                        : 'graphics/icons/ic_round_arrow_up.png',
                                                    width: 24.0,
                                                    height: 24.0),
                                              ),
                                            )),
                                          ],
                                        ))),
                                getReferralCodeView(),
                              ],
                            ),
                          )
                        : Container(),
                  ])),
              SizedBox(height: MEDIUM_XXX),
            ],
          ),
        ],
      ),
    );
  }

  Widget provinceCard() {
    return dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == false
        ? Column(
            children: [
              PPTexts.getFormLabel(
                  LocalizationUtils.getSingleValueString(
                      "signup", "signup.fields.provience-label"),
                  isBold: true),
              PPUIHelper.verticalSpaceSmall(),
              getDropDown(),
              dropDownError == null
                  ? SizedBox(height: MEDIUM_XXX)
                  : PPTexts.getFormError(dropDownError, color: errorColor),
            ],
          )
        : Container();
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(
              displayImage:
                  "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString(
                "signup", "signup.almostdone.description"),
            style: TextStyle(
              color: lightBlue,
              fontWeight: FontWeight.w500,
              fontSize: 16.0,
              height: 1.5,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  Widget normalFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString(
            "signup", "signup.almostdone.title")),
        SizedBox(height: SMALL_X),
        PPTexts.getSecondaryHeading(
            LocalizationUtils.getSingleValueString(
                "signup", "signup.almostdone.description-telehealth"),
            isBold: false),
        SizedBox(height: REGULAR_XXX),
      ],
    );
  }

  Widget getReferralCodeView() {
    if (showReferral == true) {
      return AnimatedContainer(
        duration: Duration(seconds: 2),
        child: Column(
          children: <Widget>[
            PPDivider(),
            PPUIHelper.verticalSpaceLarge(),
            Padding(
                padding: EdgeInsets.all(12.0),
                child: PPFormFields.getTextField(
                    controller: _invitationCodeController,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString(
                            "signup", "signup.fields.referral-label"),
                        hintText: LocalizationUtils.getSingleValueString(
                            "signup", "signup.fields.enter-referral-code"),
                        helperText: LocalizationUtils.getSingleValueString(
                            "signup", "signup.fields.referral-hint")),
                    validator: (value) {
                      return null;
                    }))
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  onNextClick(context, SignUpAboutYouModel signUpAboutYouModel) async {
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    onClickNextDefaultBaseView(context, signUpAboutYouModel);
  }

  // onClickNextDefaultBaseView(context, SignUpAboutYouModel signUpAboutYouModel) async {
  //   if (_aboutUsformKey.currentState.validate() && isFormValid() && StringUtils.isEmail(_emailController.text)) {
  //     var success = await signUpAboutYouModel.updateAboutYou(
  //         province: dropDownValue,
  //         selfMedication: selfMedicationRadioGroup.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
  //         isCaregiver: manageMedicationRadioGroup.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false,
  //         pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
  //         gender: getGenderEnglish(genderRadioGroup.getValue()),
  //         email: _emailController.text,
  //         referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text);
  //     if (success) {
  //       analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification);
  //       if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) == false) {
  //         Navigator.of(context)
  //             .pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
  //       } else {
  //         bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
  //         if (transferSkipped == true) {
  //           Navigator.of(context)
  //               .pushNamedAndRemoveUntil(SignupUserContactWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
  //         } else {
  //           Navigator.of(context)
  //               .pushNamedAndRemoveUntil(SignUpSuccessWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
  //         }
  //       }
  //     } else {
  //       analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done_verification_failed);
  //       onFail(context, errMessage: signUpAboutYouModel.errorMessage);
  //     }
  //   } else if (dropDownValue == null) {
  //     setState(() {
  //       dropDownError = "Please select a province.";
  //     });
  //   } else if (!isFormValid()) {
  //     onFail(context, errMessage: "Please enter a gender");
  //   } else if (StringUtils.isEmail(_emailController.text) == false) {
  //     onFail(context, errMessage: "Please enter a valid email");
  //   }
  // }

  onClickNextDefaultBaseView(
      context, SignUpAboutYouModel signUpAboutYouModel) async {
    setState(() {
      autovalidate = true;
    });
    if (!_aboutUsformKey.currentState.validate()) {
      isFormValid();
      onFail(context,
          errMessage: LocalizationUtils.getSingleValueString(
              "signup", "signup.employer.fill-details"));
      if (dropDownValue == null) {
        setState(() {
          dropDownError = LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.select-province-error");
        });
      }
    } else if (genderRadioGroup.getValue() == null) {
      genderRadioGroup.validate();
    } else if (!StringUtils.isEmail(_emailController.text)) {
      onFail(context,
          errMessage: LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.valid-email"));
    } else {
      var success = await signUpAboutYouModel.updateAboutYou(
          province: dropDownValue,
          selfMedication: selfMedicationRadioGroup.getValue() ==
                  LocalizationUtils.getSingleValueString(
                          "common", "common.all.yes")
                      .toUpperCase()
              ? true
              : false,
          isCaregiver: manageMedicationRadioGroup.getValue() ==
                  LocalizationUtils.getSingleValueString(
                          "common", "common.all.yes")
                      .toUpperCase()
              ? true
              : false,
          pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null,
          gender: getGenderEnglish(genderRadioGroup.getValue()),
          email: _emailController.text,
          referral: _invitationCodeController.text == ""
              ? null
              : _invitationCodeController.text);
      if (success) {
        analyticsEvents.sendAnalyticsEvent(
            AnalyticsEventConstant.account_almost_done_verification);
        if (dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD) ==
            false) {
          if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                SignUpOtpPasswordWidget.routeName,
                (Route<dynamic> route) => false,
                arguments: SignupStepperArguments(
                    source: BaseStepperSource.NEW_USER, chambersFlow: true));
          }
          Navigator.of(context).pushNamedAndRemoveUntil(
              SignUpOtpPasswordWidget.routeName,
              (Route<dynamic> route) => false,
              arguments:
                  SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        } else {
          bool transferSkipped =
              dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
          if (transferSkipped == true) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                SignupUserContactWidget.routeName,
                (Route<dynamic> route) => false,
                arguments:
                    SignupStepperArguments(source: BaseStepperSource.NEW_USER));
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil(
                SignUpSuccessWidget.routeName, (Route<dynamic> route) => false,
                arguments:
                    SignupStepperArguments(source: BaseStepperSource.NEW_USER));
          }
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(
            AnalyticsEventConstant.account_almost_done_verification_failed);
        onFail(context, errMessage: signUpAboutYouModel.errorMessage);
      }
    }
  }

  Widget getManitobaCheck() {
    if (dropDownValue == "manitoba") {
      return InkWell(
        onTap: () {
          setState(() {
            manitobaCheck = !manitobaCheck;
          });
        },
        child: Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Checkbox(
                    value: manitobaCheck,
                    activeColor: brandColor,
                    onChanged: (bool value) {
                      setState(() {
                        manitobaCheck = !manitobaCheck;
                      });
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: SMALL_XXX),
                  child: Text(
                    LocalizationUtils.getSingleValueString(
                        "signup", "signup.fields.packpermission-label"),
                    overflow: TextOverflow.clip,
                    style: TextStyle(
                        height: 1.4,
                        color: secondaryColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: MEDIUM),
        ]),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString(
            "signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        items: getStateMap()
            .map((state) => DropdownMenuItem<String>(
                value: state["key"], child: Text(state["value"])))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (provincial == false) {
            provincial = true;
            analyticsEvents
                .sendAnalyticsEvent(AnalyticsEventConstant.au_province_entered);
          }
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString(
            "signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: getStateMap()
            .map((state) => DropdownMenuItem<String>(
                value: state["key"], child: Text(state["value"])))
            .toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
  }

  TextFormField getUsernameField() {
    return PPFormFields.getNumericFormField(
        maxLength: 10,
        keyboardType: TextInputType.phone,
        minLength: 10,
        textInputAction: TextInputAction.next,
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocusNode,
        onFieldSubmitted: (value) {
          _fieldFocusChange(context, _phoneNumberFocusNode, _emailFnode);
        },
        validator: (value) {
          if (value.isEmpty)
            return LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.cell-phone");
          String formattedNumber = value
              .replaceAll("\(", "")
              .replaceAll("\)", "")
              .replaceAll(" ", "")
              .replaceAll("-", "")
              .replaceAll("+", "");
          if (formattedNumber.length < 10)
            return LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.cell-error1");
          if (formattedNumber.length > 10)
            return LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.cell-error2");
          return null;
        });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void initializeContent() {
    selfMedicationRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes")
            .toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no")
            .toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString(
          "signup", "signup.almostdone.medications-daily"),
      errorText: LocalizationUtils.getSingleValueString(
              "common", "common.label.required") +
          "*",
      onChange: (String value) {
        if (selfMedication == false) {
          selfMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents
              .sendAnalyticsEvent(AnalyticsEventConstant.au_dailymed_entered);
        }
      },
    );

    manageMedicationRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString("common", "common.all.yes")
            .toUpperCase(),
        LocalizationUtils.getSingleValueString("common", "common.all.no")
            .toUpperCase(),
      ],
      labelText: LocalizationUtils.getSingleValueString(
          "signup", "signup.almostdone.loved-one-medications"),
      errorText: LocalizationUtils.getSingleValueString(
              "common", "common.label.required") +
          "*",
      onChange: (String value) {
        if (manageMedication == false) {
          manageMedication = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.au_iscaregiver_entered);
        }
      },
    );

    genderRadioGroup = PPRadioGroup(
      radioOptions: [
        LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.gender-male")
            .toUpperCase(),
        LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.gender-female")
            .toUpperCase(),
        LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.gender-other")
            .toUpperCase()
      ],
      labelText: LocalizationUtils.getSingleValueString(
          "signup", "signup.fields.gender-label"),
      errorText: LocalizationUtils.getSingleValueString(
              "common", "common.label.required") +
          "*",
      onChange: (String value) {
        if (gender == false) {
          gender = true;
          final Analytics analyticsEvents = locator<Analytics>();
          analyticsEvents
              .sendAnalyticsEvent(AnalyticsEventConstant.au_gender_entered);
        }
      },
    );
  }
}

getGenderEnglish(String value) {
  if (value ==
      LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-male")
          .toUpperCase()) {
    return "MALE";
  } else if (value ==
      LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-female")
          .toUpperCase()) {
    return "FEMALE";
  } else if (value ==
      LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-other")
          .toUpperCase()) {
    return "OTHER";
  }
}

String getGenderValue(String gender) {
  String genderValue;
  switch (gender) {
    case "MALE":
      genderValue = LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-male")
          .toUpperCase();
      break;
    case "FEMALE":
      genderValue = LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-female")
          .toUpperCase();
      break;
    case "OTHER":
      genderValue = LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.gender-other")
          .toUpperCase();
      break;
  }
  return genderValue;
}
