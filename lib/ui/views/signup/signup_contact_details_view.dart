import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SignUpContactDetailsWidget extends StatefulWidget {
  static const routeName = 'signupContactDetailsView';
  final BaseStepperSource source;

  SignUpContactDetailsWidget({Key key, this.source}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpContactDetailsState();
  }
}

class SignUpContactDetailsState extends BaseState<SignUpContactDetailsWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  final _contactDetailsformKey = GlobalKey<FormState>();
  String dropDownValue = null;
  String dropDownError;
  String prevMobileNumber = "";
  String phState = '';

  bool manitobaCheck = true;
  bool autovalidate = false;
  bool phoneAlreadyRegister = false;

  static bool referralCode = false;

  final TextEditingController _invitationCodeController = TextEditingController();

  final TextEditingController _phoneNumberController = TextEditingController();

  final FocusNode _passwordFnode = FocusNode();
  final FocusNode _emailFnode = FocusNode();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  static bool provincial = false;

  @override
  void initState() {
    super.initState();
    _invitationCodeController.addListener(referralListener);
    _phoneNumberController.addListener(phoneNumberListener);
  }

  @override
  void dispose() {
    _invitationCodeController.removeListener(referralListener);
    _phoneNumberController.removeListener(phoneNumberListener);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    dataStore.readString(DataStoreService.PROVINCE) != null ? dropDownValue = dataStore.readString(DataStoreService.PROVINCE).toLowerCase() : " ";
  }

  referralListener() {
    if (referralCode == false) {
      referralCode = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_referralcode_entered);
    }
  }

  phoneNumberListener() async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      FocusScope.of(context).requestFocus(_passwordFnode);
      PhoneVerification checkPhone = await Provider.of<SignUpModel>(context).verifyPhone(prevMobileNumber);
      if (checkPhone.redirect == PhoneNumberStateConstant.LOGIN || checkPhone.redirect == PhoneNumberStateConstant.FORGOT) {
        setState(() {
          phoneAlreadyRegister = true;
          phState = checkPhone.redirect;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phoneAlreadyRegister = false;
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;

    if (_phoneNumberController.text.length == SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
    }
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: ChangeNotifierProvider(
        create: (_) => SignUpAboutYouModel(),
        child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget child) {
          return FutureBuilder(
              future: model.getLocalization(["signup"]),
              builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(model);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Widget getMainView(SignUpAboutYouModel model) {
    return Scaffold(
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? SignUpStepperAppBar(
              appBar: AppBar(),
              step: "3/5",
            )
          : null,
      bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: model.state == ViewState.Busy
                    ? ViewConstants.progressIndicator
                    : PrimaryButton(
                        text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                        onPressed: () {
                          onNextClick(context, model);
                        },
                        fullWidth: true),
              )),
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(child: getStartedView(model));
            },
          ),
        ),
      ),
    );
  }

  Widget getStartedView(SignUpAboutYouModel model) {
    return getContactDetailsView(model);
  }

  Widget getContactDetailsView(SignUpAboutYouModel model) {
    return Form(
      key: _contactDetailsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                  child: Column(
                    children: <Widget>[
                      PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.fields.contact-title")),
                      SizedBox(height: SMALL_X),
                      PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.fields.contact-description"), isBold: false),
                      SizedBox(height: MEDIUM_XXX),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.only(
                    left: MEDIUM_XXX,
                    right: MEDIUM_XXX,
                  ),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                    SizedBox(height: MEDIUM_XX),
                    getUsernameField(model),
                    phoneAlreadyRegister == true
                        ? Column(
                            children: <Widget>[
                              SizedBox(height: SMALL),
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: GestureDetector(
                                    onTap: () async {
                                      onClickCellPhoneAlreadyRegister(model);
                                    },
                                    child: RichText(
                                      text: TextSpan(
                                        text: LocalizationUtils.getSingleValueString("signup", "signup.fields.cellphone-already"),
                                        style: MEDIUM_XX_RED_BOLD,
                                        children: <TextSpan>[
                                          TextSpan(
                                            text: phState == PhoneNumberStateConstant.LOGIN
                                                ? LocalizationUtils.getSingleValueString("signup", "signup.fields.login-instead")
                                                : LocalizationUtils.getSingleValueString("signup", "signup.fields.password-instead"),
                                            style: MEDIUM_XX_LINK,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ))
                            ],
                          )
                        : Text(
                            LocalizationUtils.getSingleValueString("signup", "signup.fields.code-sent"),
                            style: MEDIUM_XX_SECONDARY,
                          ),
                    SizedBox(height: REGULAR_XXX),
                    PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.fields.provience-label"), isBold: true),
                    PPUIHelper.verticalSpaceSmall(),
                    getDropDown(),
                    dropDownError == null ? SizedBox(height: MEDIUM_XXX) : PPTexts.getFormError(dropDownError, color: errorColor),
                    SizedBox(height: MEDIUM),
                  ])),
              getManitobaCheck(),
              SizedBox(height: MEDIUM_XXX),
            ],
          ),
        ],
      ),
    );
  }

  void onClickCellPhoneAlreadyRegister(SignUpAboutYouModel model) async {
    if (phState == PhoneNumberStateConstant.LOGIN) {
      Navigator.pushNamedAndRemoveUntil(context, LoginWidget.routeName, (Route<dynamic> route) => false,
          arguments: LoginArguments(phone: _phoneNumberController.text != null ? _phoneNumberController.text : "", source: BaseStepperSource.ABOUT_YOU_SCREEN));
    } else if (phState == PhoneNumberStateConstant.FORGOT) {
      var success = await model.getOtp(_phoneNumberController.text);
      if (success == true) {
        Navigator.pushNamedAndRemoveUntil(context, LoginVerificationWidget.routeName, (Route<dynamic> route) => false,
            arguments: VerificationArguments(
              phoneNo: int.parse(_phoneNumberController.text),
            ));
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }

  onNextClick(context, SignUpAboutYouModel signUpAboutYouModel) async {
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }

    if (phoneAlreadyRegister == false) {
      onClickNextContactView(context, signUpAboutYouModel);
    }
  }

  onClickNextContactView(context, SignUpAboutYouModel signUpAboutYouModel) async {
    if (_contactDetailsformKey.currentState.validate() && dropDownValue != null) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      var success = await signUpAboutYouModel.updateAboutYou(
        province: dropDownValue,
        phone: _phoneNumberController.text,
      );
      if (success) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_signup_details);
        Navigator.of(context)
            .pushNamedAndRemoveUntil(SignUpOtpPasswordWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
      } else {
        onFail(context, errMessage: signUpAboutYouModel.errorMessage);
      }
    } else if (dropDownValue == null) {
      setState(() {
        dropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.select-province-error");
      });
    }
  }

  Widget getManitobaCheck() {
    if (dropDownValue == "manitoba") {
      return InkWell(
        onTap: () {
          setState(() {
            manitobaCheck = !manitobaCheck;
          });
        },
        child: Column(children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Checkbox(
                    value: manitobaCheck,
                    activeColor: brandColor,
                    onChanged: (bool value) {
                      setState(() {
                        manitobaCheck = !manitobaCheck;
                      });
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.fields.packpermission-label"),
                    overflow: TextOverflow.clip,
                    style: TextStyle(height: 1.4, color: secondaryColor, fontSize: 14, fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: MEDIUM),
        ]),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]))).toList(),
        onChanged: (selectedItem) => setState(() {
          FocusScope.of(context).requestFocus(_emailFnode);
          if (provincial == false) {
            provincial = true;
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_province_entered);
          }
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]))).toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
  }

  TextFormField getUsernameField(SignUpAboutYouModel model) {
    return PPFormFields.getNumericFormField(
        maxLength: 10,
        keyboardType: TextInputType.phone,
        minLength: 10,
        textInputAction: TextInputAction.done,
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocusNode,
        onFieldSubmitted: (value) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          onNextClick(context, model);
        },
        validator: (value) {
          if (value.isEmpty) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-phone");
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error1");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error2");
          return null;
        });
  }
}
