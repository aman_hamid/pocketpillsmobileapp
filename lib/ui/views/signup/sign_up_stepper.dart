import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/sign_up_stepper_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/custom_stepper.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_user_contact_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/success_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/route/CustomRoute.dart';
import 'package:provider/provider.dart';

class SignupStepper extends StatefulWidget {
  static const String routeName = 'signupStepper';

  int position;
  BaseStepperSource source;

  SignupStepper({this.position, this.source = BaseStepperSource.UNKNOWN_SCREEN});

  @override
  SignupStepperState createState() {
    return SignupStepperState();
  }
}

class SignupStepperState extends BaseState<SignupStepper> {
  final DataStoreService dataStore = locator<DataStoreService>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1),
        () => {Provider.of<SignupStepperModel>(context).setCurrentStep(widget.position), Provider.of<VitaminsSubscriptionModel>(context, listen: false).clearVitaminList()});
    super.initState();
  }

  bool isDoubleLine = false;

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context).size.width <= 400 ? isDoubleLine = true : isDoubleLine = false;
    return Consumer<SignupStepperModel>(builder: (BuildContext context, SignupStepperModel stepperModel, Widget child) {
      return FutureBuilder(
          future: stepperModel.getLocalization(["stepper"]),
          builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
            if (snapshot.hasData != null && snapshot.data != null) {
              return getMainView(stepperModel);
            } else if (snapshot.hasError) {
              return ErrorScreen();
            } else {
              return LoadingScreen();
            }
          });
    });
  }

  Widget getMainView(SignupStepperModel stepperModel) {
    return Scaffold(
        key: _scaffoldKey,
        body: CustomStepper(
          controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
            return PPContainer.emptyContainer();
          },
          type: CustomStepperType.horizontal,
          currentStep: stepperModel.currentStep,
          onStepContinue: stepperModel.currentStep < 3 ? () => stepperModel.incrCurrentStep() : null,
          onStepCancel: stepperModel.currentStep > 0 ? () => stepperModel.decrCurrentStep() : null,
          steps: <CustomStep>[
            CustomStep(
              title: Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Text(
                  LocalizationUtils.getSingleValueString("stepper", "stepper.labels.personal"),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 14),
                ),
              ),
              content: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: SignupWidget(
                  onBack: onBack(context, stepperModel),
                  onSuccess: onSuccess(context, stepperModel),
                  onShowSnackBar: (value) => {(value != null && value != "") ? showSnackBar(value) : null},
                ),
              ),
              isActive: stepperModel.currentStep >= 0,
              state: stepperModel.currentStep == 0 ? CustomStepState.editing : CustomStepState.complete,
            ),
            CustomStep(
              title: new Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.pharmacy"),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
              content: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: TransferWidget(
                    source: BaseStepperSource.NEW_USER,
                    onBack: onBack(context, stepperModel),
                    onSuccess: onSuccess(context, stepperModel),
                  )),
              isActive: stepperModel.currentStep >= 1,
              state: stepperModel.currentStep == 1 ? CustomStepState.editing : (stepperModel.currentStep >= 1 ? CustomStepState.complete : CustomStepState.disabled),
            ),
            CustomStep(
              title: new Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.almostdone"),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
              content: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: SignUpAlmostDoneWidget(
                    onBack: onBack(context, stepperModel),
                    onSuccess: onSuccess(context, stepperModel),
                    onShowSnackBar: (value) => {(value != null && value != "") ? showSnackBar(value) : null},
                  )),
              isActive: stepperModel.currentStep >= 2,
              state: stepperModel.currentStep == 2 ? CustomStepState.editing : (stepperModel.currentStep >= 2 ? CustomStepState.complete : CustomStepState.disabled),
            ),
            CustomStep(
              title: Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: Text(
                  LocalizationUtils.getSingleValueString("stepper", "stepper.labels.free_vitamins"),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 14),
                ),
              ),
              subtitle: Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.20-off"),
                textAlign: TextAlign.right,
                style: TextStyle(fontSize: 10),
              ),
              icon: Icon(
                Icons.card_giftcard,
                color: stepperModel.currentStep >= 3 ? brandColor : secondaryColor,
              ),
              content: SizedBox(height: MediaQuery.of(context).size.height, child: getStepperBasedOntransferFlow(stepperModel)),
              isActive: stepperModel.currentStep >= 3,
              state: stepperModel.currentStep >= 3 ? CustomStepState.complete : CustomStepState.disabled,
            ),
          ],
        ));
  }

  showSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      duration: Duration(seconds: 3),
    ));
  }

  Widget getStepperBasedOntransferFlow(SignupStepperModel stepperModel) {
    bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
    if (transferSkipped == true) {
      return VitaminsWidget(
        source: BaseStepperSource.NEW_USER,
      );
    } else {
      return SignUpSuccessWidget(
        source: BaseStepperSource.NEW_USER,
      );
    }
  }

  onBack(BuildContext context, SignupStepperModel stepperModel) {
    return () async {
      stepperModel.decrCurrentStep();
    };
  }

  onSuccess(BuildContext context, SignupStepperModel stepperModel) {
    return () async {
      bool transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
      if (stepperModel.currentStep < 3) {
        if (stepperModel.currentStep == 2 && transferSkipped == true) {
          Navigator.push(context, new CustomRoute(builder: (context) => new SignupUserContactWidget()));
        } else {
          stepperModel.incrCurrentStep();
        }
        analyticsEvents.setUserIdentifiers();
      } else {
        stepperModel.resetWithoutNotify();
      }
    };
  }
}
