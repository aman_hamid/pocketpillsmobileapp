import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class TransferArguments {
  final UserPatient userPatient;
  final BaseStepperSource source;
  final String snackBarMessage;
  final SignUpTransferModel model;
  final String medicineName;
  final int quantity;
  final String gender;
  final String PharmacyNamePopular;
  bool chambersFlow;

  TransferArguments(
      {this.userPatient,
      this.source,
      this.snackBarMessage,
      this.model,
      this.medicineName,
      this.quantity,
      this.gender,
      this.PharmacyNamePopular,
      this.chambersFlow});
}
