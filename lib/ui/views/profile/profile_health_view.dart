import 'package:basic_utils/basic_utils.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/health_info.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_health_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_inputchiplist.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ProfileHealthView extends StatefulWidget {
  final Function onSuccess;
  final bool noPadding;
  final Function onBack;
  final int position;
  final bool showStepperText;

  ProfileHealthView(
      {Key key,
      this.onBack,
      this.onSuccess,
      this.noPadding = false,
      this.showStepperText = false,
      this.position = 0})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProfileHealthState();
  }
}

class _ProfileHealthState extends BaseState<ProfileHealthView> {
  Key counterSupplementsKey;
  Key allergicMedsKey;

  @override
  void initState() {
    super.initState();
    counterSupplementsKey = UniqueKey();
    allergicMedsKey = UniqueKey();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: widget.position > 0
            ? widget.onBack
            : () async {
                Navigator.pop(context);
                return true;
              },
        child: Consumer<ProfileHealthModel>(
          builder: (BuildContext context, ProfileHealthModel profileHealthModel,
              Widget child) {
            return FutureBuilder(
              future: myFutureMethodOverall(profileHealthModel, context),
              // ignore: missing_return
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (profileHealthModel.connectivityResult ==
                        ConnectivityResult.none &&
                    snapshot.connectionState == ConnectionState.done) {
                  return NoInternetScreen(
                    onClickRetry: profileHealthModel.clearAsyncMemorizer,
                  );
                }

                if (snapshot.hasData == true &&
                    profileHealthModel.connectivityResult !=
                        ConnectivityResult.none) {
                  return _profileHealthBuild(context, profileHealthModel);
                } else if (snapshot.hasError &&
                    profileHealthModel.connectivityResult !=
                        ConnectivityResult.none) {
                  Crashlytics.instance.log(snapshot.hasError.toString());
                  return ErrorScreen();
                }

                if (snapshot.connectionState == ConnectionState.active ||
                    snapshot.connectionState == ConnectionState.waiting) {
                  return LoadingScreen();
                }
              },
            );
          },
        ));
  }

  Future myFutureMethodOverall(
      ProfileHealthModel profileHealthModel, BuildContext context) async {
    Future<HealthInfo> future1 = profileHealthModel.fetchHealthData(
        Provider.of<DashboardModel>(context)
            .selectedPatientId); // will take 1 sec
    Future<Map<String, dynamic>> future2 = profileHealthModel
        .getLocalization(["order-checkout"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  //// ${PatientUtils.getYouOrName(Provider.of<DashboardModel>(context).selectedPatient)} like to receive ${PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context).selectedPatient)}  ${StringUtils.capitalize(PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context).selectedPatient))}?",
  Widget _profileHealthBuild(
      BuildContext context, ProfileHealthModel profileHealthModel) {
    PPInputChipList allergiesChipList;
    PPInputChipList vitaminsChipList;
    allergiesChipList = PPInputChipList(
        key: allergicMedsKey,
        chips: profileHealthModel.healthInfo.allergies == null
            ? []
            : profileHealthModel.healthInfo.allergies,
        totalList: ViewConstants.ALLERGIES);
    vitaminsChipList = PPInputChipList(
        key: counterSupplementsKey,
        chips: profileHealthModel.healthInfo.vitamins == null
            ? []
            : profileHealthModel.healthInfo.vitamins,
        totalList: []);
    return Scaffold(
        bottomNavigationBar: Builder(
            builder: (BuildContext context) =>
                profileHealthModel.state == ViewState.Busy
                    ? ViewConstants.progressIndicator
                    : PPBottomBars.getButtonedBottomBar(
                        child: widget.onSuccess != null
                            ? getSaveButtons(profileHealthModel,
                                allergiesChipList, vitaminsChipList)
                            : getBottomButtom(profileHealthModel,
                                allergiesChipList, vitaminsChipList))),
        body: GestureDetector(
          onTap: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          child: Padding(
            padding: widget.noPadding == true
                ? EdgeInsets.all(0)
                : EdgeInsets.symmetric(
                    horizontal: PPUIHelper.HorizontalSpaceMedium,
                    vertical: MEDIUM),
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPTexts.getMainViewHeading(
                        LocalizationUtils.getSingleValueString("order-checkout",
                            "order-checkout.main.title-health")),
                    PPUIHelper.verticalSpaceXSmall(),
                    Text(
                      getDescriptionText(PatientUtils.getForGender(
                          Provider.of<DashboardModel>(context)
                              .selectedPatient)),
                      style: MEDIUM_XX_SECONDARY,
                    ),
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPRadioGroup(
                      radioOptions: [
                        LocalizationUtils.getSingleValueString(
                                "common", "common.labels.yes")
                            .toUpperCase(),
                        LocalizationUtils.getSingleValueString(
                                "common", "common.labels.no")
                            .toUpperCase()
                      ],
                      initialValue: profileHealthModel.radioOption > 0
                          ? LocalizationUtils.getSingleValueString(
                                  "common", "common.labels.yes")
                              .toUpperCase()
                          : LocalizationUtils.getSingleValueString(
                                  "common", "common.labels.no")
                              .toUpperCase(),
                      labelText: getLabelText(context),
                      errorText: LocalizationUtils.getSingleValueString(
                              "common", "common.label.required") +
                          "*",
                      onChange: (String value) {
                        if (value ==
                            LocalizationUtils.getSingleValueString(
                                    "common", "common.labels.no")
                                .toUpperCase()) {
                          profileHealthModel.setRadioOption(0);
                        } else {
                          profileHealthModel.setRadioOption(1);
                        }
                      },
                    ),
                    PPUIHelper.verticalSpaceSmall(),
                    profileHealthModel.radioOption > 0
                        ? Column(
                            children: <Widget>[
                              PPTexts.getFormLabel(getMedicationText(
                                  PatientUtils.getForGender(
                                      Provider.of<DashboardModel>(context)
                                          .selectedPatient))),
                              PPUIHelper.verticalSpaceSmall(),
                              allergiesChipList,
                              SizedBox(
                                height: REGULAR_X,
                              ),
                            ],
                          )
                        : Container(),
                    PPTexts.getFormLabel(getSupplementsText(
                        PatientUtils.getForGender(
                            Provider.of<DashboardModel>(context)
                                .selectedPatient))),
                    PPUIHelper.verticalSpaceSmall(),
                    vitaminsChipList,
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                    PPUIHelper.verticalSpaceLarge(),
                    PPUIHelper.verticalSpaceLarge(),
                    PPUIHelper.verticalSpaceLarge(),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget getSaveButtons(ProfileHealthModel profileHealthModel,
      PPInputChipList allergiesChipList, PPInputChipList vitaminsChipList) {
    Widget withoutStepper = PrimaryButton(
      text: LocalizationUtils.getSingleValueString(
              "common", "common.button.continue")
          .toUpperCase(),
      fullWidth: false,
      onPressed: () {
        onClickSave(profileHealthModel, allergiesChipList, vitaminsChipList);
      },
    );
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SecondaryButton(
          text: LocalizationUtils.getSingleValueString(
                  "common", "common.button.back")
              .toUpperCase(),
          onPressed: widget.onBack,
        ),
        SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
        withoutStepper
      ],
    );
  }

  Widget getBottomButtom(ProfileHealthModel profileHealthModel,
      PPInputChipList allergiesChipList, PPInputChipList vitaminsChipList) {
    return PrimaryButton(
      text:
          LocalizationUtils.getSingleValueString("common", "common.button.save")
              .toUpperCase(),
      fullWidth: true,
      onPressed: () {
        onClickSave(profileHealthModel, allergiesChipList, vitaminsChipList);
      },
    );
  }

  void onClickSave(
      ProfileHealthModel profileHealthModel,
      PPInputChipList allergiesChipList,
      PPInputChipList vitaminsChipList) async {
    bool connectivityResult = await profileHealthModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profileHealthModel.noInternetConnection);
      return;
    }
    String f = allergiesChipList.getCurrentSelection().toString();
    print(f + "rrrrrrrr");
    bool res = await profileHealthModel.setPatientHealth(
        profileHealthModel.radioOption > 0 ? allergiesChipList.chips : [],
        vitaminsChipList.chips);
    if (res == true) {
      if (widget.onSuccess == null)
        showOnSnackBar(context,
            successMessage: LocalizationUtils.getSingleValueString(
                "order-checkout", "order-checkout.main.details-updated"));
      if (widget.onSuccess != null) widget.onSuccess();
    } else
      onFail(context, errMessage: profileHealthModel.errorMessage);
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.main.description-health-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.main.description-health-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.main.description-health-other");
      default:
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.main.description-health");
    }
  }

  String getMedicationText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.medications-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.medications-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.medications-other");
      default:
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.medications");
    }
  }

  String getSupplementsText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.supplements-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.supplements-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.supplements-other");
      default:
        return LocalizationUtils.getSingleValueString(
            "order-checkout", "order-checkout.labels.supplements");
    }
  }

  String getLabelText(BuildContext context) {
    if (Provider.of<DashboardModel>(context).selectedPatient == null ||
        Provider.of<DashboardModel>(context).selectedPatient.primary == true) {
      return LocalizationUtils.getSingleValueString(
          "order-checkout", "order-checkout.labels.allergies");
    } else {
      return LocalizationUtils.getSingleValueString(
              "order-checkout", "order-checkout.labels.allergies-secondary")
          .replaceAll(
              "{{name}}",
              PatientUtils.getThey(
                  Provider.of<DashboardModel>(context).selectedPatient));
    }
  }
}
