class StartViewArguments {
  final String deepLinkRouteName;
  final int carouselIndex;
  final bool chambersFlow;

  StartViewArguments(
      {this.deepLinkRouteName, this.carouselIndex, this.chambersFlow});
}
