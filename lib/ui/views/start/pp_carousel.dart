import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PPCarousel extends StatefulWidget {
  final int carouselIndex;

  PPCarousel({this.carouselIndex});

  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends BaseState<PPCarousel> {
  int _current = 0;
  List imgList;
  List headings;
  List descriptions;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {});
    _current = widget.carouselIndex < 7 ? widget.carouselIndex : 0;
  }

  final DataStoreService dataStore = locator<DataStoreService>();

  List<Widget> getCarouselItems() {
    getListItems();
    List<Widget> itemList = [];
    for (int i = 0; i < 6; i++) {
      itemList.add(
        Container(
          width: MediaQuery.of(context).size.width,
          child: Stack(children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 0.0, horizontal: 16.0),
                  child: Image.asset(
                    imgList[i],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PPUIHelper.HorizontalSpaceMedium,
                      vertical: SMALL_XX),
                  child: Text(
                    headings[i],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: REGULAR_XX *
                            (MediaQuery.of(context).devicePixelRatio * 0.4),
                        fontWeight: FontWeight.bold,
                        height: 1.0,
                        color: primaryColor),
                  ),
                ),
                PPUIHelper.verticalSpaceXSmall(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PPUIHelper.HorizontalSpaceMedium),
                  child: Text(
                    descriptions[i],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 11 *
                            (MediaQuery.of(context).devicePixelRatio * 0.4),
                        height: 1.0,
                        color: secondaryColor),
                  ),
                ),
              ],
            ),
          ]),
        ),
      );
    }
    return itemList;
  }

  List<Widget> getCarouselDots() {
    List<Widget> itemList = [];
    for (int i = 0; i < 6; i++) {
      itemList.add(Container(
        width: 8.0,
        height: 8.0,
        margin: EdgeInsets.symmetric(horizontal: 6.0),
        decoration: _current == i
            ? BoxDecoration(shape: BoxShape.circle, color: brandColor)
            : BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xffffffff),
                border: Border.all(color: brandColor)),
      ));
    }
    return itemList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            color: Colors.white,
            child: CarouselSlider(
              autoPlay: false,
              autoPlayInterval: Duration(seconds: 3),
              height: MediaQuery.of(context).size.height * .5,
              items: getCarouselItems(),
              //aspectRatio: 2.0,
              initialPage: widget.carouselIndex,
              viewportFraction: 1.0,
              pauseAutoPlayOnTouch: Duration(minutes: 60),
              reverse: false,
              scrollDirection: Axis.horizontal,
              onPageChanged: (index) {
                setState(
                  () {
                    _current = index;
                  },
                );
                switch (_current) {
                  case 0:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel1);
                    break;
                  case 1:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel2);
                    break;
                  case 2:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel3);
                    break;
                  case 3:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel4);
                    break;
                  case 4:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel4);
                    break;
                  case 5:
                    analyticsEvents
                        .sendAnalyticsEvent(AnalyticsEventConstant.carousel4);
                    break;
                }
              },
            ),
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: getCarouselDots()),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  void getListItems() {
    imgList = [
      'graphics/apptour/apptour_img1.png',
      'graphics/apptour/apptour_img2.png',
      'graphics/apptour/apptour_img3.png',
      'graphics/apptour/apptour_img4.png',
      'graphics/apptour/apptour_img5.png',
      'graphics/apptour/apptour_img6.png',
    ];

    headings = [
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header1"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header2"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header3"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header4"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header5"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.header6"),
    ];
    descriptions = [
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description1"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description2"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description3"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description4"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description5"),
      LocalizationUtils.getSingleValueString(
          "app-landing", "app-landing.tour.description6"),
    ];
  }
}
