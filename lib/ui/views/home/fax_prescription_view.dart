import 'package:flutter/material.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class FaxPrescriptionWidget extends StatefulWidget {
  static const routeName = 'faxprescription';

  FaxPrescriptionWidget({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FaxPrescriptionState();
  }
}

class FaxPrescriptionState extends BaseState<FaxPrescriptionWidget> {
  @override
  void initState() {
    super.initState();
    PillReminderDayModel model = PillReminderDayModel();
    getLanguageData(model);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String name = PatientUtils.getYouOrNameTitle(Provider.of<DashboardModel>(context).selectedPatient);
    name = '${name[0].toUpperCase()}${name.substring(1)}';
    return Scaffold(
      appBar: InnerAppBar(
        titleText: name,
        appBar: AppBar(),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            PPUIHelper.verticalSpaceMedium(),
                            PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.fax-title")),
                            PPUIHelper.verticalSpaceSmall(),
                            PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.description").replaceAll("{{name}}", name),
                                isBold: false),
                            PPUIHelper.verticalSpaceMedium(),
                          ],
                        ),
                      ),
                    ),
                    PPCard(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                  child: Column(
                                children: <Widget>[
                                  PPTexts.getDescription(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.pharmacy-name"), isBold: true)
                                ],
                              )),
                              Expanded(
                                  child: Column(
                                children: <Widget>[
                                  PPTexts.getDescription(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.pharmacy-name"), isBold: true)
                                ],
                              ))
                            ],
                          ),
                          PPUIHelper.verticalSpaceSmall(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                  child: Column(
                                children: <Widget>[PPTexts.getHeading("PocketPills")],
                              )),
                              Expanded(
                                  child: Column(
                                children: <Widget>[PPTexts.getHeading(ApplicationConstant.care_number)],
                              ))
                            ],
                          )
                        ],
                      ),
                    ),
                    PPUIHelper.verticalSpaceMedium(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            PPUIHelper.verticalSpaceLarge(),
                            PPTexts.getHeading(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.expect-next")),
                            PPUIHelper.verticalSpaceSmall(),
                            PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.fax.expect-next-description")
                                .replaceAll("{{name}}", PatientUtils.getPronounForGender(Provider.of<DashboardModel>(context).selectedPatient))),
                            PPUIHelper.verticalSpaceMedium(),
                            PrimaryButton(
                              fullWidth: true,
                              text: LocalizationUtils.getSingleValueString("common", "app-dashboard.fax.go-home"),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

void getLanguageData(PillReminderDayModel model) {
  FutureBuilder(
      future: myFutureMethodOverall(model),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null) {
          return null;
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      });
}

Future myFutureMethodOverall(PillReminderDayModel model) async {
  Future<Map<String, dynamic>> future1 = model.getLocalization(["app-dashboard"]);
  return await Future.wait([future1]);
}
