import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/loader/shimmer_loader.dart';

class HomeShimmerLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          ShimmerLoader.fromColors(
            baseColor: borderColor,
            highlightColor: quartiaryColor,
            child: Column(
              children: <Widget>[
                PPCard(
                  child: Container(
                    width: MediaQuery.of(context).size.width - LARGE_X,
                    height: LARGE_X,
                    color: errorColor,
                  ),
                ),
                SizedBox(
                  height: REGULAR,
                ),
                PPCard(
                  child: Container(
                    width: MediaQuery.of(context).size.width - LARGE_X,
                    height: LARGE_X,
                    color: errorColor,
                  ),
                ),
                SizedBox(
                  height: REGULAR,
                ),
                PPCard(
                  padding: EdgeInsets.all(SMALL_XX),
                  child: Container(
                    width: MediaQuery.of(context).size.width - LARGE_X,
                    height: MEDIUM_XXX,
                    color: errorColor,
                  ),
                ),
                SizedBox(
                  height: MEDIUM_XXX,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: PPCard(
                        child: Container(
                          width: MediaQuery.of(context).size.width - LARGE_X,
                          height: (MediaQuery.of(context).size.width - 96) / 2,
                          color: errorColor,
                        ),
                      ),
                    ),
                    Expanded(
                      child: PPCard(
                        child: Container(
                          width: MediaQuery.of(context).size.width - LARGE_X,
                          height: (MediaQuery.of(context).size.width - 96) / 2,
                          color: errorColor,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: REGULAR,
          ),
        ],
      ),
    );
  }
}
