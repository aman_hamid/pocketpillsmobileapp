import 'dart:io';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/dashboard/dashboard_item_list.dart';
import 'package:pocketpills/core/dashboard/dashboard_response.dart';
import 'package:pocketpills/core/dashboard/header.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_action_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_detail_tag_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_error_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_image_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_scroll_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_long_tag_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_pill_reminder_card_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_square_card_view.dart';
import 'package:pocketpills/ui/views/home/home_shimmer_loader.dart';
import 'package:pocketpills/ui/views/signup/telehealth_success_bottom_sheet.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';
import 'package:provider/provider.dart';
import 'dashboard_long_button_card_view.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';

class HomeWidget extends StatefulWidget {
  static const routeName = 'home';
  final UserPatient userPatient;

  HomeWidget({Key key, this.userPatient}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends BaseState<HomeWidget> {
  List<Medicine> medicinePredictions = List();
  GlobalKey<AutoCompleteTextFieldState<Medicine>> key = new GlobalKey();
  bool medicineSubmitted = false;
  String prevMedicineName;
  bool isDoubleLine = false;

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dashboard);
    super.initState();
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Consumer<HomeModel>(
        builder: (BuildContext context, HomeModel homeModel, Widget child) {
          return Scaffold(
              backgroundColor: lightBlue2Color,
              body: Builder(
                builder: (BuildContext context) {
                  return Stack(
                    children: <Widget>[
                      ListView(
                        shrinkWrap: true,
                        children: <Widget>[
                          SizedBox(
                            height: REGULAR,
                          ),
                          getDashboardData(context, homeModel),
                          SizedBox(
                            height: REGULAR,
                          ),
                        ],
                      ),
                      homeModel.state == ViewState.Busy
                          ? Container(
                              color: Color(0x40ffffff),
                              child: Center(
                                  child: ViewConstants.progressIndicator))
                          : PPContainer.emptyContainer(),
                    ],
                  );
                },
              ));
        },
      ),
    );
  }

  Widget getDashboardData(BuildContext context, HomeModel homeModel) {
    return FutureBuilder(
      future: homeModel.fetchDashboardData(
          Provider.of<DashboardModel>(context).selectedPatientId, context),
      builder:
          (BuildContext context, AsyncSnapshot<DashboardResponse> snapshot) {
        if (homeModel.connectivityResult == ConnectivityResult.none &&
            snapshot.connectionState == ConnectionState.done) {
          return Padding(
            padding: const EdgeInsets.all(MEDIUM_XXX),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  homeModel.noInternetConnection,
                  style: MEDIUM_XX_RED_BOLD,
                )),
          );
        }

        if (snapshot.hasData &&
            homeModel.connectivityResult != ConnectivityResult.none) {
          if (dataStore.readBoolean(DataStoreService.DASHBOARD_TELEHEALTH) ==
              true) {
            Future.delayed(
                Duration(milliseconds: 0),
                () => {
                      print("here DASHBOARD_TELEHEALTH"),
                      showBottomSheet(context, "DASHBOARD_TELEHEALTH"),
                    });
          }
          if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) ==
              true) {
            Future.delayed(
                Duration(milliseconds: 0),
                () => {
                      print("here SIGNUP_TELEHEALTH"),
                      showBottomSheet(context, "SIGNUP_TELEHEALTH"),
                    });
          }

          if (dataStore.readBoolean(DataStoreService.SIGNUP_PRESCRIPTION) ==
              true) {
            Future.delayed(
                Duration(milliseconds: 0),
                () => {
                      print("here SIGNUP_PRESCRIPTION"),
                      showBottomSheet(context, "SIGNUP_PRESCRIPTION"),
                    });
          }
          return getDashboardWidgets(context, snapshot.data, homeModel);
        } else if (snapshot.hasError &&
            homeModel.connectivityResult != ConnectivityResult.none) {
          return Text(LocalizationUtils.getSingleValueString(
              "common", "common.label.server-error-description"));
        }

        if (snapshot.connectionState == ConnectionState.active ||
            snapshot.connectionState == ConnectionState.waiting) {
          return HomeShimmerLoader();
        }
        return PPContainer.emptyContainer();
      },
    );
  }

  // Long and Item card are similar and category division given below
  Widget getDashboardWidgets(
      BuildContext context, DashboardResponse response, HomeModel homeModel) {
    if (response.dashboardItemList != null &&
        response.dashboardItemList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          primary: false,
          scrollDirection: Axis.vertical,
          itemCount: response.dashboardItemList.length,
          itemBuilder: (BuildContext context, int index) {
            DashboardItemList dashboardItemList =
                response.dashboardItemList[index];
            switch (dashboardItemList.getLayoutType()) {
              case LayoutType.LONG:
              case LayoutType.ITEM:
                return getLongCardView(dashboardItemList, homeModel);
                break;
              case LayoutType.GRID:
                return getSquareCard(dashboardItemList, homeModel);
                break;
              case LayoutType.SCROLL:
                return getLongCardScroll(dashboardItemList, homeModel);
                break;
              case LayoutType.PILL_REMINDER:
                return getPillReminderView(dashboardItemList, homeModel);
                break;
              default:
                return PPContainer.emptyContainer();
                break;
            }
          });
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getLongCardView(
      DashboardItemList dashboardItemList, HomeModel homeModel) {
    if (dashboardItemList.data.length > 0) {
      return Column(
        children: <Widget>[
          getHeadingView(dashboardItemList, homeModel),
          ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: dashboardItemList.data.length,
              itemBuilder: (BuildContext context, int index) {
                DashboardItem dashboardItem = dashboardItemList.data[index];
                //print(
                //'AmanLongCard:${dashboardItem.cardType} + ${dashboardItem.description}');
                switch (dashboardItem.getCardType()) {
                  case CardType.DEFAULT:
                    return DashboardLongDetailTagCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  case CardType.WARNING:
                    return DashboardLongErrorCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  case CardType.TAG:
                    return DashboardLongTagCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  case CardType.ACTION:
                    return DashboardLongActionCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  case CardType.IMAGE:
                    return DashboardLongImageCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  case CardType.BUTTON:
                    return DashboardLongButtonCardView(
                        dashboardItem: dashboardItem, homeModel: homeModel);
                    break;
                  default:
                    return PPContainer.emptyContainer();
                    break;
                }
              }),
        ],
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getHeadingView(
      DashboardItemList dashboardItemList, HomeModel homeModel) {
    if (dashboardItemList != null && dashboardItemList.header != null) {
      switch (dashboardItemList.header.getHeadingType()) {
        case HeadingType.PRIMARY:
          return getLayoutMainHeading(dashboardItemList.header);
          break;
        case HeadingType.SECONDARY:
          return getLayoutViewAllHeading(dashboardItemList.header, homeModel);
          break;
        default:
          return PPContainer.emptyContainer();
          break;
      }
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getLayoutMainHeading(Header header) {
    return header.heading != null
        ? Padding(
            padding: const EdgeInsets.only(
                top: REGULAR_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                header.heading ?? "",
                overflow: TextOverflow.clip,
                style: TextStyle(
                    color: primaryColor,
                    fontSize: PPUIHelper.FontSizeXXLarge,
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        : PPContainer.emptyContainer();
  }

  Widget getLayoutViewAllHeading(Header header, HomeModel homeModel) {
    return header.heading != null
        ? Padding(
            padding: const EdgeInsets.only(
                top: MEDIUM_X, left: MEDIUM_XXX, right: MEDIUM_XXX),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Text(
                  header.heading.toUpperCase() ?? "",
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                )),
                FlatButton(
                  color: Colors.transparent,
                  child: Text(
                    header.buttonText ?? "",
                    style: MEDIUM_XXX_LINK_BOLD_MEDIUM,
                  ),
                  onPressed: () {
                    homeModel.handleDashboardRoute(
                        header.getActionType(), context);
                  },
                ),
              ],
            ),
          )
        : PPContainer.emptyContainer();
  }

  Widget getLongCardScroll(
      DashboardItemList dashboardItemList, HomeModel homeModel) {
    return Column(
      children: <Widget>[
        getHeadingView(dashboardItemList, homeModel),
        DashboardLongScrollCardView(
          dashboardItemList: dashboardItemList,
          homeModel: homeModel,
        )
      ],
    );
  }

  Widget getPillReminderView(
      DashboardItemList dashboardItemList, HomeModel homeModel) {
    return DashboardPillReminderCardView(
      dashboardItemList: dashboardItemList,
      homeModel: homeModel,
    );
  }

  Widget getSquareCard(
      DashboardItemList dashboardItemList, HomeModel homeModel) {
    return Column(
      children: <Widget>[
        getHeadingView(dashboardItemList, homeModel),
        DashboardSquareCardView(
          dashboardItemList: dashboardItemList,
          homeModel: homeModel,
        )
      ],
    );
  }

  showBottomSheet(BuildContext context, String s) {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return WillPopScope(
              onWillPop: () {}, child: SuccessSheet(context, s));
        });
  }
}
