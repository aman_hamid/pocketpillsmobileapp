import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item_list.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class DashboardPillReminderCardView extends StatefulWidget {
  final DashboardItemList dashboardItemList;
  final HomeModel homeModel;

  DashboardPillReminderCardView({@required this.dashboardItemList, @required this.homeModel});

  @override
  _DashboardPillReminderCardViewState createState() => _DashboardPillReminderCardViewState();
}

class _DashboardPillReminderCardViewState extends BaseState<DashboardPillReminderCardView> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<Offset> offset;

  // Animation<Offset> reverseOffset;
  bool showNameLabel = false;
  PillReminderDayModel model;

  @override
  void initState() {
    super.initState();
    model = PillReminderDayModel();

    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));

    offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 1.0)).animate(controller);
    //reverseOffset = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset(1.0, 0.0)).animate(controller);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(PillReminderDayModel model) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization(["app-dashboard", "modal"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Consumer<PillReminderDayModel>(builder: (BuildContext context, PillReminderDayModel pillReminderDayModel, Widget child) {
      return FutureBuilder(
        future: pillReminderDayModel.fetchPillReminderDayData(),
        builder: (BuildContext context, AsyncSnapshot<DayWiseMedications> snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return _afterFutureBuild(context, pillReminderDayModel.dayWiseMedications, pillReminderDayModel);
          }
          return PPContainer.emptyContainer();
        },
      );
    });
  }

  Widget _afterFutureBuild(BuildContext context, DayWiseMedications dayWiseMedications, PillReminderDayModel pillReminderDayModel) {
    if (dayWiseMedications.pocketPackDetails != null && dayWiseMedications.pocketPackDetails.length > 0) {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: REGULAR_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_X),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.todays-medication"),
                overflow: TextOverflow.clip,
                style: MEDIUM_XXX_PRIMARY_BOLD,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.fromLTRB(MEDIUM, SMALL_X, MEDIUM, SMALL_X),
                child: IntrinsicHeight(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: (dayWiseMedications.pocketPackDetails == null && dayWiseMedications.pocketPackDetails == 0)
                          ? <Widget>[PPContainer.emptyContainer()]
                          : dayWiseMedications.pocketPackDetails
                              .map(
                                (pocketPackDetail) => Container(
                                  constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2 - 16),
                                  child: PPCard(
                                      onTap: () async {
                                        Provider.of<DashboardModel>(context).applicationStart = false;
                                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_show_daily_medication);
                                      },
                                      cardColor: whiteColor,
                                      margin: EdgeInsets.only(left: SMALL_XXX, right: SMALL_XXX),
                                      padding: EdgeInsets.all(0),
                                      child: Container(
                                          child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                child: Padding(
                                                    padding: EdgeInsets.only(left: MEDIUM_X, right: SMALL_XXX, top: MEDIUM_X, bottom: MEDIUM_X),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Text(
                                                          getDayLanguage(pocketPackDetail.partOfDay) ?? "",
                                                          style: MEDIUM_XX_PRIMARY_BOLD,
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 2,
                                                        ),
                                                        SizedBox(
                                                          height: SMALL_XX,
                                                        ),
                                                        Text(
                                                          StringUtils.getFormattedTimeHHMMNN(pocketPackDetail.time),
                                                          style: MEDIUM_XX_SECONDARY,
                                                        ),
                                                        SizedBox(
                                                          height: SMALL_XXX,
                                                        ),
                                                      ],
                                                    )),
                                              ),
                                            ],
                                          ),
                                          SlideTransition(position: offset, child: getPillReminderStatusUpdateButtonView(pillReminderDayModel, pocketPackDetail))
                                        ],
                                      ))),
                                ),
                              )
                              .toList()),
                ),
              ),
            ),
          )
        ],
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getPillReminderStatusUpdateButtonView(PillReminderDayModel pillReminderDayModel, PocketPackDetails pocketPackDetail) {
    switch (pocketPackDetail.getPillStatusType()) {
      case PillStatusType.TAKEN:
        return Container(
            color: headerBgColor,
            width: double.infinity,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: MEDIUM_XX),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: SMALL_XXX,
                          ),
                        ],
                        color: successColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(SMALL_X),
                        child: Icon(
                          Icons.check,
                          color: whiteColor,
                          size: MEDIUM_X,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: SMALL_XXX),
                      child: Text(
                        LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.you-taken"),
                        style: MEDIUM_XX_GREEN_BOLD_MEDIUM,
                      ),
                    ),
                  ],
                ),
              ),
            ));
        break;
      case PillStatusType.MISSED:
        return Container(
            color: headerBgColor,
            width: double.infinity,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: MEDIUM_XX),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: SMALL_XXX,
                          ),
                        ],
                        color: secondaryColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(SMALL_X),
                        child: Icon(
                          Icons.close,
                          color: whiteColor,
                          size: MEDIUM_X,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: SMALL_XXX),
                      child: Text(
                        LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.you-missed"),
                        style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                      ),
                    ),
                  ],
                ),
              ),
            ));
        break;
      case PillStatusType.NONE:
      case PillStatusType.FUTURE:
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
                child: InkWell(
              onTap: () async {
                await pillReminderDayModel.updatePillReminderStatus(PillStatusType.MISSED, pocketPackDetail.pocketPackIds);
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_pill_missed);
              },
              child: Container(
                color: headerBgColor,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: MEDIUM_XX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.missed"),
                      style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                    ),
                  ),
                ),
              ),
            )),
            Expanded(
                child: InkWell(
              onTap: () async {
                await pillReminderDayModel.updatePillReminderStatus(PillStatusType.TAKEN, pocketPackDetail.pocketPackIds);
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_pill_taken);
              },
              child: Container(
                color: headerBgColor,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: MEDIUM_XX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.taken"),
                      style: MEDIUM_XX_GREEN_BOLD_MEDIUM,
                    ),
                  ),
                ),
              ),
            ))
          ],
        );

        break;
    }
  }
}

getDayLanguage(String partOfDay) {
  switch (partOfDay) {
    case "MORNING":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning").toUpperCase();
      break;
    case "AFTERNOON":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon").toUpperCase();
      break;
    case "EVENING":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening").toUpperCase();
      break;
    case "BEDTIME":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.bedtime").toUpperCase();
      break;
    default:
      return partOfDay;
      break;
  }
}
