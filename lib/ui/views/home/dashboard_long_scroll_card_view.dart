import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item_list.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';

class DashboardLongScrollCardView extends StatelessWidget {
  final DashboardItemList dashboardItemList;
  final HomeModel homeModel;

  DashboardLongScrollCardView({@required this.dashboardItemList, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    if (dashboardItemList.data == null) {
      return PPContainer.emptyContainer();
    }
    int itemCount = dashboardItemList.data.length;
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.fromLTRB(MEDIUM, SMALL_X, MEDIUM, SMALL_X),
        child: IntrinsicHeight(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: itemCount == 0
                  ? <Widget>[PPContainer.emptyContainer()]
                  : dashboardItemList.data
                      .map(
                        (dashboardItem) => Container(
                          constraints: BoxConstraints(
                              maxWidth: itemCount == 1
                                  ? MediaQuery.of(context).size.width - 16
                                  : (MediaQuery.of(context).size.width - 48)),
                          child: PPCard(
                              onTap: () async {
                                homeModel.handleDashboardRoute(dashboardItem.getActionType(), context,
                                    dashboardItem: dashboardItem);
                                if (dashboardItem.trackActionComplete == true) {
                                  await homeModel.trackDashboardCard(dashboardItem.id);
                                }
                              },
                              cardColor: (dashboardItem.colorSchema != null)
                                  ? dashboardItem.colorSchema.getCardColor()
                                  : whiteColor,
                              margin: EdgeInsets.only(left: SMALL_XXX, right: SMALL_XXX),
                              padding: EdgeInsets.only(
                                  left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_X, bottom: MEDIUM_XXX),
                              child: Container(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    dashboardItem.title ?? "",
                                    style: TextStyle(
                                        color: (dashboardItem.colorSchema != null)
                                            ? dashboardItem.colorSchema.getTextColor()
                                            : darkPrimaryColor,
                                        fontSize: MEDIUM_XXX,
                                        fontWeight: FontWeight.bold,
                                        height: 1.4),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  SizedBox(
                                    height: SMALL_XX,
                                  ),
                                  Text(
                                    dashboardItem.description ?? "",
                                    style: TextStyle(
                                        color: (dashboardItem.colorSchema != null)
                                            ? dashboardItem.colorSchema.getTextColor()
                                            : darkPrimaryColor,
                                        fontSize: MEDIUM_XX,
                                        height: 1.4),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 5,
                                  ),
                                ],
                              ))),
                        ),
                      )
                      .toList()),
        ),
      ),
    );
  }
}
