import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';

class DashboardLongDetailTagCardView extends StatelessWidget {
  final DashboardItem dashboardItem;
  final HomeModel homeModel;

  DashboardLongDetailTagCardView({@required this.dashboardItem, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    return PPCard(
      onTap: () {
        homeModel.handleDashboardRoute(dashboardItem.getActionType(), context, dashboardItem: dashboardItem);
      },
      margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM),
      padding: EdgeInsets.all(MEDIUM_X),
      child: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  dashboardItem.title ?? "",
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                SizedBox(
                  height: SMALL_X,
                ),
                Text(
                  dashboardItem.description ?? "",
                  style: MEDIUM_XX_PRIMARY,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ],
            ),
          ),
          (dashboardItem.tagName != null && dashboardItem.tagName != "")
              ? Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: PPChip(
                    label: dashboardItem.tagName ?? "",
                    color: primaryColor,
                  ),
                )
              : SizedBox(
                  height: 0.0,
                )
        ],
      )),
    );
  }
}
