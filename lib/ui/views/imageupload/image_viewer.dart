import 'package:flutter/material.dart';

class ImageViewer extends StatelessWidget {
  final ImageProvider image;

  ImageViewer({@required this.image});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Image"),
        backgroundColor: Colors.black,
      ),
      body: Container(
        child: Center(child: Image(image: this.image)),
        decoration: BoxDecoration(color: Colors.black),
      ),
    );
  }
}
