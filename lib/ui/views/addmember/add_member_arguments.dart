import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class AddMemberArguments {
  final String gender;
  final AddMemberRequest addMemberRequest;

  AddMemberArguments({this.gender, this.addMemberRequest});
}
