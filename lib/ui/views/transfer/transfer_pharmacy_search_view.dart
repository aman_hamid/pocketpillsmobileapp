import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class TransferPharmacySearchView extends StatefulWidget {
  static const routeName = 'transfer_pharmacy_search_view';

  final BaseStepperSource source;
  final SignUpTransferModel model;
  final String PharmacyNamePopular ;
  TransferPharmacySearchView({Key key, this.source = BaseStepperSource.MAIN_SCREEN, this.model,this.PharmacyNamePopular}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TransferPharmacySearchViewState();
  }
}

class TransferPharmacySearchViewState extends BaseState<TransferPharmacySearchView> {
  final DataStoreService dataStore = locator<DataStoreService>();

  bool autovalidate = false;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _pharmacySearchNameController = TextEditingController();
  final TextEditingController _pharmacyNameController = TextEditingController();
  final TextEditingController _pharmacyPhoneController = TextEditingController();
  final TextEditingController _pharmacyAddressController = TextEditingController();
  final TextEditingController _commentsController = TextEditingController();

  bool newUserFlow = false;
  bool transferAll = true;
  bool pharmacyName = false, pharmacyAddress = false, pharmacyPhone = false, pharmacyComment = false, pharmacySearchName = false;
  bool addedValue=false;
  bool suggestionChanged=false;

  BuildContext innerContext;
  String prevPharmacyName = "";
  String province = "";
  String selectedPlaceId = '';

  File transferImageResource = null;

  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _pharmacyNameController.addListener(pharmacyNameListener);
    _pharmacySearchNameController.addListener(pharmacyControllerListener);
    _pharmacyPhoneController.addListener(pharmacyPhoneListener);
    _pharmacyAddressController.addListener(pharmacyAddressListener);
    _commentsController.addListener(pharmacyCommentListener);
    _focusNode = FocusNode();

  }

  @override
  void dispose() {
    _pharmacyNameController.removeListener(pharmacyNameListener);
    _pharmacySearchNameController.removeListener(pharmacyControllerListener);
    _pharmacyPhoneController.removeListener(pharmacyPhoneListener);
    _pharmacyAddressController.removeListener(pharmacyAddressListener);
    _commentsController.removeListener(pharmacyCommentListener);
    super.dispose();
  }

  pharmacyControllerListener() {
    if (pharmacySearchName == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacySearchName = true;
      if(widget.PharmacyNamePopular==""){
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_name_entered);
      }
    }
  }

  pharmacyPhoneListener() {
    if (pharmacyPhone == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyPhone = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_phone_entered);
    }
  }

  pharmacyNameListener() {
    if (pharmacyName == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyName = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_name_entered);
    }
  }

  pharmacyCommentListener() {
    if (pharmacyComment == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyComment = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_comment_entered);
    }
  }

  pharmacyAddressListener() {
    if (pharmacyAddress == false && widget.source == BaseStepperSource.NEW_USER) {
      pharmacyAddress = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_address_entered);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SignUpTransferModel(),
      child: Consumer<SignUpTransferModel>(builder: (BuildContext context, SignUpTransferModel model, Widget child) {
        return FutureBuilder(
            future: model.getLocalization(["signup"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
              if(!addedValue){
                addedValue=true;
                if(widget.PharmacyNamePopular!="") {
                  _pharmacySearchNameController.text = widget.PharmacyNamePopular;
                  model.getPlaceSuggestions(_pharmacySearchNameController.text);
                }
              }
                return getStartView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget getStartView(SignUpTransferModel model) {
    return BaseScaffold(
      body: SafeArea(child: Builder(
        builder: (BuildContext context) {
          return Stack(
            children: <Widget>[
              getMainView(model),
              model.state == ViewState.Busy
                  ? Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: whiteOpacity,
                      child: ViewConstants.progressIndicator,
                    )
                  : PPContainer.emptyContainer()
            ],
          );
        },
      )),
    );
  }

  Widget getMainView(SignUpTransferModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Material(
                elevation: 4,
                color: whiteColor,
                child: Padding(
                  padding: const EdgeInsets.only(top: REGULAR_XX, left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: SMALL),
                  child: getAutoCompleteField(model),
                )),
          ),
        ),
        Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: model.pharmacyPredictions.length,
              itemBuilder: (BuildContext context, int index) {
                Pharmacy pharmacy = model.pharmacyPredictions[index];
                return InkWell(
                  onTap: () async {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    model.pharmacySubmitted = true;
                    model.setState(ViewState.Busy);
                    PlaceDetails selectedPlace = await model.getPlaceDetails(pharmacy.pharmacyPlaceId);
                    List<AddressComponent> addressComponents = selectedPlace.addressComponents;
                    AddressComponent addressCompone;
                    addressComponents.forEach((value) => {
                          addressCompone = value,
                          if (ViewConstants.province.containsKey(addressCompone.longName))
                          {province = ViewConstants.province[addressCompone.longName.toString()]},
                        });
                    model.setState(ViewState.Idle);
                    if (selectedPlace != null) {
                      selectedPlaceId = selectedPlace.placeId;
                      _pharmacySearchNameController.text = selectedPlace.name;
                      widget.model.pharmacyAddress = selectedPlace.formattedAddress;
                      widget.model.pharmacyName = selectedPlace.name;
                      widget.model.pharmacyPhoneNumber = selectedPlace.formattedPhoneNumber;
                      widget.model.province = province;
                      _pharmacyNameController.text = selectedPlace.name;
                      _pharmacyPhoneController.text = selectedPlace.formattedPhoneNumber;
                      _pharmacyAddressController.text = selectedPlace.formattedAddress;
                      Navigator.pop(context);
                    }
                    FocusScope.of(context).requestFocus(new FocusNode());
                    if (model.checkSignupFlow(widget.source)) {
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search_select);
                    } else {
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_search_select);
                    }
                  },
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(MEDIUM_X, SMALL_XXX, MEDIUM_X, SMALL_XXX),
                          child: getAutoCompleteItem(pharmacy),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: SMALL_X),
                          child: PPDivider(),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
        getButton(model)
      ],
    );
  }

  Widget getButton(SignUpTransferModel model) {
    return Builder(
      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
        child: model.state == ViewState.Busy
            ? Container(
                width: double.infinity,
                height: double.infinity,
                color: whiteOpacity,
              )
            : Row(
                children: <Widget>[
                  SecondaryButton(
                    isExpanded: false,
                    text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.back"),
                    onPressed: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                  SecondaryButton(
                    isExpanded: true,
                    isTextUpperCase: false,
                    text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.forgotbutton"),
                    textColor: linkColor,
                    onPressed: () {
                      _settingModalBottomSheet(context, model);
                    },
                  )
                ],
              ),
      ),
    );
  }

  Widget getAutoCompleteItem(Pharmacy item) {
    if (item != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  item.pharmacyName ?? "",
                  style: widget.PharmacyNamePopular==""?MEDIUM_XXX_PRIMARY_BOLD:suggestionChanged?MEDIUM_XXX_PRIMARY_BOLD:MEDIUM_XX_SECONDARY,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: SMALL,
                ),
                Text(
                  item.pharmacyAddress ?? "",
                  style: widget.PharmacyNamePopular==""?MEDIUM_XX_SECONDARY:suggestionChanged?MEDIUM_XX_SECONDARY:MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          Text(
            LocalizationUtils.getSingleValueString("signup", "signup.transfer.nearby-select"),
            style: MEDIUM_X_LINK_MEDIUM_BOLD,
          )
        ],
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getAutoCompleteField(SignUpTransferModel model) {
    return Stack(
      children: <Widget>[
        Focus(
          child: PPFormFields.getTextField(
              keyboardType: TextInputType.emailAddress,
              focusNode: _focusNode,
              controller: _pharmacySearchNameController,
              autoFocus: true,
              textInputAction: TextInputAction.next,
              onTextChanged: (value) {
                suggestionChanged = true;
                model.getPlaceSuggestions(_pharmacySearchNameController.text);
              },
              decoration: PPInputDecor.getDecoration(
                  labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.searchpharmacy"),
                  hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-name"),
                  suffixIcon: Icon(Icons.search)),
              onFieldSubmitted: (value) {}),
          onFocusChange: (hasFocus) {
            if (hasFocus) {
              model.pharmacySubmitted = false;
            }
          },
        ),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(MEDIUM_X),
            child: (_pharmacySearchNameController.text != null && _pharmacySearchNameController.text != "")
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        _pharmacySearchNameController.text = "";
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(MEDIUM_XXX),
                          )),
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Icon(
                          Icons.clear,
                          size: 18,
                          color: whiteColor,
                        ),
                      ),
                    ),
                  )
                : Icon(
                    Icons.search,
                    size: 24,
                    color: secondaryColor,
                  ),
          ),
        )
      ],
    );
  }

  Widget getCommentsBox() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
      child: PPFormFields.getMultiLineTextField(
        controller: _commentsController,
        decoration: PPInputDecor.getDecoration(
            labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.medications"),
            hintText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.list-pahramacy-info")),
      ),
    );
  }

  void _settingModalBottomSheet(context, SignUpTransferModel model) {
    double height = MediaQuery.of(context).size.height * 0.63;
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: SingleChildScrollView(
              reverse: true,
              child: Container(
                height: height,
                child: new Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: MEDIUM_XXX, bottom: MEDIUM_XXX, top: MEDIUM_XXX),
                            child: Text(
                              LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-title"),
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                            ),
                          ),
                          Divider(
                            color: secondaryColor,
                            height: 1,
                          ),
                          Padding(
                              padding: const EdgeInsets.all(MEDIUM_XXX),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  children: <Widget>[
                                    PPFormFields.getTextField(
                                        textCapitalization: TextCapitalization.words,
                                        controller: _pharmacyNameController,
                                        textInputAction: TextInputAction.next,
                                        decoration: PPInputDecor.getDecoration(
                                            labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-name"),
                                            hintText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-name")),
                                        onFieldSubmitted: (value) {}),
                                    PPUIHelper.verticalSpaceMedium(),
                                    PPFormFields.getNumericFormField(
                                      textInputAction: TextInputAction.next,
                                      autovalidate: autovalidate,
                                      controller: _pharmacyPhoneController,
                                      decoration: PPInputDecor.getDecoration(
                                          labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-phone"), hintText: ''),
                                      maxLength: 10,
                                      minLength: 10,
                                    ),
                                    PPUIHelper.verticalSpaceMedium(),
                                    PPFormFields.getMultiLineTextField(
                                      controller: _pharmacyAddressController,
                                      decoration: PPInputDecor.getDecoration(
                                          labelText: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-address"), hintText: ''),
                                    ),
                                  ],
                                ),
                              )),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: <Widget>[
                          PPDivider(),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
                              child: Container(
                                width: double.infinity,
                                alignment: Alignment(0, 0),
                                child: Row(
                                  children: <Widget>[
                                    SecondaryButton(
                                      text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-close"),
                                      onPressed: () {
                                        Navigator.pop(bc);
                                      },
                                    ),
                                    SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                    PrimaryButton(
                                        text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.current-pharmacy-continue"),
                                        onPressed: () {
                                          widget.model.pharmacyAddress = _pharmacyAddressController.text;
                                          widget.model.pharmacyName = _pharmacyNameController.text;
                                          widget.model.pharmacyPhoneNumber = _pharmacyPhoneController.text;
                                          if (_formKey.currentState.validate()) {
                                            if (_pharmacyNameController.text == '' || _pharmacyAddressController.text == '') {
                                              onFail(context, errMessage: 'Please fill in the required fields.');
                                              return;
                                            } else {
                                              Navigator.pop(bc);
                                              Navigator.pop(context, _pharmacyNameController.text + "*" + _pharmacyPhoneController.text + "*" + _pharmacyAddressController.text);
                                            }
                                          }
                                        })
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  int getFormatterPhoneNumber(String phoneNumber) {
    try {
      String formattedNumber = phoneNumber.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
      return int.parse(formattedNumber);
    } catch (ec) {
      return -1;
    }
  }
}
