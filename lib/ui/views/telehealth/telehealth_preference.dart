import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pocketpills/core/enums/telehealthAppointmentDetails.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/telehealth_medical_need.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_inputchiplist.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/views/telehealth/pp_inputchiplist_new.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/signup/telehealth_signup_bottom_sheet.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

class TelehealthPreference extends StatefulWidget {
  static const routeName = 'telehealth_preference';

  String from;
  SignUpTransferModel modelSignUp;
  BaseStepperSource source;
  UserPatient userPatient;

  TelehealthPreference(
      {this.from = "telehealth",
      this.modelSignUp,
      this.source,
      this.userPatient});

  @override
  _TelehealthPreferenceState createState() => _TelehealthPreferenceState();
}

class _TelehealthPreferenceState extends BaseState<TelehealthPreference> {
  InnerAppBar appBar;
  int preferencePosition;
  int categoryPosition;
  bool defaultValue = true;
  String dropDownPreferenceError;
  String dropDownCategoryError;
  String dropDownCategoryValue;
  String dropDownPreferenceValue;
  String inputChipError;
  PPInputChipList medicationList;
  PPInputChipListNew medicationListNew;
  bool _isKeyboardVisible = false;
  String valueChanged = "";
  bool valuePresentArray = false;
  bool autoFocus = false;

  bool eventTime = false;
  bool eventDescription = false;

  List<MedicalConditionSuggestion> medicalConditionSuggestion = List();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _enterMedicalController = TextEditingController();

  GlobalKey<AutoCompleteTextFieldState<MedicalConditionSuggestion>> key =
      new GlobalKey();
  AutoCompleteTextField searchTextField;
  TextEditingController controller = new TextEditingController();
  final FocusNode autoSearchFnode = FocusNode();
  final FocusNode descriptionFnode = FocusNode();

  Key medicationPreferenceKey;
  List<String> currentMedications;

  bool autovalidate = false;
  PreferenceModel preferenceModel;

  void _loadData() async {
    await PreferenceModel.loadPlayers();
  }

  medicalConditionControllerListener() {
    getSearchResponse(controller.text);
  }

  getSearchResponse(String keyword) async {
    medicalConditionSuggestion =
        await preferenceModel.searchMedicalConditions(keyword);
    if (autoSearchFnode.hasFocus) {
      searchTextField.updateSuggestions(medicalConditionSuggestion);
    }
  }

  @override
  void initState() {
    analyticsEvents
        .sendAnalyticsEvent(AnalyticsEventConstant.telehealth_preference);
    //_loadData();
    super.initState();
    medicationPreferenceKey = UniqueKey();
    controller.addListener(medicalConditionControllerListener);
    Map<String, String> map = new Map();
    map["patientId"] = dataStore.getPatientId().toString();
    map["userId"] = dataStore.getUserId().toString();
    widget.from == "telehealth"
        ? analyticsEvents.sendAnalyticsEvent("au_consultation", map)
        : analyticsEvents.sendAnalyticsEvent("consultation", map);
  }

  @override
  void dispose() {
    controller.removeListener(medicalConditionControllerListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.from == "telehealth"
          ? widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  Navigator.pop(context);
                  return true;
                }
              : () => _onBackPressed(context)
          : () async {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  DashboardWidget.routeName, (Route<dynamic> route) => false);
              return true;
            },
      child: ChangeNotifierProvider.value(
        value: PreferenceModel(),
        child: Consumer<PreferenceModel>(builder:
            (BuildContext context, PreferenceModel model, Widget child) {
          String name = StringUtils.capitalize(PatientUtils.getYouOrNameTitle(
              Provider.of<DashboardModel>(context).selectedPatient));
          name = '${name[0].toUpperCase()}${name.substring(1)}';
          preferenceModel = model;
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(model, name);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(PreferenceModel verificationModel) async {
    Future<Map<String, dynamic>> future1 =
        verificationModel.getLocalization(["signup", "consultation"]);
    return await Future.wait([future1]);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Widget getMainView(PreferenceModel model, String name) {
    return GestureDetector(
      onTap: () {
        final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
        final newValue = bottomInset > 0.0;
        if (newValue != _isKeyboardVisible) {
          setState(() {
            _isKeyboardVisible = newValue;
          });
        }
        if (_isKeyboardVisible) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        } else {
          FocusScope.of(context).requestFocus(FocusNode());
          //_fieldFocusChange(context, autoSearchFnode, descriptionFnode);
        }
      },
      child: Scaffold(
        appBar: innerAppbar(widget.from, name),
        body: Builder(
          builder: (BuildContext context) {
            return SafeArea(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
                      Center(
                        child: Avatar(
                            displayImage:
                                "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
                      ),
                      SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: PPUIHelper.HorizontalSpaceMedium),
                        child: Text(
                          LocalizationUtils.getSingleValueString("signup",
                              "signup.telehealth.description-pharmacist"),
                          style: TextStyle(
                            color: lightBlue,
                            fontWeight: FontWeight.w500,
                            fontSize: 16.0,
                            height: 1.5,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
                      //getPrescriptionList(),
                      getMedicalNeed(),
                      //getAutoCompleteField(),
                      Padding(
                        padding: EdgeInsets.fromLTRB(16, 8, 12, 8),
                        child: Text(
                          LocalizationUtils.getSingleValueString(
                              "signup", "signup.condition.hint"),
                          style: TextStyle(
                            fontSize: 14.0,
                            color: secondaryColor,
                            height: 1.2,
                          ),
                        ),
                      ),
                      controller != null
                          ? getPositionalViewPreference(controller.text)
                          : Container(),
                      SizedBox(
                        height: PPUIHelper.VerticalSpaceMedium,
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
            child: model.state == ViewState.Busy
                ? ViewConstants.progressIndicator
                : Row(
                    children: <Widget>[
                      PrimaryButton(
                        text: LocalizationUtils.getSingleValueString(
                            "common", "common.button.continue"),
                        onPressed: () {
                          onSaveDetails(context, model);
                        },
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Widget getPrescriptionList() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          PPFormFields.getDropDown(
            labelText: LocalizationUtils.getSingleValueString(
                "consultation", "consultation.label.preference-title"),
            value: dropDownPreferenceValue == null
                ? null
                : dropDownPreferenceValue,
            onChanged: (value) {
              if (mounted)
                setState(() {
                  if (value != 1) {
                    categoryPosition = null;
                    dropDownCategoryValue = null;
                    autovalidate = false;
                  }
                  Chips.chips = [];
                  dropDownPreferenceValue = value;
                  preferencePosition =
                      getPreferenceMap().values.toList().indexOf(value);
                  dropDownPreferenceError = null;
                });
            },
            items: getPreferenceMap()
                .map((String key, String value) {
                  return MapEntry(
                    key,
                    DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ),
                  );
                })
                .values
                .toList(),
          ),
          dropDownPreferenceError == null
              ? SizedBox(height: 2)
              : PPTexts.getFormError(dropDownPreferenceError,
                  color: errorColor),
        ],
      ),
    );
  }

  Widget getCategoryList() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          PPFormFields.getDropDown(
            labelText: LocalizationUtils.getSingleValueString(
                "consultation", "consultation.label.category-title"),
            value: dropDownCategoryValue == null ? null : dropDownCategoryValue,
            onChanged: (value) {
              if (mounted)
                setState(() {
                  Chips.chips = [];
                  dropDownCategoryValue = value;
                  categoryPosition =
                      getcategoryMap().values.toList().indexOf(value);
                  autovalidate = false;
                  dropDownCategoryError = null;
                });
            },
            items: getcategoryMap()
                .map((String key, String value) {
                  return MapEntry(
                    key,
                    DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ),
                  );
                })
                .values
                .toList(),
          ),
          dropDownCategoryError == null
              ? SizedBox(height: 2)
              : PPTexts.getFormError(dropDownCategoryError, color: errorColor),
        ],
      ),
    );
  }

  Widget getPositionalViewPreference(String value) {
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      if (value == "")
        return Container();
      else if (value == "Renew Prescription") {
        return commonView();
      } else
        return descriptionView();
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr) {
      if (value == "")
        return Container();
      else if (value == "Renouveler la Prescription") {
        return commonView();
      } else
        return descriptionView();
    }
  }

  Widget getMedicalNeed() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Stack(
        children: [
          Column(children: <Widget>[
            Column(children: <Widget>[
              searchTextField =
                  AutoCompleteTextField<MedicalConditionSuggestion>(
                style:
                    new TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
                controller: controller,
                suggestions: medicalConditionSuggestion,
                submitOnSuggestionTap: true,
                focusNode: autoSearchFnode,
                onFocusChanged: (focus) {
                  if (focus) {}
                },
                key: key,
                itemSorter: (a, b) => 0,
                itemFilter: (suggestion, input) => true,
                itemBuilder: (context, item) {
                  return Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                            child: getAutoCompleteItem(item),
                          ),
                        ),
                      ),
                    ],
                  );
                },
                textChanged: (value) {
                  if (eventTime == false) {
                    eventTime = true;
                    analyticsEvents
                        .sendAnalyticsEvent("consultation_condition_search");
                  }
                  setState(() {
                    valueChanged = value;
                  });
                },
                itemSubmitted: (MedicalConditionSuggestion item) async {
                  Chips.chips = [];
                  analyticsEvents
                      .sendAnalyticsEvent("consultation_condition_select");
                  String _addressComplete =
                      await preferenceModel.setValue(item.symptom);
                  setState(() {
                    searchTextField.controller.text = _addressComplete;
                  });
                },
                decoration: PPInputDecor.getDecoration(
                  labelText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.condition.search"),
                  hintText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.condition.search"),
                ),
              ),
            ]),
          ]),
          if (valueChanged != "")
            Positioned(
                right: 10,
                top: 10,
                child: InkWell(
                    onTap: () {
                      setState(() {
                        searchTextField.clear();
                        valueChanged = "";
                        FocusScope.of(context).requestFocus(FocusNode());
                      });
                    },
                    child: Icon(Icons.close))),
        ],
      ),
    );
  }

  Widget getPositionalViewCategory(int position) {
    if (position == null)
      return Container();
    else if (position == getcategoryMap().length - 1)
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          medicalConditionView(),
          commonView(),
        ],
      );
    else
      return commonView();
  }

  Widget getAutoCompleteItem(MedicalConditionSuggestion item) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
        child: PPTexts.getTertiaryHeading(item.symptom ?? "", isBold: true));
  }

  Widget medicalConditionView() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          RichText(
            text: TextSpan(
              text: LocalizationUtils.getSingleValueString(
                  "consultation", "consultation.form.symptoms"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                color: blackColor,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: '*',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                      color: Colors.red,
                    )),
              ],
            ),
          ),
          PPUIHelper.verticalSpaceSmall(),
          PPFormFields.getTextField(
            autovalidate: autovalidate,
            controller: _enterMedicalController,
            decoration: PPInputDecor.getDecoration(),
          ),
        ],
      ),
    );
  }

  Widget commonView() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          RichText(
            text: TextSpan(
              text: LocalizationUtils.getSingleValueString(
                  "consultation", "consultation.medicationfield.title"),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                color: blackColor,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: '*',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0,
                      color: Colors.red,
                    ))
              ],
            ),
          ),
          PPUIHelper.verticalSpaceSmall(),
          Container(
            child: getChipAddViewNew(),
          ),
          Text(
            LocalizationUtils.getSingleValueString(
                "consultation", "consultation.medicationfield.notes"),
            style: TextStyle(
              fontSize: 13.0,
              color: blackColor,
              letterSpacing: 0.8,
              height: 1.2,
            ),
          ),
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          CheckboxListTile(
            title: Text(
              LocalizationUtils.getSingleValueString(
                  "consultation", "consultation.doctor.label"),
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
            value: defaultValue,
            activeColor: brandColor,
            controlAffinity: ListTileControlAffinity.leading,
            onChanged: (bool value) => setState(() {
              defaultValue = !defaultValue;
            }),
          ),
        ],
      ),
    );
  }

  Widget descriptionView() {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          Text(
            LocalizationUtils.getSingleValueString(
                "consultation", "consultation.description.title"),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
              color: blackColor,
            ),
          ),
          PPUIHelper.verticalSpaceSmall(),
          PPFormFields.getMultiLineTextField(
              autovalidate: false,
              controller: _descriptionController,
              focusNode: descriptionFnode,
              decoration: PPInputDecor.getDecoration(),
              onTextChanged: (value) {
                print(value);
                if (eventDescription == false) {
                  eventDescription = true;
                  widget.from == "telehealth"
                      ? analyticsEvents
                          .sendAnalyticsEvent("au_description_entered")
                      : analyticsEvents
                          .sendAnalyticsEvent("description_entered");
                }
              }),
          Text(
            LocalizationUtils.getSingleValueString(
                "consultation", "consultation.medicationfield.notes"),
            style: TextStyle(
              fontSize: 13.0,
              color: blackColor,
              letterSpacing: 0.8,
              height: 1.2,
            ),
          ),
          SizedBox(height: PPUIHelper.VerticalSpaceMedium),
          CheckboxListTile(
            title: Text(
              LocalizationUtils.getSingleValueString(
                  "consultation", "consultation.doctor.label"),
              style: TextStyle(
                fontSize: 14.0,
              ),
            ),
            value: defaultValue,
            activeColor: brandColor,
            controlAffinity: ListTileControlAffinity.leading,
            onChanged: (bool value) => setState(() {
              defaultValue = !defaultValue;
            }),
          )
        ],
      ),
    );
  }

  Widget getChipAddView() {
    medicationList = PPInputChipList(
      key: medicationPreferenceKey,
      chips: currentMedications == null ? [] : currentMedications,
      totalList: [],
      labelText: "(e.g. Lipitor, etc)",
    );
    return medicationList;
  }

  Widget getChipAddViewNew() {
    medicationListNew = PPInputChipListNew(
      key: medicationPreferenceKey,
      chips: currentMedications == null ? [] : currentMedications,
      totalList: [],
      labelText: "(e.g. Lipitor, etc)",
    );
    return medicationListNew;
  }

  void onSaveDetails(BuildContext context, PreferenceModel model) async {
    if (controller.text == "") {
      if (mounted)
        setState(() {
          dropDownPreferenceError = LocalizationUtils.getSingleValueString(
              "common", "common.label.this-field-required");
          autovalidate = false;
        });
    } else if ((controller.text == "Renew Prescription" ||
            controller.text == "Renouveler la Prescription") &&
        Chips.chips.length == 0) {
      print(Chips.chips);
      String data = LocalizationUtils.getSingleValueString(
          "consultation", "consultation.all.medication-required");
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(data),
      ));
    } else {
      if (widget.from == "telehealth") {
        print(Chips.chips);
        TelehealthSignUpPreferenceRequest telehealthRequest =
            TelehealthSignUpPreferenceRequest(
                type: "TELEHEALTH",
                prescriptionFilledByExternalPharmacy: !defaultValue,
                prescriptionState: "FILED",
                prescriptionMedicalConditions: controller.text,
                telehealthRequestedMedications:
                    Chips.chips == null ? null : Chips.chips.join(","),
                prescriptionComment: _descriptionController.text.isEmpty
                    ? null
                    : _descriptionController.text,
                appointmentTime: AppointmentDetails
                    .AppointmentSelectedItem.actualAppointmentTime);
        bool connectivityResult = await model.isInternetConnected();
        if (connectivityResult == false) {
          onFail(context, errMessage: model.noInternetConnection);
          return;
        }

        if (!defaultValue) {
          Provider.of<PreferenceModel>(context).showCallAndChatBottomSheet(
              context,
              telehealthRequest,
              TelehealthRequest(),
              model,
              "Telehealth",
              widget.modelSignUp,
              widget.userPatient,
              widget.source);
        } else {
          bool success = await model.sendPreferencesSignUp(telehealthRequest);
          if (success) {
            //Chips.chips.clear();
            Map<String, String> map = new Map();
            map["patientId"] = dataStore.getPatientId().toString();
            map["userId"] = dataStore.getUserId().toString();

            widget.from == "telehealth"
                ? analyticsEvents.sendAnalyticsEvent(
                    "au_consultation_verification", map)
                : analyticsEvents.sendAnalyticsEvent(
                    "consultation_verification", map);
            print(Chips.chips);
            Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
                arguments: TelehealthArguments(
                    modelSignUp: widget.modelSignUp,
                    source: widget.source,
                    userPatient: widget.userPatient));
          } else
            analyticsEvents
                .sendAnalyticsEvent("consultation_verification_failed");
          model.showSnackBar(
              context,
              LocalizationUtils.getSingleValueString(
                  "common", "common.label.error-message"));
        }
      } else {
        TelehealthRequest telehealthRequest = TelehealthRequest(
            prescriptionType: "TELEHEALTH",
            prescriptionFilledByExternalPharmacy: !defaultValue,
            prescriptionState: "FILED",
            prescriptionMedicalConditions: controller.text,
            telehealthRequestedMedications:
                Chips.chips == null ? null : Chips.chips.join(","),
            prescriptionComment: _descriptionController.text.isEmpty
                ? null
                : _descriptionController.text,
            appointmentTime: AppointmentDetails
                .AppointmentSelectedItem.actualAppointmentTime);
        bool connectivityResult = await model.isInternetConnected();
        if (connectivityResult == false) {
          onFail(context, errMessage: model.noInternetConnection);
          return;
        }

        if (!defaultValue) {
          Provider.of<PreferenceModel>(context).showCallAndChatBottomSheet(
              context,
              TelehealthSignUpPreferenceRequest(),
              telehealthRequest,
              model,
              "",
              widget.modelSignUp,
              widget.userPatient,
              widget.source);
        } else {
          bool success = await model.sendPreferences(telehealthRequest);
          if (success) {
            Map<String, String> map = new Map();
            map["patientId"] = dataStore.getPatientId().toString();
            map["userId"] = dataStore.getUserId().toString();
            analyticsEvents.sendAnalyticsEvent(
                "consultation_verification", map);
            print(Chips.chips);
            Navigator.pushNamed(context, HealthCardUploadViewSignUp.routeName,
                arguments: TelehealthArguments(
                    modelSignUp: widget.modelSignUp,
                    source: widget.source,
                    userPatient: widget.userPatient));
            Chips.chips = [];
            Navigator.pushReplacementNamed(context, OrderStepper.routeName,
                arguments: BaseStepperArguments(
                    source: BaseStepperSource.TELEHEALTH_SCREEN,
                    startStep: 0,
                    from: widget.from));
          } else
            analyticsEvents
                .sendAnalyticsEvent("consultation_verification_failed");
          model.showSnackBar(
              context,
              LocalizationUtils.getSingleValueString(
                  "common", "common.label.error-message"));
        }
      }
    }
  }

  Widget innerAppbar(String from, String name) {
    if (from == "telehealth") {
      return AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png',
            width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
      );
    } else {
      return InnerAppBar(
          titleText: name,
          appBar: AppBar(),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                DashboardWidget.routeName, (Route<dynamic> route) => false);
          });
    }
  }

  showCallAndChatBottomSheet(
      BuildContext context, SignUpTransferModel model, String openFrom) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return SkipSignupSheet(
              context, model, widget.source, widget.userPatient, openFrom);
        });
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    Chips.chips = [];
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context).pushNamedAndRemoveUntil(
          SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false,
          arguments:
              SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName,
          arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName,
          arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(
              source: BaseStepperSource.TRANSFER_SCREEN,
              successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName,
          arguments: SourceArguments(source: widget.source));
    }
  }
}
