import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class SuccessChatCardView extends BaseStatelessWidget {
  final TransactionSuccessField field;
  final UserContactModel userContactModel;

  SuccessChatCardView(this.field, this.userContactModel);

  @override
  Widget build(BuildContext context) {
    if (field != null) {
      return PPCard(
        onTap: () {
          userContactModel.handleSuccessViewRoute(field.getActionType(), context);
        },
        margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
        padding: EdgeInsets.all(0.0),
        child: Container(
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius:
                    BorderRadius.only(topLeft: Radius.circular(SMALL_X), bottomLeft: Radius.circular(SMALL_X)),
                child: Container(
                  color: brandColor,
                  child: Image.network(
                    (field.image != null && field.image != "")
                        ? field.image
                        : "https://static.pocketpills.com/dashboard/pharmacist/susan.jpg",
                    width: 84,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: MEDIUM_X, top: SMALL_XXX, bottom: SMALL_XXX, right: MEDIUM),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        field.title ?? "",
                        style: MEDIUM_XXX_PRIMARY_BOLD,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      SizedBox(
                        height: SMALL,
                      ),
                      Text(
                        field.description ?? "",
                        style: MEDIUM_XX_SECONDARY,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }
}
