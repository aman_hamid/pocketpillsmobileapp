import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_successful_response.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/success/transaction_successful_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/success/base_success_views.dart';
import 'package:pocketpills/ui/shared/success/success_tick_card_view.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:provider/provider.dart';

class BaseSuccessScreen extends StatefulWidget {
  static const String routeName = 'baseSuccessScreen';

  BaseStepperSource source;
  TransactionSuccessEnum transactionSuccessEnum;

  BaseSuccessScreen({@required this.source = BaseStepperSource.UNKNOWN_SCREEN, @required this.transactionSuccessEnum});

  @override
  _BaseSuccessScreenState createState() => _BaseSuccessScreenState();
}

class _BaseSuccessScreenState extends State<BaseSuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
          ChangeNotifierProvider<TransactionSuccessfulModel>(create: (_) => TransactionSuccessfulModel())
        ],
        child: Consumer2<TransactionSuccessfulModel, UserContactModel>(
          builder: (BuildContext context, TransactionSuccessfulModel transactionSuccessfulModel,
              UserContactModel userContactModel, Widget child) {
            return WillPopScope(
              onWillPop: () {
                Provider.of<DashboardModel>(context).dashboardIndex = 0;
                Provider.of<HomeModel>(context).clearData();
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
              },
              child: Scaffold(
                  appBar: widget.source == BaseStepperSource.NEW_USER
                      ? null
                      : InnerAppBar(
                          titleText: widget.source == BaseStepperSource.COPAY_REQUEST
                              ? "Copay Request"
                              : PatientUtils.getPatientName(Provider.of<DashboardModel>(context).selectedPatient),
                          appBar: AppBar(),
                          backButtonIcon: Icon(
                            Icons.home,
                            color: whiteColor,
                          ),
                          leadingBackButton: () {
                            Provider.of<DashboardModel>(context).dashboardIndex = 0;
                            Provider.of<HomeModel>(context).clearData();
                            Navigator.of(context)
                                .pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                          },
                        ),
                  body: FutureBuilder(
                    future: transactionSuccessfulModel.getTransactionSuccessData(widget.transactionSuccessEnum,
                        memberPatientId: Provider.of<DashboardModel>(context).memberPatientId),
                    builder: (BuildContext context, AsyncSnapshot<TransactionSuccessfulResponse> snapshot) {
                      if (transactionSuccessfulModel.connectivityResult == ConnectivityResult.none &&
                          snapshot.connectionState == ConnectionState.done) {
                        return NoInternetScreen(
                          onClickRetry: transactionSuccessfulModel.clearAsyncMemoizer,
                        );
                      }

                      if (snapshot.hasData &&
                          transactionSuccessfulModel.connectivityResult != ConnectivityResult.none) {
                        return _afterFutureResolved(context, snapshot.data, userContactModel);
                      } else if (snapshot.hasError &&
                          transactionSuccessfulModel.connectivityResult != ConnectivityResult.none) {
                        Crashlytics.instance.log(snapshot.hasError.toString());
                        return ErrorScreen();
                      }

                      if (snapshot.connectionState == ConnectionState.active ||
                          snapshot.connectionState == ConnectionState.waiting) {
                        return LoadingScreen();
                      }
                      return PPContainer.emptyContainer();
                    },
                  )),
            );
          },
        ));
  }

  Widget _afterFutureResolved(
      BuildContext context, TransactionSuccessfulResponse successfulResponse, UserContactModel userContactModel) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SuccessTickCardView(successfulResponse.successDetails),
          SizedBox(
            height: REGULAR_XXX,
          ),
          BaseSuccessViews(successfulResponse.successDetails.fields, userContactModel),
        ],
      ),
    );
  }
}
