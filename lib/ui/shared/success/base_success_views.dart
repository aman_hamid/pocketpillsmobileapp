import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/success/free_vitamin_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_chat_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_deliveryby_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_prefer_contact_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_prefrence_card_view.dart';
import 'package:pocketpills/ui/shared/success/success_text_card_view.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';

class BaseSuccessViews extends StatelessWidget {
  final List<TransactionSuccessField> fields;
  final UserContactModel userContactModel;

  BaseSuccessViews(@required this.fields, @required this.userContactModel);

  @override
  Widget build(BuildContext context) {
    List<Widget> viewList = [];
    for (int i = 0; i < fields.length; i++) {
      TransactionSuccessField field = fields[i];
      switch (field.getCardType()) {
        case TransactionSuccessCardType.TEXT:
          viewList.add(SuccessTextCardView(field));
          break;
        case TransactionSuccessCardType.CARE_PHARMACIST:
          viewList.add(SuccessChatCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.PRESCRIPTION_PREFERENCE:
          userContactModel.preferenceData = true;
          viewList.add(SuccessPrefrenceCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.DELIVERY_BY:
          userContactModel.deliveryByData = true;
          viewList.add(SuccessDeliveryByCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.PREFERRED_TIMINGS:
          viewList.add(SuccessPreferContactCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.VITAMINS_INFO:
          viewList.add(FreeVitaminsCardView(field, userContactModel));
          break;
        case TransactionSuccessCardType.BUTTON:
          viewList.add(Padding(
            padding: const EdgeInsets.only(top: LARGE_XX, left: MEDIUM_XXX, right: MEDIUM_XXX),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                PrimaryButton(
                  isExpanded: true,
                  text: field.title,
                  onPressed: () {
                    userContactModel.handleSuccessViewRoute(field.getActionType(), context);
                  },
                )
              ],
            ),
          ));
          break;
      }
    }
    viewList.add(SizedBox(
      height: LARGE_XX,
    ));

    return Column(
      children: viewList,
    );
  }
}
