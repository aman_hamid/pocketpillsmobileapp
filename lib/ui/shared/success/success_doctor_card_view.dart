import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class SuccessDoctorCardView extends BaseStatelessWidget {
  final SuccessDetails successDetails;

  SuccessDoctorCardView(this.successDetails);

  @override
  Widget build(BuildContext context) {
    if (successDetails != null) {
      return Container(
        color: bghighlight2,
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: MEDIUM_XXX, bottom: MEDIUM_XXX),
              child: Text(
                successDetails.title ?? "",
                style: MEDIUM_XXX_DARK_BLUE_BOLD,
              ),
            ),
            CircleAvatar(
                radius: LARGE_XX,
                backgroundColor: whiteColor,
                child: CircleAvatar(
                  radius: 44,
                  backgroundColor: brandColor,
                  backgroundImage: CachedNetworkImageProvider(
                    successDetails.image ?? "",
                  ),
                )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: REGULAR_XXX, vertical: MEDIUM_XXX),
              child: Text(
                successDetails.description ?? "",
                style: MEDIUM_XX_LIGHT_BLUE_BOLD_MEDIUM,
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }
}
