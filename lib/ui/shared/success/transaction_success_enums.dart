enum TransactionSuccessEnum {
  SIGN_UP_SUCCESS,
  VITAMINS_SUCCESS,
  ADD_MEMBER_SUCCESS,
  ORDER_CHECKOUT_SUCCESS,
  COPAY_REQUEST_SUCCESS
}

enum TransactionSuccessCardType {
  PRESCRIPTION_PREFERENCE,
  DELIVERY_BY,
  PREFERRED_TIMINGS,
  VITAMINS_INFO,
  TEXT,
  CARE_PHARMACIST,
  BUTTON
}

enum TransactionSuccessActionType { CUSTOMER_CARE, MEDICATIONS, OPEN_CHAT, DASHBOARD }
