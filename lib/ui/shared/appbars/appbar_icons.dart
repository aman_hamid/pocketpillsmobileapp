import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/about_us.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/profile/profile_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:provider/provider.dart';

class AppBarIcons {
  static final Analytics analyticsEvents = locator<Analytics>();
  static final DataStoreService dataStore = locator<DataStoreService>();

  static getAppBarIcons(BuildContext context, {bool aboutUs = true}) {
    List<Widget> ll = [];
    if (aboutUs == true) {
      ll.add(IconButton(
        icon: new ImageIcon(
          new AssetImage('graphics/icons/add_member.png'),
          size: 28,
        ),
        onPressed: () {
          Navigator.pushNamed(context, AddMemberSignupWidget.routeName);
        },
      ));
      ll.add(IconButton(
        icon: new ImageIcon(new AssetImage('graphics/icons/call_chat.png')),
        onPressed: () {
          analyticsEvents
              .sendAnalyticsEvent(AnalyticsEventConstant.click_call_chat);
          Provider.of<HomeModel>(context).showCallAndChatBottomSheet(context);
        },
      ));
      ll.add(
        PopupMenuButton<String>(
          icon: Icon(Icons.more_vert),
          onSelected: (value) async {
            switch (value) {
              case "about":
                locator<NavigationService>().pushNamed(AboutUsWidget.routeName,
                    viewPopEvent: Provider.of<DashboardModel>(context)
                        .getCurrentScreenName());
                break;
              case "logout":
                analyticsEvents
                    .sendAnalyticsEvent(AnalyticsEventConstant.click_logout);
                analyticsEvents.reset();

                bool logOutRes =
                    await Provider.of<SignUpModel>(context, listen: false)
                        .logout();
                if (logOutRes == true) {
                  dataStore.writeBoolean(DataStoreService.CHAMBERS_FLOW, false);
                  dataStore.writeBoolean(
                      DataStoreService.REFRESH_DASHBOARD, false);

                  try {
                    Provider.of<DashboardModel>(context).clearAsyncMemoizer();
                  } catch (e) {
                    print(e.toString());
                  }
                  try {
                    Provider.of<PillReminderDayModel>(context, listen: false)
                        .cleanAsyncMemoizer();
                  } catch (e) {
                    print(e.toString());
                  }
                  Provider.of<HomeModel>(context, listen: false).clearData();
                  Navigator.pushNamedAndRemoveUntil(context,
                      SplashView.routeName, (Route<dynamic> route) => false);
                }
                break;
              case "profile":
                locator<NavigationService>().pushNamed(ProfileWidget.routeName,
                    viewPopEvent: Provider.of<DashboardModel>(context)
                        .getCurrentScreenName());
            }
          },
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem<String>(
                value: "profile",
                child: Text(LocalizationUtils.getSingleValueString(
                    "common", "common.navbar.profile")),
              ),
              PopupMenuItem<String>(
                value: "about",
                child: Text(LocalizationUtils.getSingleValueString(
                    "common", "common.navbar.about-us")),
              ),
              PopupMenuItem<String>(
                value: "logout",
                child: Text(LocalizationUtils.getSingleValueString(
                    "common", "common.navbar.logout")),
              )
            ];
          },
          offset: Offset(50, 50),
        ),
      );
    } else {
      ll.add(Center(
        child: FlatButton.icon(
          icon: new ImageIcon(
            new AssetImage('graphics/icons/call_chat.png'),
            color: whiteOpacity,
          ),
          //`Icon` to display
          label: Text(
            LocalizationUtils.getSingleValueString(
                    "common", "common.navbar.help")
                .toUpperCase(),
            style: MEDIUM_XX_WHITE_OPACITY_MEDIUM_BOLD,
          ),
          //`Text` to display
          onPressed: () {
            analyticsEvents
                .sendAnalyticsEvent(AnalyticsEventConstant.click_call_chat);
            Provider.of<HomeModel>(context).showCallAndChatBottomSheet(context);
          },
        ),
      ));
    }
    return ll;
  }
}
