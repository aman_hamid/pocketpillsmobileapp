import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/logos.dart';

class SecondaryAppBar extends AppBar {
  SecondaryAppBar({Key key})
      : super(
          key: key,
          backgroundColor: Colors.white,
          brightness: Platform.isIOS == true ? Brightness.light : null,
          centerTitle: true,
          elevation: 1,
          iconTheme: IconThemeData(
            color: primaryColor, //change your color here
          ),
          title: Image.asset(
            Logos.ppHorizontalLogoPath,
            height: 20,
          ),
        );
}
