import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class VitaminsBottomSheet extends StatefulWidget {
  Function onBack;
  Function onContiune;
  int stepNumber;

  VitaminsBottomSheet({@required this.onBack, @required this.onContiune, @required this.stepNumber});

  @override
  _VitaminsBottomSheetState createState() => _VitaminsBottomSheetState();
}

class _VitaminsBottomSheetState extends BaseState<VitaminsBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(
      builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget child) {
        return PPBottomBars.getButtonedBottomBar(
          child: Stack(children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SecondaryButton(
                  text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.cart.back").toUpperCase(),
                  onPressed: () {
                    if (widget.onBack != null) widget.onBack();
                  },
                  isExpanded: false,
                ),
                SizedBox(
                  width: MEDIUM_XXX,
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: MEDIUM_XXX),
                  child: RaisedButton(
                    color: brandColor,
                    disabledColor: secondaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(SMALL_XX),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.total").toUpperCase(),
                                style: MEDIUM_WHITE,
                              ),
                              RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: "\$" +
                                            (vitaminsCatalogModel.addMedicineResponse != null
                                                ? vitaminsCatalogModel.addMedicineResponse?.totalPrice.toStringAsFixed(0).toString() + " "
                                                : "0"),
                                        style: MEDIUM_XXX_WHITE),
                                    vitaminsCatalogModel.addMedicineResponse?.totalPrice != vitaminsCatalogModel.addMedicineResponse?.listPrice
                                        ? TextSpan(
                                            text: "\$" +
                                                (vitaminsCatalogModel?.addMedicineResponse != null
                                                    ? vitaminsCatalogModel.addMedicineResponse?.listPrice.toStringAsFixed(0).toString() + " "
                                                    : "0"),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: SMALL_XXX,
                                                decoration: TextDecoration.lineThrough,
                                                decorationColor: secondaryBrandColor,
                                                color: secondaryBrandColor),
                                          )
                                        : TextSpan(text: ""),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: MEDIUM,
                          ),
                          RotatedBox(
                            quarterTurns: 1,
                            child: Divider(
                              color: Colors.black,
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                getButtonText(widget.stepNumber),
                                style: MEDIUM_XXX_WHITE,
                              ),
                              SizedBox(
                                width: SMALL_XX,
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: Colors.white,
                                size: 20.0,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    onPressed: () {
                      widget.onContiune();
                      // updateShoppingCart(vitaminsCatalogModel);
                    },
                  ),
                ))
              ],
            ),
            vitaminsCatalogModel.state != ViewState.Busy
                ? Container()
                : Container(
                    color: Colors.white30,
                  )
          ]),
        );
      },
    );
  }

  String getButtonText(int stepNumber) {
    switch (stepNumber) {
      case 0:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.cart.continue").toUpperCase();
      case 1:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.cart.finish").toUpperCase();
      case 2:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.review-pack").toUpperCase();
      default:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.cart.continue").toUpperCase();
    }
  }
}
