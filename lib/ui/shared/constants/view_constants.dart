import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ViewConstants {
  static Widget get progressIndicator => Row(mainAxisAlignment: MainAxisAlignment.center, children: [CircularProgressIndicator()]);

  static const int PHARMACY_PHONE = 18334355679;
  static const int POCKETPILLS_PHONE = 18334355679;
  static const PRIVACY_POLICY_URL = "https://www.pocketpills.com/privacy-policy";
  static const TOC_URL = "https://www.pocketpills.com/terms-of-use";
  static const WEBSITE_URL = "https://www.pocketpills.com";
  static const PLAYSTORE_URL = "";
  static const SUPPORT_EMAIL_ID = "mailto:care@pocketpills.com";
  static const NETWORK_ISSUE_TEXT = "We are encountering some issues - Please try again";
  static const NO_INTERNET_TEXT = "Please check your internet connection";
  static const ONTARIO = "ontario";
  static var languageId = "en";
  static var languageIdEn = "en";
  static var languageIdFr = "fr";
  static var languageEnglish = "en_CA";
  static var languageFrench = "fr_CA";

  static const List<Map<String, String>> states = [
    {"key": "alberta", "value": "Alberta"},
    {
      "key": "british_columbia",
      "value": "British Columbia",
    },
    {
      "key": "manitoba",
      "value": "Manitoba",
    },
    {
      "key": "new_brunswick",
      "value": "New Brunswick",
    },
    {
      "key": "newfoundland_and_labrador",
      "value": "Newfoundland and Labrador",
    },
    {
      "key": "northwest_territories",
      "value": "Northwest Territories",
    },
    {
      "key": "nova_scotia",
      "value": "Nova Scotia",
    },
    {
      "key": "nunavut",
      "value": "Nunavut",
    },
    {
      "key": "ontario",
      "value": "Ontario",
    },
    {
      "key": "prince_edward_island",
      "value": "Prince Edward Island",
    },
    {
      "key": "quebec",
      "value": "Quebec",
    },
    {
      "key": "saskatchewan",
      "value": "Saskatchewan",
    },
    {
      "key": "yukon",
      "value": "Yukon",
    }
  ];

  static const Map<String, String> province = {
    "Alberta": "alberta",
    "British Columbia": "british_columbia",
    "Manitoba": "manitoba",
    "New Brunswick": "new_brunswick",
    "Newfoundland and Labrador": "newfoundland_and_labrador",
    "Northwest Territories": "northwest_territories",
    "Nova Scotia": "nova_scotia",
    "Nunavut": "nunavut",
    "Ontario": "ontario",
    "Prince Edward Island": "prince_edward_island",
    "Quebec": "quebec",
    "Saskatchewan": "saskatchewan",
    "Yukon": "yukon",
  };

  static const ALLERGIES = [
    "Penicillin",
    "Tetracycline",
    "Macrolide (erythromycin/clarithromycin)",
    "Sulfa drugs",
    "Cephalosporins",
    "Fluoroquinalone",
    "NSAIDs (ibuprofen)",
    "NSAIDs (naproxen)",
    "Aspirin (ASA)",
    "Codeine",
    "Morphine"
  ];

  /*static const dayMap = {
    "ANY": "All Days",
    "WEEKENDS": "Weekend",
    "WEEKDAYS": "Weekdays",
  };*/

  /*static const timeMap = {
    "Any Time": "Any Time",
    "8 AM - 12 NOON": "8 AM - 12 Noon",
    "12 Noon - 4 PM": "12 Noon - 4 PM",
    "4 PM - 8 PM": "4 PM - 8 PM",
  };*/

  static const deliveryTimeMap = {
    "EIGHT_TO_FIFTEEN_DAYS": "8 - 15 days",
    "FOUR_TO_SEVEN_DAYS": "4 - 7 days",
    "ONE_TO_THREE_DAYS": "1 - 3 days",
  };

  static const preferenceMap = {
    "fax_doctor": "Fax it to the doctor for co-sign",
    "mail_prescription": "I will mail the original prescription",
  };

  static const insuranceCover = {
    "0": "0%",
    "50": "50%",
    "55": "55%",
    "60": "60%",
    "65": "65%",
    "70": "70%",
    "75": "75%",
    "80": "80%",
    "85": "85%",
    "90": "90%",
    "95": "95%",
    "100": "100%",
  };

  static const String day = "ANY";
  static const String time = "Any Time";
  static const String deliveryBy = "EIGHT_TO_FIFTEEN_DAYS";
  static const String preference = "fax_doctor";

  static getColorForPrescriptionStatus(String status) {
    if (status.toLowerCase() == 'CANCELLED'.toLowerCase()) {
      return errorColor;
    } else if (status.toLowerCase() == 'DELIVERED'.toLowerCase()) {
      return successColor;
    } else if (status.toLowerCase() == 'RECEIVED'.toLowerCase()) {
      return secondaryColor;
    } else if (status.toLowerCase() == 'FILED'.toLowerCase()) {
      return successColor;
    } else {
      return secondaryColor;
    }
  }

  static String getPrescriptionStatus(String status) {
    switch (status.toUpperCase()) {
      case "RECEIVED":
        return LocalizationUtils.getSingleValueString("common", "common.status.received");
        break;
      case "READYTOLINK":
        return LocalizationUtils.getSingleValueString("common", "common.status.processing");
        break;
      case "FILED":
        return LocalizationUtils.getSingleValueString("common", "common.status.filed");
        break;
      case "CANCELLED":
        return LocalizationUtils.getSingleValueString("common", "common.status.cancelled");
        break;
      case "READY_TO_SHIP":
        return LocalizationUtils.getSingleValueString("common", "common.status.processing");
        break;
      case "PAYMENT_DONE":
        return LocalizationUtils.getSingleValueString("common", "common.status.processing");
        break;
      case "NEW_PRESCRIPTION":
        return LocalizationUtils.getSingleValueString("common", "common.status.new-prescription");
        break;
      case "RENEW_EXISTING_PRESCRIPTION":
        return LocalizationUtils.getSingleValueString("common", "common.status.renew-prescription");
        break;
      case "SOMETHING_ELSE":
        return LocalizationUtils.getSingleValueString("common", "common.status.see-doctor-something");
        break;
      default:
        return status;
        break;
    }
  }

  static String getPrescriptionType(String status) {
    switch (status.toUpperCase()) {
      case "NEW":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.new").toUpperCase();
        break;
      case "REFILL":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.refill").toUpperCase();
        break;
      case "ADAPTATION":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.adaptation").toUpperCase();
        break;
      case "CANCELLED":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.cancelled").toUpperCase();
        break;
      case "PROCESSING":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.processing").toUpperCase();
        break;
      case "FILED":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.filed").toUpperCase();
        break;
      case "OTHER":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.other").toUpperCase();
        break;
      default:
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.type.other").toUpperCase();
        break;
    }
  }

  static String getOrderStatus(String status) {
    switch (status.toUpperCase()) {
      case "PAYMENT_DONE":
        return LocalizationUtils.getSingleValueString("common", "common.status.payment-process");
        break;
      case "CANCELLED":
        return LocalizationUtils.getSingleValueString("common", "common.status.cancelled");
        break;
      case "DELIVERED":
        return LocalizationUtils.getSingleValueString("common", "common.status.delivered");
        break;
      case "PROCESSING":
        return LocalizationUtils.getSingleValueString("common", "common.status.processing");
        break;
      case "PAYMENT_INITIATED":
        return LocalizationUtils.getSingleValueString("common", "common.status.payment-initiate");
        break;
      case "SHIPPED":
        return LocalizationUtils.getSingleValueString("common", "common.status.shipped");
        break;
      case "RECEIVED":
        return LocalizationUtils.getSingleValueString("common", "common.status.received");
        break;
      default:
        return status;
    }
  }

  static String getShipmentStatus(String status) {
    switch (status.toUpperCase()) {
      case "DELIVERED":
        return LocalizationUtils.getSingleValueString("common", "common.status.delivered");
        break;
      case "RTO":
        return LocalizationUtils.getSingleValueString("common", "common.status.returned");
        break;
      case "READY_TO_SHIP":
      case "SHIPPED":
      case "ATTEMPTED_DELIVERY":
        return LocalizationUtils.getSingleValueString("common", "common.status.shipped");
        break;
      default:
        return status;
        break;
    }
  }

  static getColorForShipmentStatus(String status) {
    switch (status.toUpperCase()) {
      case "DELIVERED":
        return successColor;
        break;
      case "RTO":
        return secondaryColor;
        break;
      case "READY_TO_SHIP":
      case "SHIPPED":
      case "ATTEMPTED_DELIVERY":
        return secondaryColor;
        break;
      default:
        return secondaryColor;
        break;
    }
  }

  static const languageMap = {
    "en": "English",
    "fr": "Français",
  };
}

String getNoInternetMessageError() {
  return getSelectedLanguage() == ViewConstants.languageIdEn ? ViewConstants.NO_INTERNET_TEXT : "S'il vous plait, vérifiez votre connexion internet";
}

String getInternetMessageError() {
  return getSelectedLanguage() == ViewConstants.languageIdEn ? ViewConstants.NETWORK_ISSUE_TEXT : "Nous rencontrons des problèmes. Veuillez réessayer";
}

String getErrorTitle() {
  return getSelectedLanguage() == ViewConstants.languageIdEn ? "Something went wrong" : "Un problème est survenu";
}

String getErrorDescription() {
  return getSelectedLanguage() == ViewConstants.languageIdEn
      ? "Your request can’t be completed due to some server error."
      : "Votre demande ne peut pas être traitée en raison d'une erreur du serveur";
}

String getButtonText() {
  return getSelectedLanguage() == ViewConstants.languageIdEn ? "Retry" : "Réessayer";
}
