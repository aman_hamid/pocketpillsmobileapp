import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/chips/chips_input_local.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/core/enums/ChipValue.dart';
import 'dart:developer' as developer;

class PPInputChipList extends StatefulWidget {
  final List<String> chips;
  final List<String> totalList;
  final Function suggestions;
  final String labelText;

  PPInputChipList({Key key, this.chips, this.totalList, this.suggestions, this.labelText}) : super(key: key);
  final PPInputChipListState state = PPInputChipListState();

  List<String> getCurrentSelection() {
    return state.currentList;
  }

  @override
  PPInputChipListState createState() {
    return state;
  }
}

class PPInputChipListState extends State<PPInputChipList> {
  List<String> currentList;
  ChipsInputLocal chipsInputLocal;
  Key key = UniqueKey();

  @override
  void initState() {
    super.initState();
    currentList = widget.chips;
  }

  List<String> suggestions(String query) {
    if (query.endsWith(",")) {
      if (chipsInputLocal.state != null) {
        chipsInputLocal.state.selectSuggestion(query.substring(0, query.length - 1));
      }
    }
    if (query.length > 0) {
      query = query.toLowerCase();
      return widget.totalList.where((text) {
        return text.toLowerCase().contains(query.toLowerCase());
      }).toList(growable: false)
        ..sort((a, b) => a.toLowerCase().indexOf(query).compareTo(b.toLowerCase().indexOf(query)));
    }
    return const <String>[];
  }

  List<String> emptySuggestions(String query) {
    if (query.endsWith(",")) {
      if (chipsInputLocal.state != null) {
        chipsInputLocal.state.selectSuggestion(query.substring(0, query.length - 1));
      }
    }
    return const <String>[];
  }

  @override
  Widget build(BuildContext context) {
    if (chipsInputLocal == null)
      chipsInputLocal = ChipsInputLocal<String>(
        key: key,
        initialValue: widget.chips,
        decoration: PPInputDecor.getDecoration(collapsed: true, labelText: widget.labelText == null ? "" : widget.labelText),
        findSuggestions: widget.totalList.length == 0 ? emptySuggestions : suggestions,
        onChanged: (List<String> data) {
          currentList = data;
        },
        chipBuilder: (context, state, chipText) {
          return InputChip(
            key: ObjectKey(chipText),
            deleteIconColor: secondaryColor,
            backgroundColor: headerBgColor,
            autofocus: true,
            label: Text(chipText),
            labelStyle: TextStyle(
              color: primaryColor,
              fontSize: PPUIHelper.FontSizeSmall,
              height: 1.4,
              fontWeight: FontWeight.bold,
            ),
            onDeleted: () => state.deleteChip(chipText),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          );
        },
        suggestionBuilder: (context, state, chipText) {
          return ListTile(
            key: ObjectKey(chipText),
            title: Text(chipText),
            onTap: () => state.selectSuggestion(chipText),
          );
        },
      );
    return chipsInputLocal;
  }
}
