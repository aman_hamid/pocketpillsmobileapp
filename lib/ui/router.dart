import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/chat_view.dart';
import 'package:pocketpills/ui/views/about_us.dart';
import 'package:pocketpills/ui/views/addmember/about_patient.dart';
import 'package:pocketpills/ui/views/addmember/add_member_arguments.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/addmember/cellphone_view.dart';
import 'package:pocketpills/ui/views/consent/activate_patient.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/copay/copay_request_arguments.dart';
import 'package:pocketpills/ui/views/copay/copay_request_success_screen.dart';
import 'package:pocketpills/ui/views/copay/copay_request_view.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_arguments.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_appointment_detail_view.dart';
import 'package:pocketpills/ui/views/home/dashboard_prescription_detail_view.dart';
import 'package:pocketpills/ui/views/home/fax_prescription_view.dart';
import 'package:pocketpills/ui/views/home/medicine_detail_view.dart';
import 'package:pocketpills/ui/views/home/prescription_arguments.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription_arguments.dart';
import 'package:pocketpills/ui/views/login/forgotpassword_view.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/login/resetpassword_view.dart';
import 'package:pocketpills/ui/views/login/setpassword_view.dart';
import 'package:pocketpills/ui/views/medications/medications_refill_action_view.dart';
import 'package:pocketpills/ui/views/order/order_arguments.dart';
import 'package:pocketpills/ui/views/order/order_detail_view.dart';
import 'package:pocketpills/ui/views/order_stepper_success_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_calendar_view.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_medications_view.dart';
import 'package:pocketpills/ui/views/prescriptions/prescription_detail_view.dart';
import 'package:pocketpills/ui/views/profile/address_detail_arguments.dart';
import 'package:pocketpills/ui/views/profile/address_detail_view.dart';
import 'package:pocketpills/ui/views/profile/payment_detail_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/profile/profile_insurance_view.dart';
import 'package:pocketpills/ui/views/profile/profile_source_arguments.dart';
import 'package:pocketpills/ui/views/profile/profile_view.dart';
import 'package:pocketpills/ui/views/referral/referral_contact_sync_view.dart';
import 'package:pocketpills/ui/views/referral/referral_view.dart';
import 'package:pocketpills/ui/views/signup/email_view.dart';
import 'package:pocketpills/ui/views/signup/sign_up_stepper.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_contact_details_view.dart';
import 'package:pocketpills/ui/views/signup/signup_email_details_view.dart';
import 'package:pocketpills/ui/views/signup/signup_otp_password_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_success_details_view.dart';
import 'package:pocketpills/ui/views/signup/signup_user_contact_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/success_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/ui/views/start/startview_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/AppointmentDateWidget.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/ui/views/vitamins/vitamin_filter_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_details_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_filter_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_order_stepper.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_order_success_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_subscription_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/ui/views/webview/webview_arguments.dart';
import 'package:pocketpills/ui/views/webview/webview_home_view.dart';
import 'package:pocketpills/utils/route/CustomRoute.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/signup/health_card_signup_view.dart';

class PPRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case DashboardWidget.routeName:
        final DashboardArguments args = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => DashboardWidget(
            snackBarMessage: args != null ? args.snackBarMessage : null,
          ),
        );
        return MaterialPageRoute(
            builder: (_) => DashboardWidget(
                snackBarMessage: args != null ? args.snackBarMessage : null));
      case WebviewHomeView.routeName:
        final WebviewArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => WebviewHomeView(
                  sourceUrl: args.sourceUrl,
                ));
      case LoginWidget.routeName:
        final LoginArguments args = settings.arguments;
        if (args == null)
          return MaterialPageRoute(builder: (_) => LoginWidget());
        return MaterialPageRoute(
            builder: (_) => LoginWidget(
                  snackbarMessage: args.snackbarMessage,
                  phone: args.phone,
                  source: args.source,
                ));
      case ForgotPasswdWidget.routeName:
        return MaterialPageRoute(builder: (_) => ForgotPasswdWidget());
      case LoginVerificationWidget.routeName:
        final VerificationArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => LoginVerificationWidget(phoneNo: args.phoneNo));
      case ResetPasswordWidget.routeName:
        return MaterialPageRoute(builder: (_) => ResetPasswordWidget());
      case SetPasswordWidget.routeName:
        return MaterialPageRoute(builder: (_) => SetPasswordWidget());
      case StartView.routeName:
        final StartViewArguments args = settings.arguments;
        if (args == null) return MaterialPageRoute(builder: (_) => StartView());
        return MaterialPageRoute(
            builder: (_) => StartView(
                  deepLinkRouteName: args.deepLinkRouteName,
                  carouselIndex: args.carouselIndex,
                  chambersFlow:
                      args.chambersFlow != null ? args.chambersFlow : false,
                ));
      case SplashView.routeName:
        final StartViewArguments args = settings.arguments;
        if (args == null)
          return MaterialPageRoute(builder: (_) => SplashView());
        return MaterialPageRoute(
            builder: (_) => SplashView(
                  deepLinkRouteName: args.deepLinkRouteName,
                  carouselIndex: args.carouselIndex,
                ));
      case SignupStepper.routeName:
        final SignupStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => SignupStepper(
                  position: args.position,
                  source: args.source,
                ));
      case EmailWidget.routeName:
        return MaterialPageRoute(builder: (_) => EmailWidget());
      case TransferWidget.routeName:
        final TransferArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => TransferWidget(
                  source: args != null ? args.source : null,
                  userPatient: args != null ? args.userPatient : null,
                  medicineName: args != null ? args.medicineName : null,
                  quantity: args != null ? args.quantity : null,
                  chambersFlow:
                      args.chambersFlow != null ? args.chambersFlow : false,
                ));
      case TransferPharmacySearchView.routeName:
        final TransferArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => TransferPharmacySearchView(
                  source: args != null ? args.source : null,
                  model: args.model,
                  PharmacyNamePopular: args.PharmacyNamePopular,
                ));
      case VitaminsWidget.routeName:
        final SourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) =>
                VitaminsWidget(source: args != null ? args.source : null));
      case VitaminsCatalogWidget.routeName:
        final VitaminFilterArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => VitaminsCatalogWidget(
                source: args != null ? args.source : null,
                filterArgu: args.filterArgu != null ? args.filterArgu : ""));
      case VitaminsCatalogFilterWidget.routeName:
        final BaseStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => VitaminsCatalogFilterWidget(
                itemId: args.itemId,
                source: args != null ? args.source : null));
      case VitaminsCatalogDetailsWidget.routeName:
        return MaterialPageRoute(
            builder: (_) => VitaminsCatalogDetailsWidget());
      case VitaminsOrderSuccessScreen.routeName:
        final BaseStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => VitaminsOrderSuccessScreen(
                source: args != null ? args.source : null));
      case VitaminsOrderStepper.routeName:
        final BaseStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => VitaminsOrderStepper(
                source: args.source, startStep: args.startStep));
      case SignUpVerificationWidget.routeName:
        final VerificationArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => SignUpVerificationWidget(
                  phoneNo: args.phoneNo,
                  source: args.source,
                ));
      case AddMemberSignupWidget.routeName:
        return MaterialPageRoute(builder: (_) => AddMemberSignupWidget());
      case CellPhoneWidget.routeName:
        final AddMemberArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => CellPhoneWidget(
                request: args != null ? args.addMemberRequest : null,
                gender: args != null ? args.gender : null));
      case AboutPatientWidget.routeName:
        final TransferArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => AboutPatientWidget(
                userPatient: args != null ? args.userPatient : null,
                snackBarMessage: args != null ? args.snackBarMessage : null,
                gender: args != null ? args.gender : null));
      case AboutUsWidget.routeName:
        return MaterialPageRoute(builder: (_) => AboutUsWidget());
      case UploadPrescription.routeName:
        final UploadPrescriptionArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => UploadPrescription(
                  source: args != null ? args.source : null,
                  medicineName: args != null ? args.medicineName : null,
                  quantity: args != null ? args.quantity : null,
                  from: args != null ? args.from : null,
                  model: args != null ? args.model : null,
                  userPatient: args != null ? args.userPatient : null,
                ));
      case TelehealthPreference.routeName:
        final TelehealthArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => TelehealthPreference(
                from: args != null ? args.from : null,
                modelSignUp: args != null ? args.modelSignUp : null,
                source: args != null ? args.source : null,
                userPatient: args != null ? args.userPatient : null));
      case AppointmentDateWidget.routeName:
        final TelehealthArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => AppointmentDateWidget(
                from: args != null ? args.from : null,
                modelSignUp: args != null ? args.modelSignUp : null,
                source: args != null ? args.source : null,
                userPatient: args != null ? args.userPatient : null));
      case OrderStepperSuccessScreen.routeName:
        final TelehealthArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => OrderStepperSuccessScreen(
                  from: args != null ? args.from : null,
                ));
      case CopayRequestSuccessScreen.routeName:
        return MaterialPageRoute(builder: (_) => CopayRequestSuccessScreen());
      case OrderDetailWidget.routeName:
        final OrderArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => OrderDetailWidget(
                  orderId: args != null ? args.orderId : null,
                ));
      case DashboardPrescriptionsDetailsView.routeName:
        final PrescriptionArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => DashboardPrescriptionsDetailsView(
                  prescriptionId: args != null ? args.prescriptionId : null,
                ));
      case DashboardAppointmentDetailsView.routeName:
        final PrescriptionArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => DashboardAppointmentDetailsView(
                  prescriptionId: args != null ? args.prescriptionId : null,
                ));
      case PrescriptionDetailWidget.routeName:
        return MaterialPageRoute(builder: (_) => PrescriptionDetailWidget());
      case MedicationsRefillAction.routeName:
        return MaterialPageRoute(builder: (_) => MedicationsRefillAction());
      case PaymentDetailWidget.routeName:
        return MaterialPageRoute(builder: (_) => PaymentDetailWidget());
      case VitaminsSubcriptionWidget.routeName:
        final BaseStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => VitaminsSubcriptionWidget(
                source: args != null ? args.source : null));
      case ChatWidget.routeName:
        return MaterialPageRoute(builder: (_) => ChatWidget());
      case UserContactWidget.routeName:
        final PrescriptionSourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => UserContactWidget(
                  source: args != null ? args.source : null,
                  successDetails: args != null ? args.successDetails : null,
                ));
      case FaxPrescriptionWidget.routeName:
        return MaterialPageRoute(builder: (_) => FaxPrescriptionWidget());
      case ProfileWidget.routeName:
        final ProfileSourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ProfileWidget(
                  activeIndex: args != null ? args.index : 0,
                ));
      case AddressDetailWidget.routeName:
        final AddressDetailArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => AddressDetailWidget(address: args.address));
      case MedicineDetailWidget.routeName:
        final UploadPrescriptionArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => MedicineDetailWidget(
                  source: args != null ? args.source : null,
                  medicineName: args != null ? args.medicineName : null,
                  from: args != null ? args.from : null,
                  model: args != null ? args.model : null,
                  userPatient: args != null ? args.userPatient : null,
                ));
      case OrderStepper.routeName:
        final BaseStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => OrderStepper(
                source: args.source,
                startStep: args.startStep,
                from: args.from != null ? args.from : null));
      case WaitConsentWidget.routeName:
        final ConsentArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => WaitConsentWidget(
                  userPatient: args != null ? args.userPatient : null,
                ));
      case AddMemberCompleteWidget.routeName:
        final ConsentArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => AddMemberCompleteWidget(
                  userPatient: args != null ? args.userPatient : null,
                ));
      case ActivatePatientWidget.routeName:
        final ConsentArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ActivatePatientWidget(
                  userPatient: args != null ? args.userPatient : null,
                ));
      case ReferralView.routeName:
        return MaterialPageRoute(builder: (_) => ReferralView());
      case ReferralContactSyncView.routeName:
        return MaterialPageRoute(builder: (_) => ReferralContactSyncView());
      case SignupWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignupWidget(
                  source: args.source,
                  chambersFlow:
                      args.chambersFlow != null ? args.chambersFlow : false,
                ));
      case SignUpContactDetailsWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignUpContactDetailsWidget(
                  source: args.source,
                ));
      case SignUpOtpPasswordWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignUpOtpPasswordWidget(
                  source: args.source,
                  chambersFlow:
                      args.chambersFlow != null ? args.chambersFlow : false,
                ));
      case SignUpEmailDetailsWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignUpEmailDetailsWidget(
                  source: args.source,
                ));
      case SignupUserContactWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignupUserContactWidget(
                  source: args.source,
                ));
      case SignUpSuccessDetailsWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return CustomRoute(
            builder: (_) => SignUpSuccessDetailsWidget(
                  source: args.source,
                ));

      case SignUpSuccessWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => SignUpSuccessWidget(
                  source: args.source,
                ));
      case PillReminderCalendarView.routeName:
        return MaterialPageRoute(builder: (_) => PillReminderCalendarView());
      case PillReminderMedicationsView.routeName:
        return MaterialPageRoute(builder: (_) => PillReminderMedicationsView());
      case SignUpAlmostDoneWidget.routeName:
        final SignupStepperArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => SignUpAlmostDoneWidget(
                  source: args.source,
                  chambersFlow:
                      args.chambersFlow != null ? args.chambersFlow : false,
                ));
      case CopayRequestView.routeName:
        final CopayRequestArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => CopayRequestView(
                  medicine: args.medicine,
                  quantity: args.quantity,
                ));
      case ProfileInsuranceView.routeName:
        final SourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ProfileInsuranceView(
                source: args != null ? args.source : null));
      case ProfileHealthCardView.routeName:
        final SourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ProfileHealthCardView(
                source: args != null ? args.source : null));
      case ProfileInsuranceView.routeName:
        final SourceArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ProfileInsuranceView(
                source: args != null ? args.source : null));
        return MaterialPageRoute(
            builder: (_) => ProfileInsuranceView(
                source: args != null ? args.source : null));
      case HealthCardUploadViewSignUp.routeName:
        final TelehealthArguments args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => HealthCardUploadViewSignUp(
                model: args != null ? args.modelSignUp : null,
                source: args != null ? args.source : null,
                userPatient: args != null ? args.userPatient : null));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
