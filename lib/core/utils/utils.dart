import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class Utils {
  static getState(String key) {
    for (int i = 0; i < ViewConstants.states.length; i++) {
      if (ViewConstants.states.elementAt(i)["key"] == key) return ViewConstants.states.elementAt(i)["value"];
    }
  }

  static String intToTimes(int value) {
    List<String> retVal = ["None", "Once", "Twice", "Thrice", "Four times", "Five times", "Six times", "Seven times"];
    if (value > -1 && value < 8) return retVal[value];
    return "Many times";
  }

  static String intToString(int value) {
    return value.toString();
  }

  static String dosageToSigCode(List<String> dosage) {
    String msc = "0", asc = "0", esc = "0", bsc = "0"; //TODO : make this an enum
    if (dosage.indexOf("Morning") > -1) msc = "1";
    if (dosage.indexOf("Afternoon") > -1) asc = "1";
    if (dosage.indexOf("Evening") > -1) esc = "1";
    if (dosage.indexOf("Bedtime") > -1) bsc = "1";
    return msc + asc + esc + bsc;
  }

  static String cardTypeToImage(String cardType) {
    switch (cardType) {
      case PaymentMode.MASTERCARD:
        return 'graphics/payment_icons/mastercard.png';
      case PaymentMode.DISCOVER:
        return 'graphics/payment_icons/discover.png';
      case PaymentMode.JCB:
        return 'graphics/payment_icons/jcb.png';
      case PaymentMode.MAESTRO:
        return 'graphics/payment_icons/maestro.png';
      case PaymentMode.AMEX:
        return 'graphics/payment_icons/amex.png';
      case PaymentMode.MY_HSA:
        return 'graphics/payment_icons/my_hsa.png';
      default:
        return 'graphics/payment_icons/visa.png';
    }
  }

  static String removeDecimalZeroFormat(num n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
  }
}
