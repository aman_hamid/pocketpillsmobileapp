import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/models/user_language.dart';
import 'package:pocketpills/core/request/credit_card_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup_phone_number_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/request/transfer_prescription_request.dart';
import 'package:pocketpills/core/request/update_email_request.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'dart:developer' as developer;
import 'package:pocketpills/core/request/signup_prescription_request.dart';

import 'package:pocketpills/utils/localization/localization_utils.dart';

class HttpApi {
  static final serverUrl = locator<AppConfig>().apiBaseUrl;
  static final dataStore = locator<DataStoreService>();
  Dio _dio;

  HttpApi() {
    _dio = Dio();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers['Authorization'] =
          checkNullAndEmpty(dataStore.getAuthorizationToken());
      options.headers['userId'] = dataStore.getUserId();
      options.headers['patientId'] = dataStore.getPatientId();
      options.headers['locale'] = getServerSendLanguage();
      options.headers['platform'] = Platform.isIOS ? "IOS" : "ANDROID";
      options.headers['version'] =
          dataStore.readString(DataStoreService.APP_VERSION);
      options.headers['deviceId'] =
          dataStore.readString(DataStoreService.DEVICE_ID);
      options.headers['ppDistinctId'] = checkNullAndEmpty(
          dataStore.readString(DataStoreService.PP_DISTINCT_ID));
      return options;
    }));
    _dio.interceptors.add(LogInterceptor(
        responseBody: false,
        requestBody: false,
        requestHeader: false,
        responseHeader: false,
        request: false));
  }

  String checkNullAndEmpty(String checkString) {
    if (checkString != null) {
      return checkString;
    } else {
      return "";
    }
  }

  Response handleError(DioError e, Response response) {
    response = e.response;
    developer.log('P1D1 : handleError :', name: response.toString());
    return handleResponse(response);
  }

  Response handleResponse(Response response) {
    developer.log('P1D1 : handleResponse :', name: response.toString());
    Logger().d(response.toString());
    return response;
  }

  Future<Response> checkPhone(String phoneNo) async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/user?phoneNumber=$phoneNo");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getUserInfo() async {
    Response response;
    try {
      response =
          await _dio.get("$serverUrl/signup/user/${dataStore.getUserId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> phoneNumberSignup(String phoneNo) async {
    RequestWrapper requestWrapper =
        new RequestWrapper(body: SignupPhoneNumberRequest(phone: phoneNo));
    Response response;
    try {
      response = await _dio.post("$serverUrl/signup/user",
          data: requestWrapper.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> checkVersion(String version, String platform) async {
    Response response;
    try {
      response =
          await _dio.get("$serverUrl/version/$version?platform=$platform");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> checkSession() async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/session-check");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> postLogin(String authHeader, RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/login",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> logOut() async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/logout",
        data: RequestWrapper(body: null).toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> signup(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/signup/user",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateUserInfo(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/signup/user/${dataStore.getUserId()}",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> chambersUserInfo(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.get(
        "$serverUrl/signup/user/${dataStore.getUserId()}",
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> otpVerify(RequestWrapper body, String phoneNo,
      String verificationCode, String isSignup) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/otp/" +
            phoneNo +
            "?verificationCode=" +
            verificationCode +
            "&isRegistrationProcess=" +
            isSignup,
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getVerifyRegistrationFlow(
      RequestWrapper body,
      String phoneNo,
      String verificationCode,
      String isSignup,
      int userID) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/signup/user/" +
            userID.toString() +
            "/verify-otp?userPhone=" +
            phoneNo +
            "&otp=" +
            verificationCode,
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getOtp(String phoneNo) async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/reset/" + phoneNo);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> resendOtp(
      String phoneNo, String mode, bool isRegistrationProcess) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/otp/resend/$phoneNo?isRegistrationProcess=$isRegistrationProcess&otpMode=$mode");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> resetPassword(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/user/resetPassword",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> setPassword(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/user/setPassword",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateUser(RequestWrapper body, String userId) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user/$userId?isRegistrationProcess=true",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateUserAlmostDone(
      RequestWrapper body, String userId) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user/$userId/almost-done?isRegistrationProcess=true",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> activateInvitationCode(String invitationCode) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/patient/${dataStore.getPatientId()}/activate-insurance/$invitationCode",
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateUserMarkSummary() async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/user/${dataStore.getUserId()}/summary",
          data: RequestWrapper(body: null));
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addPrescription(int patientId, RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/user/${dataStore.getUserId()}/patient/${patientId.toString()}/prescription?isRegistrationProcess=true",
        data: body.toJson(),
      );

      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addSignUpPrescription(RequestWrapper body) async {
    Response response;

    try {
      response = await _dio.put(
        "$serverUrl/signup/user/${dataStore.getUserId()}/prescription",
        data: body.toJson(),
      );

      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  //caregiver/{userId}/request
  Future<Response> addPatient(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/caregiver/${dataStore.getUserId()}/request",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  //user/{userId}/patient/{patientId}/about-you
  Future<Response> updatePatient(int patientId, RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user/${dataStore.getUserId()}/patient/$patientId/about-you?isRegistrationProcess=true",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getUserPatientList(int userId) async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/userpatient/$userId");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPrescriptionId(String prescriptionType) async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/patient/${dataStore.getPatientId()}/latest-prescription/$prescriptionType",
        data: RequestWrapper(body: null).toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getMonerisHtml() async {
    Response response;
    try {
      response = await _dio.get(
        "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/cc/registration",
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addMonerisToken(token) async {
    Response response;

    CreditCardRequest request = CreditCardRequest(
        isDefault: true,
        postalCode: "",
        patientId: dataStore.getPatientId().toString(),
        token: token);
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/cc",
          data: RequestWrapper(body: request).toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updatePrescription(
    int prescriptionId,
    TransferPrescriptionRequest request,
  ) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/patient/${dataStore.getPatientId()}/prescription/$prescriptionId",
        data: RequestWrapper(body: request).toJson(),
      );
      return response;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> uploadPrescriptionSignUp(RequestWrapper body) async {
    Response response;
    String bo = body.toJson().toString();
    try {
      response = await _dio.put(
        "$serverUrl/signup/user/${dataStore.getUserId()}/prescription",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  //https://c1.api.pocketpills.com/signup/user/13004
  Future<Response> telehealthProvinceAdd(RequestWrapper body) async {
    Response response;
    String bo = body.toJson().toString();
    try {
      response = await _dio.put(
        "$serverUrl/signup/user/${dataStore.getUserId()}",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///patient/{patientId}/prescriptions/{prescriptionId}/document
  Future<Response> uploadImage(
      int prescriptionId, File image, int pageNumber) async {
    Response response;
    try {
      FormData data = FormData.fromMap({
        "patientId": dataStore.getPatientId(),
        "patientPrescriptionId": prescriptionId,
        "image": await MultipartFile.fromFile(image.path,
            filename: basename(image.path)),
        "pageNumber": pageNumber
      });

      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/prescriptions/$prescriptionId/document",
          data: data);
      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/prescriptions/$prescriptionId/document",
          data: data);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///https://c1.api.pocketpills.com/signup/user/12941/prescription-document'
  Future<Response> signUpUploadImage(File image, int pageNumber) async {
    Response response;
    try {
      FormData data = FormData.fromMap({
        "image": await MultipartFile.fromFile(image.path,
            filename: basename(image.path)),
        "pageNumber": pageNumber
      });

      response = await _dio.put(
          "$serverUrl/signup/user/${dataStore.getUserId()}/prescription-document",
          data: data);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> uploadSignUpTransferImage(File image) async {
    Response response;
    try {
      FormData data = FormData.fromMap({
        "image": await MultipartFile.fromFile(image.path,
            filename: basename(image.path)),
      });

      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/upload-document",
          data: data);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> uploadSignUpTransferImageList(
      Map<String, dynamic> requestWrapper) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/signup/user/${dataStore.getUserId()}/prescription-document-list",
          data: requestWrapper);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getOrders() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/api/patient/${dataStore.getPatientId()}/order/?patientIds=${dataStore.getPatientId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getAppointmentSlots() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/appointment-slots?patientId=${dataStore.getPatientId()}&userId=${dataStore.getUserId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  //https://qa.pocketpills.com/api/patient/63/order/shipment/614/track-url
  Future<String> getOrderTrackURl(int orderId) async {
    Response response;
    String errorMessage;
    try {
      response = await _dio.get(
          "$serverUrl/api/patient/${dataStore.getPatientId()}/order/shipment/$orderId/track-url");

      if (response != null) {
        OMSResponse<String> res = OMSResponse<String>.fromJson(response.data);
        if (!res.status) {
          errorMessage = res.getErrorMessage();
          return null;
        }
        String str = res.response;
        return str.toString();
      }
      return null;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> getOrderData(int orderId) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/api/patient/${dataStore.getPatientId()}/order/$orderId/");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPatientHealth() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/patient/${dataStore.getPatientId()}/health");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> setPatientHealth(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/health",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPatientInsurance() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/patient/${dataStore.getPatientId()}/insurance");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> setPatientInsurance(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/insurance",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> deleteInsuranceImage(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/insurance-document/delete",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addInsuranceImage(File image, String insuranceType,
      bool isInsuranceFront, String patientId) async {
    Response response;
    try {
      FormData data = FormData.fromMap({
        "patientId": dataStore.getPatientId().toString(),
        "image": await MultipartFile.fromFile(image.path,
            filename: basename(image.path)),
        "type": insuranceType,
        "isFront": isInsuranceFront
      });

      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/insurance-document",
          data: data);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> createOrder(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/order-din",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPatientAddress() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/patient/${dataStore.getPatientId()}/address");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addPatientAddress(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/patient/${dataStore.getPatientId()}/address",
          data: body.toJson());
      return handleResponse(response);
    } catch (e) {
      response = e.response;
      return handleResponse(response);
    }
  }

  Future<Response> editPatientAddress(
      RequestWrapper body, int addressId) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/patient/${dataStore.getPatientId()}/address/${addressId.toString()}",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> deletePatientAddress(int addressId) async {
    Response response;
    try {
      response = await _dio.delete(
          "$serverUrl/patient/${dataStore.getPatientId()}/address/$addressId?addressId=$addressId",
          data: RequestWrapper(body: null).toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> setPatientAddressDefault(int addressId) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/patient/${dataStore.getPatientId()}/address/$addressId/default");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> setPatientCardDefault(int cardId) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/cc/$cardId/default");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> addPatientCard(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/cc",
          data: body.toJson());
      return handleResponse(response);
    } catch (e) {
      response = e.response;
      return handleResponse(response);
    }
  }

  Future<Response> deletePatientCard(int cardId) async {
    Response response;
    try {
      response = await _dio.delete(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/cc/$cardId");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPatientCard() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/cc?patientId=${dataStore.getPatientId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPrescriptions() async {
    Response response;
    try {
      response = await _dio.post(
        "$serverUrl/api/patient/${dataStore.getPatientId()}/prescription/get",
        data: RequestWrapper(body: null).toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPrescriptionsByIdAppointment(int prescriptionId) async {
    Response response;
    try {
      //response = await _dio.get("$serverUrl/api/patient/${dataStore.getPatientId()}/prescription/$prescriptionId/get");
      response = await _dio.get(
        "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/telehealth/$prescriptionId/appointment-details",
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPrescriptionsById(int prescriptionId) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/api/patient/${dataStore.getPatientId()}/prescription/$prescriptionId/get");
      //response = await _dio.get("$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/telehealth/$prescriptionId/appointment-details",);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getMedications() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/patient/${dataStore.getPatientId()}/active-medicines-map?allow-null-medication-ids=true");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> searchMedicines(String keyword) async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/search?query=$keyword");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getMedicine(String medicineId) async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/search/$medicineId");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getHomeData() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/dashboard?version=4");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> trackDashboardCard(int cardId) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/track-cards?cardIds=$cardId",
          data: null);
      return handleResponse(response);
    } catch (e) {
      response = e.response;
      return handleResponse(response);
    }
  }

  Future<Response> getFreeMedicinesData() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/patient/${dataStore.getPatientId()}/free-medicines");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> activatePatient(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/activate-patient",
          data: body.toJson());
      return handleResponse(response);
    } catch (e) {
      response = e.response;
      return handleResponse(response);
    }
  }

  Future<Response> resendConsent(int patientId) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/caregiver/${dataStore.getUserId()}/resendConsent/$patientId");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> refillMedications(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/refill",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getContact() async {
    Response response;
    try {
      response =
          await _dio.get("$serverUrl/user/${dataStore.getUserId()}/contact");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> putContact(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/user/${dataStore.getUserId()}/contact",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> putUpdateFirebaseToken(RequestWrapper body) async {
    Response response;
    try {
      response =
          await _dio.put("$serverUrl/refersh-token", data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getReferralCode() async {
    Response response;
    try {
      response =
          await _dio.get("$serverUrl/user/${dataStore.getUserId()}/referral");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> postReferralCode(String referralCode) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/referral?referralCode=$referralCode",
          data: RequestWrapper(body: null).toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///GET vitamins catalog list
  Future<Response> getVitaminsCatalogList(String filter) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/vns-listing?patientId=${dataStore.getPatientId()}&itemGroupIds=$filter");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///GET shopping cart list
  Future<Response> getShoppingCartList() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart?patientId=${dataStore.getPatientId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///Add medicine to sopping cart
  Future<Response> addMedicineToShoppingCart(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart/variant?patientId=${dataStore.getPatientId()}",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateVitaminsCartFromDashboard(int itemGroupId) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/update-cart?itemGroupIds=${itemGroupId}",
          data: null);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  ///Update shopping cart items
  Future<Response> updateShoppingCartItems(
      RequestWrapper body, int variantId) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart/variant/$variantId?patientId=${dataStore.getPatientId()}",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Update shopping cart
  Future<Response> updateShoppingCart(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.put(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart?patientId=${dataStore.getPatientId()}",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Remove medicine from shoppinfg cart
  Future<Response> removeMedicineFromShoppingCart(int variantId) async {
    Response response;
    try {
      response = await _dio.delete(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart/variant/$variantId?patientId=${dataStore.getPatientId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Complete shopping cart checkout
  Future<Response> completeShoppingCartCheckOut(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/shopping-cart/complete-checkout?patientId=${dataStore.getPatientId()}",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Complete shopping cart checkout
  Future<Response> getCouponCodeList() async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/coupon-code-list?patientId=${dataStore.getPatientId()}",
          data: null);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get all address suggestions
  Future<Response> getPatientAddressSuggestion() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/address-suggestions");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Copy insurance card from candidate
  Future<Response> getCopyInsuranceCardFromCandidate(int sourcePatientId,
      String fromInsuranceType, String toInsuranceType) async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/insurance-copy/$sourcePatientId?fromInsuranceType=$fromInsuranceType&toInsuranceType=$toInsuranceType",
          data: null);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get all address suggestions
  Future<Response> getCopyInsuranceCandidate() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/insurance-copy/candidates");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get suggested employers
  Future<Response> getSuggestedEmployers() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/patient/${dataStore.getPatientId()}/employer-suggestions");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get vns filter
  Future<Response> getVitaminsFilter() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/user/${dataStore.getUserId()}/vns-filters");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get vitamins subscription details
  Future<Response> getVitaminssubscriptionDetails() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/subscription?patientId=${dataStore.getPatientId()}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Get lat long using ip address
  Future<Response> getLocationUsingIp() async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/geoip");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> sendAdvertiseParams(RequestWrapper body) async {
    Response response;
    try {
      response =
          await _dio.post("$serverUrl/pp-distinct-entity", data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateAdvertiseParams(
      RequestWrapper body, String ppDistinctId) async {
    Response response;
    try {
      response = await _dio.put("$serverUrl/pp-distinct-entity/$ppDistinctId",
          data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> ChambersActivate(RequestWrapper body) async {
    Response response;
    try {
      print('AMANREQueest:${body.toJson().toString()}');
      response =
          await _dio.post("$serverUrl/mdh/activate", data: body.toJson());
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  /// Post zero copay in payment
  Future<Response> postZeroCopayOption() async {
    Response response;
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/zero-copay?isZeroCopay=true",
          data: null);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> postUserContact(Map<String, dynamic> body) async {
    Response response;
    print(body.toString());
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/sync-contact-list",
          data: body);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> postSendInvitationSMS(RequestWrapper body) async {
    Response response;
    print(body.toString());
    try {
      response = await _dio.post(
          "$serverUrl/user/${dataStore.getUserId()}/send-invitation-sms",
          data: body);
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  // Signup Success view
  Future<Response> getSignUpSuccessData() async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/signup/user/${dataStore.getUserId()}/congrats");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getVitaminSuccessData() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/vitamin-order-success");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getAddMemberSuccessData(int memberPatientId) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/$memberPatientId/add-member-success");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getOrderCheckoutSuccessData() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/order-checkout-success");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getCopayRequestSuccessData() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/order-checkout-success?isCopayRequested=true");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPillReminderMonthWise(int month, int year) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/pill-reminder/monthly-view?month=${month}&year=${year}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPillReminderDayWise(int day, int month, int year) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/pill-reminder/daily-view?month=${month}&year=${year}&day=${day}");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getPillReminderMedicationView() async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/pill-reminder/medications-view");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updatePillReminderStatus(RequestWrapper body) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/pill-reminder/save-status",
        data: body.toJson(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> searchPostalCode(String postalCode) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/addresses/suggestion?search=$postalCode");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> searchMedicalConditions(String condition) async {
    Response response;
    try {
      response = await _dio
          .get("$serverUrl/medical-conditions/search?query=$condition");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> searchProvinceList() async {
    Response response;
    try {
      response = await _dio.get("$serverUrl/telehealth-provinces");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> getCompleteAddress(String id) async {
    Response response;
    try {
      response = await _dio.get(
          "$serverUrl/user/${dataStore.getUserId()}/addresses/suggestion-detail/$id");
      return handleResponse(response);
    } on DioError catch (e) {
      return handleError(e, response);
    }
  }

  Future<Response> updateTelehealth(
    int prescriptionId,
    TelehealthRequest request,
  ) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/patient/${dataStore.getPatientId()}/prescription/$prescriptionId",
        data: RequestWrapper(body: request).toJson(),
      );
      return response;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> updateTelehealthAppointment(
    int prescriptionId,
    TelehealthRequestUpdate request,
  ) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user/${dataStore.getUserId()}/patient/${dataStore.getPatientId()}/telehealth/$prescriptionId/update-telehealth-appt-time",
        data: RequestWrapper(body: request).toJson(),
      );
      return response;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> updateLanguage(LanguageRequest languageRequest) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/user-profile/${dataStore.getUserId()}",
        data: RequestWrapper(body: languageRequest).toJson(),
      );
      return response;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> getLocalizationTexts(LocalizationRequest request) async {
    Response response;
    List<Map> identifierJson = new List();
    identifierJson.add(request.toJson());
    try {
      String url;
      if (serverUrl == "https://api.pocketpills.com") {
        url = "$serverUrl/content-engine/get";
      } else {
        url = "$serverUrl/prod-proxy/content-engine/get";
      }
      response = await _dio.post(
        url,
        data: jsonEncode(identifierJson).toString(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> getLocalizationTexts2(
      List<LocalizationRequest> request) async {
    Response response;
    try {
      String url;
      if (serverUrl == "https://api.pocketpills.com") {
        url = "$serverUrl/content-engine/get";
      } else {
        url = "$serverUrl/prod-proxy/content-engine/get";
      }
      response = await _dio.post(
        url,
        data: jsonEncode(request).toString(),
      );
      return handleResponse(response);
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> getUpdatesContentEngine() async {
    Response response;
    try {
      String url;
      if (serverUrl == "https://api.pocketpills.com") {
        url = "$serverUrl/content-engine/get-updated-time";
      } else {
        url = "$serverUrl/prod-proxy/content-engine/get-updated-time";
      }
      response = await _dio.get(url);
      return handleResponse(response);
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }

  Future<Response> updateMemberEmail(
      int patient_id, EmailUpdateRequest request) async {
    Response response;
    try {
      response = await _dio.put(
        "$serverUrl/patient/${patient_id}",
        data: RequestWrapper(body: request).toJson(),
      );
      return response;
    } on DioError catch (e) {
      response = e.response;
      return null;
    }
  }
}
