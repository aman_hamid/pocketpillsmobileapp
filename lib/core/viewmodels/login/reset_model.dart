import 'dart:async';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/src/response.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/reset_password_request.dart';
import 'package:pocketpills/core/request/setpassword_request.dart';
import 'package:pocketpills/core/request/signup_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/signup_service.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ResetModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final SignupService service = locator<SignupService>();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  final LoginService loginService = locator<LoginService>();

  final DataStoreService dataStore = locator<DataStoreService>();

  ConnectivityResult connectivityResult;

  Future<bool> resetPassword(String password) async {
    setState(ViewState.Busy);
    String resetKey = dataStore.getResetToken();
    String phoneNo = dataStore.readString(DataStoreService.PHONE);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    var response = await _api.resetPassword(RequestWrapper(body: ResetPasswordRequest(password: password, resetKey: resetKey, phone: int.parse(phoneNo), loginBody: deviceBody)));
    setState(ViewState.Idle);
    return handleLoginResponse(response);
  }

  Future<bool> setPassword(String password) async {
    setState(ViewState.Busy);
    String setToken = dataStore.readString(DataStoreService.SET_PASSWORD_TOKEN);
    DeviceBody deviceBody = DeviceBody();
    await deviceBody.init();
    var response = await _api.setPassword(RequestWrapper(body: SetPasswordRequest(password: password, token: setToken, loginBody: deviceBody)));
    setState(ViewState.Idle);
    return handleLoginResponse(response);
  }

  Object handleLoginResponse(Response response) {
    if (response != null) {
      BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      dataStore.writeInt(DataStoreService.USERID, res.response.userId);
      return loginService.loginUser(response);
    } else {
      return false;
    }
  }

  Future<bool> setSingupPassword(String password) async {
    setState(ViewState.Busy);
    SignupRequest signupRequest = SignupRequest();
    var response = await _api.updateUserInfo(RequestWrapper(body: signupRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
