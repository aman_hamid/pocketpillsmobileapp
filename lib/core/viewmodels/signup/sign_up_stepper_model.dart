import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/home_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class SignupStepperModel extends BaseModel {
  final LoginService loginService = locator<LoginService>();
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();
  int currentStep = 0;

  String rowValue;

  void initialize() {
    currentStep = 0;
    sendAnalytics();
  }

  setCurrentStep(int step) {
    currentStep = step;
    sendAnalytics();
    notifyListeners();
  }

  resetWithoutNotify() {
    currentStep = 4;
    currentStep = 0;
    sendAnalytics();
  }

  incrCurrentStep() {
    currentStep++;
    sendAnalytics();
    notifyListeners();
  }

  decrCurrentStep() {
    currentStep--;
    sendAnalytics();
    notifyListeners();
  }

  void sendAnalytics() {
    switch (currentStep) {
      case 0:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_about_you);
        break;
      case 1:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_new);
        break;
      case 2:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_almost_done);
        break;
      case 3:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_congratulations);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_about_you);
        break;
    }
  }

  Future<bool> showVitaminflow() async {
    Response response = await _api.getHomeData();
    if (response != null) {
      BaseResponse<HomeResponse> res = BaseResponse<HomeResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      if (res.response.freeVitamins.valid == true) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
