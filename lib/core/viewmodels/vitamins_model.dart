import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/response/free_medicine_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/locator.dart';

class VitaminsModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();

  final DataStoreService dataStore = locator<DataStoreService>();

  Map<String, Medicine> selectedVitamins = Map();
  Map<String, List<String>> vitaminsDosage = Map();
  AsyncMemoizer<List<Medicine>> _memoizer = AsyncMemoizer();
  Future<List<Medicine>> _freeVitaminsFuture;
  int prevPatientId;

  Future<List<Medicine>> fetchVitamins() async {
    if (prevPatientId != null && prevPatientId == dataStore.getPatientId()) return _freeVitaminsFuture;
    this._memoizer = AsyncMemoizer();
    _freeVitaminsFuture = this._memoizer.runOnce(() async {
      prevPatientId = dataStore.getPatientId();
      return await this.getVitaminsData();
    });
    return _freeVitaminsFuture;
  }

  void emptyVitaminsSelection() {
    selectedVitamins = Map();
    notifyListeners();
  }

  void addVitamin(Medicine vitamin) {
    selectedVitamins[vitamin.id] = vitamin;
    vitaminsDosage[vitamin.id] = dosageToTimes(vitamin);
    notifyListeners();
  }

  void removeVitamin(Medicine vitamin) {
    selectedVitamins.remove(vitamin.id);
    vitaminsDosage.remove(vitamin.id);
    notifyListeners();
  }

  void updateDosage(Medicine vitamin, List<bool> values) {
    List<String> dosageTime = [];
    if (values[0] == true) dosageTime.add("Morning");
    if (values[1] == true) dosageTime.add("Afternoon");
    if (values[2] == true) dosageTime.add("Evening");
    if (values[3] == true) dosageTime.add("Bedtime");
    vitaminsDosage[vitamin.id] = dosageTime;
    notifyListeners();
  }

  void addDosage(Medicine vitamin, List<String> dosage) {
    vitaminsDosage[vitamin.id] = dosage;
    notifyListeners();
  }

  void removeDosage(Medicine vitamin) {
    vitaminsDosage.remove(vitamin.id);
    notifyListeners();
  }

  List<String> dosageToTimes(Medicine vitamin) {
    //TODO : make this an enum
    if (vitamin.userSigCode != null) {
      List<String> dosageTime = [];
      if (vitamin.userSigCode[0] == '1') dosageTime.add("Morning");
      if (vitamin.userSigCode[1] == '1') dosageTime.add("Afternoon");
      if (vitamin.userSigCode[2] == '1') dosageTime.add("Evening");
      if (vitamin.userSigCode[3] == '1') dosageTime.add("Bedtime");
      return dosageTime;
    } else {
      Map<int, List<String>> retVal = {
        1: ["Morning"],
        2: ["Morning", "Evening"],
        3: ["Morning", "Afternoon", "Evening"],
        4: ["Morning", "Afternoon", "Evening", "Bedtime"]
      };
      return retVal[vitamin.dosage];
    }
  }

  Future<List<Medicine>> getVitaminsData() async {
    Response response = await _api.getFreeMedicinesData();
    if (response != null) {
      FreeMedicineResponse res = FreeMedicineResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      return res.medicines;
    } else {
      return null;
    }
  }

//  Future<bool> createOrder() async{
//    setState(ViewState.Busy);
//    List<Medicine> meds= selectedVitamins.values.toList();
//    List<MedicineRequest> medReq=[];
//    for(int i=0;i<meds.length;i++){
//      meds[i].modifyuserSigCode=Utils.dosageToSigCode(vitaminsDosage[meds[i].id]);
//      meds[i].modifyQuantity=vitaminsDosage[meds[i].id].length*30;
//      medReq.add(MedicineRequest(userSigCode: meds[i].sigCode, quantity: meds[i].quantity, din: meds[i].din,drugName: meds[i].name));
//    }
//    var response = await _api.createOrder(RequestWrapper(body:OrderRequest(dinMedications: medReq)));
//    setState(ViewState.Idle);
//    if (response != null) {
//      OldBaseResponse res=OldBaseResponse.fromJson(response.data);
//      if(!res.status) {
//        errorMessage = res.getErrorMessage();
//        return false;
//      }
//      emptyVitaminsSelection();
//      return true;
//    }else{
//      return false;
//    }
//  }
}
