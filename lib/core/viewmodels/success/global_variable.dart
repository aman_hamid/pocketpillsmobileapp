class GlobalVariable {
  static GlobalVariable _instance;

  factory GlobalVariable() => _instance ??= new GlobalVariable._();

  GlobalVariable._();

  int _prescriptionId;
  String _userFlow;

  int get prescriptionId => _prescriptionId;

  set prescriptionId(int value) {
    _prescriptionId = value;
  }

  String get userFlow => _userFlow;

  set userFlow(String value) {
    _userFlow = value;
  }
}
