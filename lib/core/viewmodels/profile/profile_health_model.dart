import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/models/health_info.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/update_health_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/patient_health_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileHealthModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<HealthInfo> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  Future<HealthInfo> _futureHealthInfo;

  int prevPatientId;
  int radioOption = 0;
  int selectedId = -1;

  HealthInfo healthInfo;

  ConnectivityResult connectivityResult;

  void setRadioOption(int option) {
    radioOption = option;
    selectedId = option;
    notifyListeners();
  }

  Future<HealthInfo> fetchHealthData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId)
      return _futureHealthInfo;
    this._memoizer = AsyncMemoizer();
    _futureHealthInfo = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      return await this.getPatientHealth();
    });
    return _futureHealthInfo;
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    prevPatientId = null;
    notifyListeners();
  }

  Future<bool> setPatientHealth(
      List<String> allergies, List<String> vitamins) async {
    setState(ViewState.Busy);
    Response response = await _api.setPatientHealth(RequestWrapper(
        body: UpdateHealthRequest(
            allergies: allergies,
            vitamins: vitamins,
            patientId: dataStore.readInteger(DataStoreService.PATIENTID))));
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<PatientHealthResponse> res =
          BaseResponse<PatientHealthResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      prevPatientId = null;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<HealthInfo> getPatientHealth() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPatientHealth();
    if (response != null) {
      BaseResponse<PatientHealthResponse> res =
          BaseResponse<PatientHealthResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      HealthInfo hInfo = res.response.healthInfo;
      if (hInfo == null) hInfo = HealthInfo();
      radioOption = selectedId == -1
          ? hInfo.allergies != null
              ? hInfo.allergies.length
              : 0
          : selectedId;
      healthInfo = hInfo;
      return hInfo;
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData =
            LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request =
              LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
