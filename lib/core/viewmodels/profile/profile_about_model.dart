import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/user_language.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/update_email_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileAboutModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  AsyncMemoizer<EmployerSuggestionResponse> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  ConnectivityResult connectivityResult;

  String language = ViewConstants.languageIdEn;
  String email;

  Future<String> updateInvitationCode(String invitationCode) async {
    setState(ViewState.Busy);
    var response = await _api.activateInvitationCode(invitationCode);
    if (response != null) {
      setState(ViewState.Idle);
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return errorMessage;
      }
      return res.errMessage;
    } else {
      setState(ViewState.Idle);
      return LocalizationUtils.getSingleValueString(
          "common", "common.label.api-error");
    }
  }

  Future<EmployerSuggestionResponse> getSuggestedEmployers() async {
    return this._memoizer.runOnce(() async {
      Response response = await _api.getSuggestedEmployers();
      if (response != null) {
        BaseResponse<EmployerSuggestionResponse> res =
            BaseResponse<EmployerSuggestionResponse>.fromJson(response.data);

        if (!res.status) {
          errorMessage = res.getErrorMessage();
          return null;
        }

        return res.response;
      } else {
        return null;
      }
    });
  }

  String getStoredLanguage() {
    var data = getSelectedLanguage();
    return data;
  }

  void setLanguage(BuildContext context, String locale) async {
    changeLanguage(context, locale);
    notifyListeners();
  }

  Future<bool> updateLanguage(String locale) async {
    setState(ViewState.Busy);
    LanguageRequest languageRequest = LanguageRequest();
    languageRequest.locale = locale == "en" ? "en_CA" : "fr_CA";
    var response = await _api.updateLanguage(languageRequest);
    if (response != null) {
      setState(ViewState.Idle);
      if (response != null) {
        OldBaseResponse res = OldBaseResponse.fromJson(response.data);
        if (!res.status) {
          errorMessage = res.getErrorMessage();
          return false;
        }
        errorMessage = res.apiMessage;
        return true;
      } else {
        return false;
      }
    } else {
      return null;
    }
  }

  Future<bool> updateMemberEmail(int patient_id, String email) async {
    setState(ViewState.Busy);
    EmailUpdateRequest emailUpdateRequest = EmailUpdateRequest();
    emailUpdateRequest.email = email;
    var response = await _api.updateMemberEmail(patient_id, emailUpdateRequest);
    if (response != null) {
      setState(ViewState.Idle);
      if (response != null) {
        OldBaseResponse res = OldBaseResponse.fromJson(response.data);
        if (!res.status) {
          errorMessage = res.getErrorMessage();
          return false;
        }
        errorMessage = res.apiMessage;
        return true;
      } else {
        return false;
      }
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData =
            LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request =
              LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }

  void setLanguageDropDown(String language) {
    if (ViewConstants.languageMap.containsKey(language)) {
      this.language = language;
      notifyListeners();
    } else {
      this.language = ViewConstants.languageId;
      notifyListeners();
    }
  }
}
