import 'package:enum_to_string/enum_to_string.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';

part 'header.g.dart';

@JsonSerializable()
class Header extends BaseRequest {
  String heading;
  String buttonAction;
  String buttonText;
  String headingType;

  ActionType getActionType() {
    return EnumToString.fromString(ActionType.values, buttonAction);
  }

  HeadingType getHeadingType() {
    return EnumToString.fromString(HeadingType.values, headingType);
  }

  Header({this.heading, this.buttonAction, this.buttonText, this.headingType});

  factory Header.fromJson(Map<String, dynamic> json) => _$HeaderFromJson(json);

  Map<String, dynamic> toJson() => _$HeaderToJson(this);
}
