import 'package:enum_to_string/enum_to_string.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/dashboard/header.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';

part 'dashboard_item_list.g.dart';

@JsonSerializable()
class DashboardItemList {
  String layoutType;
  @JsonKey(name: 'header')
  Header header;
  List<DashboardItem> data;

  LayoutType getLayoutType() {
    return EnumToString.fromString(LayoutType.values, layoutType);
  }

  DashboardItemList({this.layoutType, this.header, this.data});

  factory DashboardItemList.fromJson(Map<String, dynamic> json) => _$DashboardItemListFromJson(json);

  Map<String, dynamic> toJson() => _$DashboardItemListToJson(this);
}
