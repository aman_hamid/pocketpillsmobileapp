// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_patient.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserPatient _$UserPatientFromJson(Map<String, dynamic> json) {
  return UserPatient(
    id: json['id'] as int,
    disabled: json['disabled'] as bool,
    userId: json['userId'] as int,
    patientId: json['patientId'] as int,
    primary: json['primary'] as bool,
    patient: json['patient'] == null
        ? null
        : Patient.fromJson(json['patient'] as Map<String, dynamic>),
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    hasViewedTransferRefills: json['hasViewedTransferRefills'] as bool,
    hasProvidedTransferInformation:
        json['hasProvidedTransferInformation'] as bool,
    defaultCreditCardId: json['defaultCreditCardId'] as int,
    userRelation: json['userRelation'] as String,
    consent: json['consent'] as bool,
  );
}

Map<String, dynamic> _$UserPatientToJson(UserPatient instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'userId': instance.userId,
      'patientId': instance.patientId,
      'primary': instance.primary,
      'patient': instance.patient,
      'user': instance.user,
      'hasViewedTransferRefills': instance.hasViewedTransferRefills,
      'hasProvidedTransferInformation': instance.hasProvidedTransferInformation,
      'defaultCreditCardId': instance.defaultCreditCardId,
      'userRelation': instance.userRelation,
      'consent': instance.consent,
    };
