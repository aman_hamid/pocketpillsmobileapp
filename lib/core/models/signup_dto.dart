import 'package:json_annotation/json_annotation.dart';
part 'signup_dto.g.dart';

@JsonSerializable()
class SignupDto {
  int phone;
  String email;
  String firstName;
  String lastName;
  String gender;
  String password;
  String birthDate;
  bool hasDailyMedication;
  bool hasPassword;
  bool isCaregiver;
  String province;
  bool isMailVerified;
  String locale;
  bool verified;

  SignupDto(
      this.password,
      this.phone,
      this.email,
      this.firstName,
      this.lastName,
      this.gender,
      this.hasPassword,
      this.birthDate,
      this.hasDailyMedication,
      this.isCaregiver,
      this.province,
      this.isMailVerified,
      this.locale,
      this.verified);
  factory SignupDto.fromJson(Map<String, dynamic> json) => _$SignupDtoFromJson(json);

  Map<String, dynamic> toJson() => _$SignupDtoToJson(this);
}
