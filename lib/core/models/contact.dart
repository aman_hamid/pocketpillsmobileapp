import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'contact.g.dart';

@JsonSerializable()
class Contact extends BaseRequest {
  String day;
  String timeHigh;
  String timeLow;
  int userId;
  int id;
  bool disabled;
  String prescriptionPreference;
  String deliveryBy;
  String userFlow;
  int entityId;

  Contact(
      {this.id,
      this.userId,
      this.day,
      this.disabled,
      this.timeHigh,
      this.timeLow,
      this.prescriptionPreference,
      this.deliveryBy,
      this.userFlow,
      this.entityId});

  factory Contact.fromJson(Map<String, dynamic> json) => _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);
}
