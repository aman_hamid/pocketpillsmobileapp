// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prescription_medication.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrescriptionMedication _$PrescriptionMedicationFromJson(
    Map<String, dynamic> json) {
  return PrescriptionMedication(
    json['rxId'] as int,
    json['drug'] as String,
    json['remainingQty'] as num,
    json['filledQty'] as num,
    PPDateUtils.fromStr(json['validTill'] as String),
    json['doctorFirstName'] as String,
    json['doctorLastName'] as String,
  );
}

Map<String, dynamic> _$PrescriptionMedicationToJson(
        PrescriptionMedication instance) =>
    <String, dynamic>{
      'rxId': instance.medicationId,
      'drug': instance.medicationName,
      'remainingQty': instance.remainingQty,
      'filledQty': instance.filledQty,
      'validTill': PPDateUtils.toStr(instance.validTill),
      'doctorFirstName': instance.doctorFirstName,
      'doctorLastName': instance.doctorLastName,
    };
