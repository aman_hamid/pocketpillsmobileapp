import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/response/prescription/prescription_document.dart';
import 'package:pocketpills/core/models/response/prescription/prescription_medication.dart';

part 'prescription.g.dart';

@JsonSerializable()
class Prescription {
  int id;
  DateTime createDateTime;
  String type;
  String status;
  String exPharmacyName;
  String exPharmacyPhone;
  String exPharmacyAddress;
  String doctorsName;
  @JsonKey(name: "mappedRxs")
  List<PrescriptionMedication> medications;
  List<PrescriptionDocument> documents;

  factory Prescription.fromJson(Map<String, dynamic> json) => _$PrescriptionFromJson(json);

  Map<String, dynamic> toJson() => _$PrescriptionToJson(this);

  Prescription(this.id, this.createDateTime, this.type, this.status, this.exPharmacyName, this.exPharmacyPhone,
      this.exPharmacyAddress, this.doctorsName, this.medications, this.documents);
}
