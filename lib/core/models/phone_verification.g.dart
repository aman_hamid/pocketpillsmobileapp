// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_verification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneVerification _$PhoneVerificationFromJson(Map<String, dynamic> json) {
  return PhoneVerification(
    userMessage: json['userMessage'] as String,
    redirect: json['redirect'] as String,
    userId: json['userId'] as int,
  );
}

Map<String, dynamic> _$PhoneVerificationToJson(PhoneVerification instance) =>
    <String, dynamic>{
      'userMessage': instance.userMessage,
      'redirect': instance.redirect,
      'userId': instance.userId,
    };
