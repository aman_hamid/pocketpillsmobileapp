// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HealthInfo _$HealthInfoFromJson(Map<String, dynamic> json) {
  return HealthInfo(
    patientId: json['patientId'] as int,
    id: json['id'] as int,
    disabled: json['disabled'] as bool,
    allergies: (json['allergies'] as List)?.map((e) => e as String)?.toList(),
    vitamins: (json['vitamins'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$HealthInfoToJson(HealthInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'patientId': instance.patientId,
      'allergies': instance.allergies,
      'vitamins': instance.vitamins,
    };
