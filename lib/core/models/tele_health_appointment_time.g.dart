// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tele_health_appointment_time.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentTime _$AppointmentTimeFromJson(Map<String, dynamic> json) {
  return AppointmentTime(
    PPDateUtils.fromStr(json['actualAppointmentTime'] as String),
    json['appointmentDate'] as String,
    json['available'] as bool,
    json['clinicName'] as String,
    json['displayDate'] as String,
    json['doctorName'] as String,
    json['index'] as int,
    json['timeSlot'] as String,
  )..status = json['status'] as String;
}

Map<String, dynamic> _$AppointmentTimeToJson(AppointmentTime instance) =>
    <String, dynamic>{
      'actualAppointmentTime':
          PPDateUtils.toStr(instance.actualAppointmentTime),
      'appointmentDate': instance.appointmentDate,
      'available': instance.available,
      'clinicName': instance.clinicName,
      'displayDate': instance.displayDate,
      'doctorName': instance.doctorName,
      'index': instance.index,
      'status': instance.status,
      'timeSlot': instance.timeSlot,
    };
