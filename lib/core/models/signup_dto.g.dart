// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupDto _$SignupDtoFromJson(Map<String, dynamic> json) {
  return SignupDto(
    json['password'] as String,
    json['phone'] as int,
    json['email'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['gender'] as String,
    json['hasPassword'] as bool,
    json['birthDate'] as String,
    json['hasDailyMedication'] as bool,
    json['isCaregiver'] as bool,
    json['province'] as String,
    json['isMailVerified'] as bool,
    json['locale'] as String,
    json['verified'] as bool,
  );
}

Map<String, dynamic> _$SignupDtoToJson(SignupDto instance) => <String, dynamic>{
      'phone': instance.phone,
      'email': instance.email,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'gender': instance.gender,
      'password': instance.password,
      'birthDate': instance.birthDate,
      'hasDailyMedication': instance.hasDailyMedication,
      'hasPassword': instance.hasPassword,
      'isCaregiver': instance.isCaregiver,
      'province': instance.province,
      'isMailVerified': instance.isMailVerified,
      'locale': instance.locale,
      'verified': instance.verified,
    };
