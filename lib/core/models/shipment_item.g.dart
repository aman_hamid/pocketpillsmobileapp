// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipment_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShipmentItem _$ShipmentItemFromJson(Map<String, dynamic> json) {
  return ShipmentItem(
    quantity: json['quantity'] as num,
    id: json['id'] as int,
    orderItem: json['orderItem'] == null
        ? null
        : OrderItem.fromJson(json['orderItem'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShipmentItemToJson(ShipmentItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'quantity': instance.quantity,
      'orderItem': instance.orderItem,
    };
