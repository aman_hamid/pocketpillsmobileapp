// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderPayment _$OrderPaymentFromJson(Map<String, dynamic> json) {
  return OrderPayment(
    id: json['id'] as int,
    transactionId: json['transactionId'] as String,
    amount: json['amount'] as num,
    paymentMode: json['paymentMode'] as String,
    status: json['status'] as String,
    customerPaymentModeId: json['customerPaymentModeId'] as int,
    paymentDate: json['paymentDate'] as String,
    paymentDetails: json['paymentDetails'] == null
        ? null
        : PaymentCard.fromJson(json['paymentDetails'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OrderPaymentToJson(OrderPayment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'transactionId': instance.transactionId,
      'amount': instance.amount,
      'paymentMode': instance.paymentMode,
      'status': instance.status,
      'paymentDetails': instance.paymentDetails,
      'customerPaymentModeId': instance.customerPaymentModeId,
      'paymentDate': instance.paymentDate,
    };
