// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponCode _$CouponCodeFromJson(Map<String, dynamic> json) {
  return CouponCode(
    json['id'] as int,
    json['disabled'] as bool,
    json['userSuppliedCouponCode'] as String,
    json['isApplied'] as bool,
    json['reason'] as String,
    json['applied'] as bool,
  );
}

Map<String, dynamic> _$CouponCodeToJson(CouponCode instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'userSuppliedCouponCode': instance.userSuppliedCouponCode,
      'isApplied': instance.isApplied,
      'reason': instance.reason,
      'applied': instance.applied,
    };
