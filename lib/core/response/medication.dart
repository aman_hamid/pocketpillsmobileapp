import 'dart:core';

import 'package:json_annotation/json_annotation.dart';

part 'medication.g.dart';

@JsonSerializable()
class Medication {
  @JsonKey(required: false)
  int id;
  int rxId;
  int medicationId;
  String drug;
  String drugId;
  int refillsLeft;
  String drugType;
  num quantityLeft;
  num quantityFilled;
  num quantityPerFill;
  int daysSupply;
  num totalQuantity;
  num initialDispensedQuantity;
  String sig;
  String validUntil;
  String doctor;
  String message;
  String statusText;
  int type;
  String dgType;
  String refillQuantity;
  int variantId;
  int isSmartPack;

  Medication(
      this.rxId,
      this.medicationId,
      this.drug,
      this.drugId,
      this.refillsLeft,
      this.drugType,
      this.quantityLeft,
      this.quantityFilled,
      this.quantityPerFill,
      this.daysSupply,
      this.totalQuantity,
      this.initialDispensedQuantity,
      this.sig,
      this.validUntil,
      this.doctor,
      this.message,
      this.statusText,
      this.type,
      this.dgType,
      this.refillQuantity,
      this.variantId,
      this.isSmartPack);

  factory Medication.fromJson(Map<String, dynamic> json) => _$MedicationFromJson(json);
}
