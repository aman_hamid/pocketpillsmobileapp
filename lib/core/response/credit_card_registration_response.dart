import 'package:json_annotation/json_annotation.dart';

part 'credit_card_registration_response.g.dart';

@JsonSerializable()
class CreditCardRegistrationResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'html')
  String html;

  CreditCardRegistrationResponse({this.errMessage, this.status, this.apiMessage, this.html});

  factory CreditCardRegistrationResponse.fromJson(Map<String, dynamic> json) =>
      _$CreditCardRegistrationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CreditCardRegistrationResponseToJson(this);
}
