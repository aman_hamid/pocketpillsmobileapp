import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'address_complete.g.dart';

@JsonSerializable()
class AddressComplete {
  String province;
  String postalCode;
  String streetAddress;
  String streetAddressLineTwo;
  String city;
  String country;

  AddressComplete(
      this.province, this.postalCode, this.streetAddress, this.streetAddressLineTwo, this.city, this.country);

  factory AddressComplete.fromJson(Map<String, dynamic> json) => _$AddressCompleteFromJson(json);
}
