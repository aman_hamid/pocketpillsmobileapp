// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pincode_suggestion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PinCodeSuggestionResponse _$PinCodeSuggestionResponseFromJson(
    Map<String, dynamic> json) {
  return PinCodeSuggestionResponse(
    (json['items'] as List)
        ?.map((e) => e == null
            ? null
            : PinCodeSuggestion.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PinCodeSuggestionResponseToJson(
        PinCodeSuggestionResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
