// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_create_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthCreateResponse _$TelehealthCreateResponseFromJson(
    Map<String, dynamic> json) {
  return TelehealthCreateResponse(
    json['omsPrescriptionId'] as int,
    json['successDetails'] == null
        ? null
        : SuccessDetails.fromJson(
            json['successDetails'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TelehealthCreateResponseToJson(
        TelehealthCreateResponse instance) =>
    <String, dynamic>{
      'omsPrescriptionId': instance.omsPrescriptionId,
      'successDetails': instance.successDetails,
    };
