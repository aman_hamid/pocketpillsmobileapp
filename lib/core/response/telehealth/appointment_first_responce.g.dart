// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_first_responce.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentFirstResponse _$AppointmentFirstResponseFromJson(
    Map<String, dynamic> json) {
  return AppointmentFirstResponse(
    prescriptionDTO: json['prescriptionDTO'] == null
        ? null
        : TelehealthAppointmentResponse.fromJson(
            json['prescriptionDTO'] as Map<String, dynamic>),
    timeSlot: json['timeSlot'] as String,
    slotDate: json['slotDate'] as String,
  );
}

Map<String, dynamic> _$AppointmentFirstResponseToJson(
        AppointmentFirstResponse instance) =>
    <String, dynamic>{
      'prescriptionDTO': instance.prescriptionDTO,
      'timeSlot': instance.timeSlot,
      'slotDate': instance.slotDate,
    };
