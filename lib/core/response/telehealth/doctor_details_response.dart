import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'doctor_details_response.g.dart';

@JsonSerializable()
class DoctorResponse {
  String doctorNameWithDr;

  factory DoctorResponse.fromJson(Map<String, dynamic> json) => _$DoctorResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DoctorResponseToJson(this);
  DoctorResponse(this.doctorNameWithDr,);
}
