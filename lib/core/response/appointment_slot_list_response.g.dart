// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_slot_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentSlotListResponse _$AppointmentSlotListResponseFromJson(
    Map<String, dynamic> json) {
  return AppointmentSlotListResponse(
    items: (json['items'] as List)
        ?.map((e) => e == null
            ? null
            : AppointmentTime.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AppointmentSlotListResponseToJson(
        AppointmentSlotListResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
