import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'free_medicine_response.g.dart';

@JsonSerializable()
class FreeMedicineResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'details')
  List<Medicine> medicines;

  FreeMedicineResponse({this.status, this.errMessage, this.apiMessage, this.medicines});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory FreeMedicineResponse.fromJson(Map<String, dynamic> json) => _$FreeMedicineResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FreeMedicineResponseToJson(this);
}
