import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/user_patient.dart';
part 'user_patient_response.g.dart';


@JsonSerializable()
class UserPatientResponse extends Object{

  @JsonKey(name: 'patientList')
  List<UserPatient> userPatientList;

  UserPatientResponse({this.userPatientList});
  factory UserPatientResponse.fromJson(Map<String, dynamic> json) =>
      _$UserPatientResponseFromJson(json);
}

