import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/user_patient.dart';
part 'login_response.g.dart';


@JsonSerializable()
class LoginResponse extends Object{

  @JsonKey(name: 'userId')
  int userId;

  @JsonKey(name: 'patientList')
  List<UserPatient> userPatientList;

  LoginResponse({this.userId,this.userPatientList});
  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

}

