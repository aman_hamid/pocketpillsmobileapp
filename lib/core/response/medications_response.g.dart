// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medications_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationsResponse _$MedicationsResponseFromJson(Map<String, dynamic> json) {
  return MedicationsResponse(
    (json['medicines'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k,
          (e as List)
              ?.map((e) => e == null
                  ? null
                  : Medication.fromJson(e as Map<String, dynamic>))
              ?.toList()),
    ),
  );
}

Map<String, dynamic> _$MedicationsResponseToJson(
        MedicationsResponse instance) =>
    <String, dynamic>{
      'medicines': instance.medicines,
    };
