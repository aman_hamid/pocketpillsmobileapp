// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_address_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientAddressResponse _$PatientAddressResponseFromJson(
    Map<String, dynamic> json) {
  return PatientAddressResponse(
    addresses: (json['addresses'] as List)
        ?.map((e) =>
            e == null ? null : Address.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PatientAddressResponseToJson(
        PatientAddressResponse instance) =>
    <String, dynamic>{
      'addresses': instance.addresses,
    };
