import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/address.dart';
part 'patient_address_response.g.dart';

@JsonSerializable()
class PatientAddressResponse {
  List<Address> addresses;

  PatientAddressResponse({this.addresses});

  factory PatientAddressResponse.fromJson(Map<String, dynamic> json) => _$PatientAddressResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PatientAddressResponseToJson(this);
}
