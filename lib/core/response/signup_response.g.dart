// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupResponse _$SignupResponseFromJson(Map<String, dynamic> json) {
  return SignupResponse(
    json['userId'] as int,
    json['patientId'] as int,
    json['prescription'] == null
        ? null
        : TransferPrescription.fromJson(
            json['prescription'] as Map<String, dynamic>),
    json['signupDto'] == null
        ? null
        : SignupDto.fromJson(json['signupDto'] as Map<String, dynamic>),
  )
    ..status = json['success'] as bool
    ..errMessage = json['userMessage'] as String
    ..apiMessage = json['message'] as String;
}

Map<String, dynamic> _$SignupResponseToJson(SignupResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'userId': instance.userId,
      'patientId': instance.patientId,
      'prescription': instance.prescription,
      'signupDto': instance.signupDto,
    };
