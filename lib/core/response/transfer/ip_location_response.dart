import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/transfer/location_response.dart';
part 'ip_location_response.g.dart';

@JsonSerializable()
class IpLocationResponse {
  LocationResponse response;

  IpLocationResponse(this.response);

  factory IpLocationResponse.fromJson(Map<String, dynamic> json) => _$IpLocationResponseFromJson(json);
}
