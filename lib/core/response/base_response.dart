import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/dashboard/dashboard_response.dart';
import 'package:pocketpills/core/models/contact.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/response/activate_patient_response.dart';
import 'package:pocketpills/core/response/add_patient_responses.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion_response.dart';
import 'package:pocketpills/core/response/address_delete_response.dart';
import 'package:pocketpills/core/response/advertise/pp_distinct_entity_response.dart';
import 'package:pocketpills/core/response/home_response.dart';
import 'package:pocketpills/core/response/insurance/copy_insurance_candidate_response.dart';
import 'package:pocketpills/core/response/insurance/insurance_code_activation_details_response.dart';
import 'package:pocketpills/core/response/insurance_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/medications_response.dart';
import 'package:pocketpills/core/response/patient_address_response.dart';
import 'package:pocketpills/core/response/patient_card_response.dart';
import 'package:pocketpills/core/response/patient_health_response.dart';
import 'package:pocketpills/core/response/payment_delete_response.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/month_wise_medication_response.dart';
import 'package:pocketpills/core/response/pillreminder/pill_reminder_medications_response.dart';
import 'package:pocketpills/core/response/prescription_create_response.dart';
import 'package:pocketpills/core/response/referral_response.dart';
import 'package:pocketpills/core/response/refill/medication_refill_response.dart';
import 'package:pocketpills/core/response/signedin_verify_response.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/response/signup/transaction_successful_response.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/response/signup_transfer_image_response.dart';
import 'package:pocketpills/core/response/telehealth/localization_list_response.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/response/transfer/ip_location_response.dart';
import 'package:pocketpills/core/response/user_patient_response.dart';
import 'package:pocketpills/core/response/vitamins/add_medicine_response.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_benefit_filter_response.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_subscription_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import 'chambers/chambers_activate_response.dart';

part 'base_response.g.dart';

@JsonSerializable()
class BaseResponse<T> {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'details')
  @_Converter()
  final T response;

  BaseResponse({this.status, this.errMessage, this.apiMessage, this.response});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString(
          "common", "common.label.api-error");
  }

  factory BaseResponse.fromJson(Map<String, dynamic> json) =>
      _$BaseResponseFromJson<T>(json);

  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);
}

class _Converter<T extends Object> implements JsonConverter<T, Object> {
  const _Converter();

  Type myFunc<T>() => T;

  @override
  T fromJson(Object json) {
//    print(T.runtimeType);
//    T a;
//    print(a is LoginResponse);
    // print(myFunc() is LoginResponse);
    if (json == null) return null;
    switch (T) {
      case AddPatientResponse:
        return AddPatientResponse.fromJson(json) as T;
      case UserPatientResponse:
        return UserPatientResponse.fromJson(json) as T;
      case LoginResponse:
        return LoginResponse.fromJson(json) as T;
      case SignedinVerifyResponse:
        return SignedinVerifyResponse.fromJson(json) as T;
      case PatientHealthResponse:
        return PatientHealthResponse.fromJson(json) as T;
      case PatientAddressResponse:
        return PatientAddressResponse.fromJson(json) as T;
      case PatientCardResponse:
        return PatientCardResponse.fromJson(json) as T;
      case AddressDeleteResponse:
        return AddressDeleteResponse.fromJson(json) as T;
      case MedicationsResponse:
        return MedicationsResponse.fromJson(json) as T;
      case PaymentDeleteResponse:
        return PaymentDeleteResponse.fromJson(json) as T;
      case HomeResponse:
        return HomeResponse.fromJson(json) as T;
      case ActivatePatientResponse:
        return ActivatePatientResponse.fromJson(json) as T;
      case Contact:
        return Contact.fromJson(json) as T;
      case Insurance:
        return Insurance.fromJson(json) as T;
      case InsuranceResponse:
        return InsuranceResponse.fromJson(json) as T;
      case PrescriptionCreateResponse:
        return PrescriptionCreateResponse.fromJson(json) as T;
      case TelehealthCreateResponse:
        return TelehealthCreateResponse.fromJson(json) as T;
      case ReferralResponse:
        return ReferralResponse.fromJson(json) as T;
      case SignupPhoneNumberResponse:
        return SignupPhoneNumberResponse.fromJson(json) as T;
      case SignupResponse:
        return SignupResponse.fromJson(json) as T;
      case SignupTransferImageResponse:
        return SignupTransferImageResponse.fromJson(json) as T;
      case AddMedicineResponse:
        return AddMedicineResponse.fromJson(json) as T;
      case CopyInsuranceCandidateResponse:
        return CopyInsuranceCandidateResponse.fromJson(json) as T;
      case EmployerSuggestionResponse:
        return EmployerSuggestionResponse.fromJson(json) as T;
      case InsuranceCodeActivationDetailsResponse:
        return InsuranceCodeActivationDetailsResponse.fromJson(json) as T;
      case VitaminsBenefitFilterResponse:
        return VitaminsBenefitFilterResponse.fromJson(json) as T;
      case VitaminsSubscriptionResponse:
        return VitaminsSubscriptionResponse.fromJson(json) as T;
      case IpLocationResponse:
        return IpLocationResponse.fromJson(json) as T;
      case DashboardResponse:
        return DashboardResponse.fromJson(json) as T;
      case PPDistinctEntityResponse:
        return PPDistinctEntityResponse.fromJson(json) as T;
      case MedicationRefillResponse:
        return MedicationRefillResponse.fromJson(json) as T;
      case TransactionSuccessfulResponse:
        return TransactionSuccessfulResponse.fromJson(json) as T;
      case DayWiseMedications:
        return DayWiseMedications.fromJson(json) as T;
      case MonthWiseMedicationResponse:
        return MonthWiseMedicationResponse.fromJson(json) as T;
      case PillReminderMedicationResponse:
        return PillReminderMedicationResponse.fromJson(json) as T;
      case PinCodeSuggestionResponse:
        return PinCodeSuggestionResponse.fromJson(json) as T;
      case TelehealthCreateResponse:
        return TelehealthCreateResponse.fromJson(json) as T;
      case LocalizationListResponse:
        return LocalizationListResponse.fromJson(json) as T;
      case MedicalConditionSuggestionResponse:
        return MedicalConditionSuggestionResponse.fromJson(json) as T;
      case ProvinceResponse:
        return ProvinceResponse.fromJson(json) as T;
      case ChambersResponse:
        return ChambersResponse.fromJson(json) as T;
      default:
        return json as T;
    }
  }

//SignupResponse
  @override
  Object toJson(T object) {
    // This will only work if `object` is a native JSON type:
    //   num, String, bool, null, etc
    // Or if it has a `toJson()` function`.
    return object;
  }
}
