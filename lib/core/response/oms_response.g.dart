// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oms_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OMSResponse<T> _$OMSResponseFromJson<T>(Map<String, dynamic> json) {
  return OMSResponse<T>(
    status: json['success'] as bool,
    errMessage: json['userMessage'] as String,
    apiMessage: json['message'] as String,
    response: _Converter<T>().fromJson(json['data']),
  )..error = json['error'] as String;
}

Map<String, dynamic> _$OMSResponseToJson<T>(OMSResponse<T> instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'error': instance.error,
      'data': _Converter<T>().toJson(instance.response),
    };
