import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_request.g.dart';

@JsonSerializable()
class TelehealthRequest extends BaseRequest {
  String prescriptionType;
  bool prescriptionFilledByExternalPharmacy;
  String prescriptionRequestReason;
  String prescriptionState;
  String prescriptionRequestCategory;
  String prescriptionMedicalConditions;
  String telehealthRequestedMedications;
  String prescriptionComment;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime appointmentTime;
  bool isPrescriptionEdited;

  TelehealthRequest(
      {this.prescriptionType,
      this.prescriptionFilledByExternalPharmacy,
      this.prescriptionRequestReason,
      this.prescriptionState,
      this.prescriptionRequestCategory,
      this.prescriptionMedicalConditions,
      this.telehealthRequestedMedications,
      this.prescriptionComment,
      this.appointmentTime,
        this. isPrescriptionEdited
      });

  factory TelehealthRequest.fromJson(Map<String, dynamic> data) =>
      _$TelehealthRequestFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthRequestToJson(this);
}
