import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'medicine_request.g.dart';

@JsonSerializable(explicitToJson: true)
class MedicineRequest extends BaseRequest {
  String din;
  String drugName;
  num quantity;
  String userSigCode;

  MedicineRequest({this.quantity, this.drugName, this.din, this.userSigCode});

  factory MedicineRequest.fromJson(Map<String, dynamic> json) => _$MedicineRequestFromJson(json);
  Map<String, dynamic> toJson() => _$MedicineRequestToJson(this);
}
