import 'package:pocketpills/core/request/base_request.dart';


class SignUpEmailRequest extends BaseRequest{
  String email;



  SignUpEmailRequest({this.email});

  Map<String, dynamic> toJson() =>
      <String, dynamic>{
        'email': this.email
      };

}