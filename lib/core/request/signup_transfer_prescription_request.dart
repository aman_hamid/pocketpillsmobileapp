import 'package:pocketpills/core/request/base_request.dart';


class SignupTransferPrescriptionRequest extends BaseRequest{

  String type;
  String pharmacyName;
  String pharmacyAddress;
  int pharmacyPhone;
  String prescriptionComment;
  bool isTransferAll;
  String prescriptionState;
  String placeId;

  SignupTransferPrescriptionRequest({this.type,
    this.pharmacyName,
    this.pharmacyPhone,
    this.isTransferAll,
    this.prescriptionState,
    this.prescriptionComment,
    this.pharmacyAddress,
    this.placeId});

  Map<String, dynamic> toJson() =>
      <String, dynamic>{
        'type': this.type,
        'pharmacyName': this.pharmacyName,
        'prescriptionComment': this.prescriptionComment,
        'pharmacyPhone': this.pharmacyPhone,
        'pharmacyAddress': this.pharmacyAddress,
        'isTransferAll': this.isTransferAll.toString(),
        'prescriptionState': this.prescriptionState,
        'pharmacyGooglePlaceId' : this.placeId
      };

}