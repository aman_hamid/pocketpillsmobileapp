// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_health_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateHealthRequest _$UpdateHealthRequestFromJson(Map<String, dynamic> json) {
  return UpdateHealthRequest(
    allergies: (json['allergies'] as List)?.map((e) => e as String)?.toList(),
    patientId: json['patientId'] as int,
    vitamins: (json['vitamins'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$UpdateHealthRequestToJson(
        UpdateHealthRequest instance) =>
    <String, dynamic>{
      'allergies': instance.allergies,
      'patientId': instance.patientId,
      'vitamins': instance.vitamins,
    };
