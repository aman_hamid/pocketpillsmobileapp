// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCardRequest _$CreditCardRequestFromJson(Map<String, dynamic> json) {
  return CreditCardRequest(
    isDefault: json['isDefault'] as bool,
    patientId: json['patientId'] as String,
    postalCode: json['postalCode'] as String,
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$CreditCardRequestToJson(CreditCardRequest instance) =>
    <String, dynamic>{
      'isDefault': instance.isDefault,
      'patientId': instance.patientId,
      'postalCode': instance.postalCode,
      'token': instance.token,
    };
