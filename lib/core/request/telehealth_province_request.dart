import 'package:pocketpills/core/request/base_request.dart';

class TelehealthProvinceRequest extends BaseRequest {
  String province;
  String signupFlow;

  TelehealthProvinceRequest({this.province, this.signupFlow});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'province': this.province,
        'signupFlow': this.signupFlow
      };
}
