import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/signup_service.dart';
import 'package:pocketpills/core/services/user_service.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_config.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'core/services/notification_service.dart';
import 'core/services/shared_prefs.dart';
import 'utils/analytics.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerLazySingleton(() => HttpApi());
  locator.registerFactory(() => LoginService());
  locator.registerFactory(() => SignupService());
  locator.registerLazySingleton(() => NotificationService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => NavigationService());

  //Fire base analytics
  locator.registerLazySingleton(() => Analytics());
}

void setupAppConfig(AppConfig appConfig) {
  locator.registerSingleton(appConfig);
}

Future<void> setupSharedPreferences() async {
  final prefs = await SharedPreferences.getInstance();
  locator.registerSingleton(prefs);
  locator.registerSingleton(DataStoreService());
}

Future<void> initPlatformState() async {
  String currentLocale;
  try {
    DataStoreService dataStoreService = locator<DataStoreService>();
    currentLocale = await Devicelocale.currentLocale;
    if (dataStoreService.readString(DataStoreService.LANGUAGE) == null) {
      if (currentLocale.contains("fr")) {
        ViewConstants.languageId = "fr";
      } else {
        ViewConstants.languageId = "en";
      }
    } else {
      ViewConstants.languageId = dataStoreService.readString(DataStoreService.LANGUAGE);
    }
    print(currentLocale);
  } on PlatformException {
    ViewConstants.languageId = "en";
    print("Error obtaining current locale");
  }
}
