# pocketpills

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.


## Flutter commands
- To check if the dev environment is setup properly - flutter doctor
- Running the application - flutter run
- When multiple simulators are running, select one simulator by - flutter run -d <simulator-id> -t lib/<file.dart>
- List all the simulators available - flutter devices
- Build apk flavor - flutter build apk --flavor staging -t lib/main_staging.dart

## Build ios for Testing 
- Package name (com.Pocketpills.staging) => select Runner.app

## Build ios for Prod
- Package name (com.Pocketpills) => select Runner-prod.app

## Note 
- info.plist for staging
- Runner copy-info.plist for prod

## Dependencies
- Download module from https://github.com/iamatulkumar/native_mixpanel into root directory

## Build staging apk
- flutter build apk --release  --flavor=staging --target=lib/main_staging.dart

